/* DroughtQuery */
SELECT *
FROM EffectDrought LEFT JOIN EffectDroughtType ON EffectDrought.EffectValueType=EffectDroughtType.Id;

/* DroughtLookupQuery */
SELECT *
FROM EffectDroughtLookup LEFT JOIN EffectDroughtType ON EffectDroughtLookup.EffectValueType=EffectDroughtType.Id;

/* InvestmentQuery */
SELECT Cost AS ICost, ValueType AS IValueType, *
FROM DimensionType INNER JOIN InvestmentCost ON DimensionType.ID = InvestmentCost.DimensionType;

/* MaintenanceQuery */
SELECT Cost AS MCost, ValueType AS MValueType, *
FROM DimensionType INNER JOIN MaintenanceCost ON DimensionType.ID = MaintenanceCost.DimensionType;

/* MeasureCombinationQuery */
SELECT MeasureCombination.ID AS mcid, *
FROM MeasureCombination INNER JOIN MeasureEffect ON MeasureCombination.Measures.Value = MeasureEffect.meID;

/* MeasureEffect */
SELECT *
FROM ((((((SELECT Measure.ID as meID, Measure.CalculationType, Measure.Mechanism, Measure.Source, Measure.MaintenanceCost, Measure.InvestmentCost, EffectValue,  EffectValueMap, AdditionalBenefit, LifeSpan, FWOOgrid, SideEffect, DroughtQuery.EffectDroughtType.ID as EffectValueType, DroughtQuery.EffectDroughtType.Description AS Description, DroughtQuery.ValueType as QValueType, 'Drought' AS Type
FROM Measure

INNER JOIN DroughtQuery
ON Measure.EffectDrought = DroughtQuery.EffectDrought.ID

UNION

SELECT Measure.ID as meID, Measure.CalculationType, Measure.Mechanism, Measure.Source, Measure.MaintenanceCost, Measure.InvestmentCost, EffectValue,   EffectValueMap, AdditionalBenefit, LifeSpan, FWOOgrid, SideEffect, SaltQuery.EffectSaltType.ID as EffectValueType, SaltQuery.EffectSaltType.Description AS Description, SaltQuery.ValueType as QValueType, 'Salt' AS Type
FROM Measure
INNER JOIN SaltQuery
ON Measure.EffectSalt = SaltQuery.EffectSalt.ID
)  AS me INNER JOIN MechanismType ON me.Mechanism = MechanismType.ID) INNER JOIN CalculationType ON me.CalculationType = CalculationType.ID) 
INNER JOIN MaintenanceQuery ON me.MaintenanceCost=MaintenanceQuery.MaintenanceCost.ID) 
INNER JOIN InvestmentQuery ON me.InvestmentCost=InvestmentQuery.InvestmentCost.ID) 
INNER JOIN SourceQuery ON me.Source = SourceQuery.Source.ID) 
INNER JOIN SideEffectQuery ON me.SideEffect=SideEffectQuery.SEid;

/* SaltQuery */
SELECT *
FROM EffectSalt LEFT JOIN EffectSaltType ON EffectSalt.EffectValueType=EffectSaltType.Id;

/* SideEffectQuery */
SELECT ID AS SEid, Waterkwaliteit AS SEwaterkwaliteit, Piekafvoer AS SEpiekafvoer, Bodemdaling AS SEbodemdaling, Verdroging AS SEverdroging, Uitspoeling_N AS SEuitspoelN, Uitspoeling_P AS SEuitspoelP
FROM SideEffect;

/* SourceQuery */
SELECT *
FROM SourceType 
INNER JOIN Source ON SourceType.ID = Source.SourceType;
