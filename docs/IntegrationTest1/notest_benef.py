# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 17:41:58 2017

@author: delsman
"""
import pandas as pd
import numpy as np
import raster_func as rf

companies = rf.raster2arr(
    r"d:\SVNREPOS\regioscan_zoetwater\data\HydroInput\Scenario1\modelbedrijven.idf"
)
complist = np.unique(companies.arr)[:-1]
ref = r"d:\SVNREPOS\regioscan_zoetwater\calc\Scenario1\reference\Agricom_Out\Module_C\ActYldEur_%4i1231.idf"
refag = r"d:\SVNREPOS\regioscan_zoetwater\calc\Scenario1\reference\Agricom_Out\Module_A\%s_%4i1231.idf"
mc1 = r"d:\SVNREPOS\regioscan_zoetwater\calc\Scenario1\mc_1\Agricom_Out\Module_C\ActYldEur_%4i1231.idf"
# mc2 = r'd:\SVNREPOS\regioscan_zoetwater\calc\Scenario1\mc_2\Agricom_Out\Module_C\ActYldEur_%4i1231.idf'

years = range(1980, 2011)


def sum_comp(arr, comp, complist):
    result = pd.Series(index=complist)
    for c in complist:
        result.ix[c] = np.sum(arr.arr[comp.arr == c])
    return result


resref = pd.DataFrame(index=complist, columns=years)
resrefag = pd.DataFrame(index=complist, columns=years)
resrefag.iloc[:, :] = 0
resmc1 = pd.DataFrame(index=complist, columns=years)
for yr in years:
    aref = rf.raster2arr(ref % yr)
    amc1 = rf.raster2arr(mc1 % yr)
    # amc2 = rf.raster2arr(mc2%yr)
    resref[yr] = sum_comp(aref, companies, complist)
    resmc1[yr] = sum_comp(amc1, companies, complist)

    for s in ["arbeidskosten", "energiekosten", "heffingen"]:
        arefag = rf.raster2arr(refag % (s, yr))
        resrefag[yr] += sum_comp(arefag, companies, complist)


resref.to_csv("actyldref.csv")
resrefag.to_csv("sprinkref.csv")
resmc1.to_csv("actyldmc1.csv")
