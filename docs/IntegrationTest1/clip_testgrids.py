# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 13:39:27 2017

@author: delsman

script to clip entire regioscan schem to two model companies

"""

import raster_func as rf
import os, glob
import numpy as np

companies = [1, 2]
extent = [183750, 417750, 186500, 419000]

dir_in = r"d:\SVNREPOS\regioscan_zoetwater\data\HydroInput"
dir_out = r"d:\SVNREPOS\regioscan_zoetwater\docs\HydroInput_test"

if not os.path.exists(dir_out):
    os.mkdir(dir_out)

gi = [
    extent[0],
    extent[1],
    250,
    250,
    (extent[3] - extent[1]) / 250,
    (extent[2] - extent[0]) / 250,
    1,
    0,
    0,
]

mask = rf.raster2arr(os.path.join(dir_in, "Scenario1", "modelbedrijven.idf"), gi=gi)
b = (mask.arr * 0).astype(bool)
for c in companies:
    b = b | (mask.arr == c)
mask.arr = mask.arr * 0 + 1
mask.arr.mask = ~b
# mask.arr2raster(os.path.join(dir_out,'test.idf'))

dir_out2 = dir_out
for r, ds, fs in os.walk(dir_in):
    print(r)
    # cdir = os.path.join(dir_out,r.replace(dir_in,''))
    cdir = dir_out + r.replace(dir_in, "")
    # create dir
    for d in ds:
        if not os.path.exists(os.path.join(cdir, d)):
            os.mkdir(os.path.join(cdir, d))
    for f in fs:
        arr = rf.raster2arr(os.path.join(r, f), gi=gi) * mask
        arr.arr2raster(os.path.join(cdir, f))
