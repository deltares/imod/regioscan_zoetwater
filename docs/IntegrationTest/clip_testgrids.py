# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 13:39:27 2017

@author: delsman

script to clip entire regioscan schem to two model companies

"""

import raster_func as rf
import os
import numpy as np

companies = [1, 2, 8, 477, 600]  # [1,2,10,41,323,356,311,348]
gi = [183750, 400000, 199000, 419000]

dir_in = r"d:\SVNREPOS\regioscan_zoetwater\data\HydroInput_Raam"
dir_out = r"d:\SVNREPOS\regioscan_zoetwater\docs\IntegrationTest\HydroInput_IntTest"

if not os.path.exists(dir_out):
    os.mkdir(dir_out)

if gi is not None:
    gi = [gi[0], gi[1], 250, 250, (gi[3] - gi[1]) / 250, (gi[2] - gi[0]) / 250, 1, 0, 0]

mask = rf.raster2arr(os.path.join(dir_in, "Scenario1", "modelbedrijven.idf"), gi=gi)
b = (mask.arr * 0).astype(bool)
for c in companies:
    b = b | (mask.arr == c)
mask.arr = mask.arr * 0 + 1
mask.arr.mask = ~b
# mask.arr2raster(os.path.join(dir_out,'test.idf'))

dir_out2 = dir_out
for r, ds, fs in os.walk(dir_in):
    print(r)
    # cdir = os.path.join(dir_out,r.replace(dir_in,''))
    cdir = dir_out + r.replace(dir_in, "")
    # create dir
    for d in ds:
        if not os.path.exists(os.path.join(cdir, d)):
            os.mkdir(os.path.join(cdir, d))
    for f in fs:
        if "deelgebieden" in f or "Kansrijkheid" in r:
            arr = rf.raster2arr(
                os.path.join(r, f), gi=gi
            )  # alleen bijsnijden, niet clippen naar bedrijven
        else:
            arr = rf.raster2arr(os.path.join(r, f), gi=gi) * mask
        arr.arr2raster(os.path.join(cdir, f))
