�}q (K]q(}q(X	   MechanismqKX
   QValueTypeqKX   DescriptionqX   OpslagqX   InvestmentCost.DescriptionqX   Investering ASR Zoet waterqX   Measuresq	X   1;2q
X   ICostqG?�      X   Source.DescriptionqX   NeerslagqX	   Source.IDqKX   IDqKX   MCostqG?��Q��X   DimensionTypeqKX   DimensionType.IDqKX   mcidqKX   MeasureCombinationTypeqKX   LongDescriptionqX   ASR zoet met dripirrigatieqX   FWOOgridqX   dank_8_mar_hoog.IDFqX   SourceType.DescriptionqX   NeerslagqX   EffectValueqG@Y      X   SourceType.IDqKX   MaintenanceCostqKX   EffectValueTypeqKX   AdditionalBenefitqG        X   Sourceq KX   DimensionType.Descriptionq!X   Watervolume [m3]q"X   Mapq#NX
   SourceTypeq$KX   CalculationType.IDq%KX   LifeSpanq&KX
   IValueTypeq'KX   InvestmentCost.IDq(KX   MaintenanceCost.IDq)KX   InvestmentCostq*KX   CalculationType.Descriptionq+X	   Standaardq,X   Typeq-X   Saltq.X
   MValueTypeq/KX   MaintenanceCost.Descriptionq0X   Onderhoud ASR zoetq1X   CalculationTypeq2Ku}q3(hKhKhX
   Toedieningq4hX   Investering dripirrigatieq5h	X   1;2q6hG@@     hX
   Grondwaterq7hKhKhG@b�     hKhKhKhKhX   ASR zoet met dripirrigatieq8hX   kansrijkheid_drip.IDFq9hX
   Grondwaterq:hG@T      hKhKhKhG        h Kh!X   Areaal [ha]q;h#Nh$Kh%Kh&Kh'Kh(Kh)Kh*Kh+X	   Standaardq<h-X   Droughtq=h/Kh0X   Onderhoud dripq>h2KueK]q?(}q@(hKhKhX
   ToedieningqAhX   Investering dripirrigatieqBh	X   2;3qChG@@     hX
   GrondwaterqDhKhKhG@b�     hKhKhKhKhX   Freshmaker met dripirrigatieqEhX   kansrijkheid_drip.IDFqFhX
   GrondwaterqGhG@T      hKhKhKhG        h Kh!X   Areaal [ha]qHh#Nh$Kh%Kh&Kh'Kh(Kh)Kh*Kh+X	   StandaardqIh-X   DroughtqJh/Kh0X   Onderhoud dripqKh2Ku}qL(hKhKhX   OpslagqMhX   Investering FreshmakerqNh	X   2;3qOhG@
��
=p�hX   Overschot oppervlaktewaterqPhKhKhG?���Q�hKhKhKhKhX   Freshmaker met dripirrigatieqQhX   fwoo4_freshmaker.IDFqRhX   Overschot oppervlaktewaterqShG@Y      hKhKhKhG        h Kh!X   Watervolume [m3]qTh#Nh$Kh%Kh&Kh'Kh(Kh)Kh*Kh+X	   StandaardqUh-X   SaltqVh/Kh0X   Onderhoud FreshmakerqWh2KueK
]qX(}qY(hKhKhX   Opslag en toedieningqZhX   Investering drains2bufferq[h	X   10q\hG@��     hX   Neerslagq]hKhKhG@      hKhKhK
hKhX   Conventionele drainageq^hX   kansrijkheid_draintrad.IDFq_hX   Neerslagq`hG@Y      hKhKhKhG        h Kh!X   Areaal [ha]qah#Nh$Kh%Kh&K
h'Kh(Kh)Kh*Kh+X	   Standaardqbh-X   Saltqch/Kh0X   Onderhoud Drains2bufferqdh2Ku}qe(hKhKhX   Opslag en toedieningqfhX   Investering drains2bufferqgh	X   10qhhG@��     hX   NeerslagqihKhKhG@      hKhKhK
hKhX   Conventionele drainageqjhX   kansrijkheid_draintrad.IDFqkhX   NeerslagqlhNhKhKhNhG        h Kh!X   Areaal [ha]qmh#Nh$Kh%Kh&K
h'Kh(Kh)Kh*Kh+X	   Standaardqnh-X   Droughtqoh/Kh0X   Onderhoud Drains2bufferqph2KueK]qq(}qr(hKhKhX
   ToedieningqshX%   Investering beregeningsinstallatie owqth	X   5;7quhG@��     hX   OppervlaktewaterqvhKhKhG@J�     hKhKhKhKhX,   Kreekruginfiltratie met reguliere beregeningqwhX   bereg_oppw.IDFqxhX   OppervlaktewaterqyhG@9      hKhKhKhG        h Kh!X   Areaal [ha]qzh#Nh$Kh%Kh&K
h'Kh(Kh)Kh*Kh+X	   Standaardq{h-X   Droughtq|h/Kh0X   Onderhoud beregening owq}h2Ku}q~(hKhKhX   OpslagqhX   Investering kreekruginfiltratieq�h	X   5;7q�hG@       hX   Overschot oppervlaktewaterq�hKhKhG?�G�z�HhKhKhKhKhX,   Kreekruginfiltratie met reguliere beregeningq�hX   fwoo3_kreekruginfiltratie.IDFq�hX   Overschot oppervlaktewaterq�hG@Y      hKhK
hKhG        h Kh!X   Watervolume [m3]q�h#Nh$Kh%Kh&Kh'Kh(K
h)K
h*K
h+X	   Standaardq�h-X   Saltq�h/Kh0X   Onderhoud Kreekruginfiltratieq�h2Kueu.