import pytest

from regioscan_zoetwater.company import HydroInput
from regioscan_zoetwater.utils import idfopen
from regioscan_zoetwater.agricom import read_module_a
from regioscan_zoetwater.exceptions import InputDataError


def test_company_base():
    company_fn = "TestModelCompanyBase.idf"
    type_fn = "TestCompanyTypeBase.idf"
    area_fn = "tests/test_data/CompanyData/TestSubAreaBase.idf"
    company_folder = "tests/test_data/CompanyData"
    hi = HydroInput(
        company_folder, area_fn, companies_fn=company_fn, companytypes_fn=type_fn
    )
    hi.sub_series(hi.subareas_idf, name="deelgebied")
    hi.sub_series(hi.companytypes_idf, name="type")


test_company_base()


def test_value_missing():
    company_fn = "TestModelCompanyBase.idf"
    area_fn = "tests/test_data/CompanyData/TestSubAreaValueMissing.idf"
    type_fn = "TestCompanyTypeValueMissing.idf"

    company_folder = "tests/test_data/CompanyData"
    with pytest.raises(InputDataError):
        hi = HydroInput(
            company_folder, area_fn, companies_fn=company_fn, companytypes_fn=type_fn
        )


def test_value_extra():
    company_fn = "TestModelCompanyBase.idf"
    area_fn = "tests/test_data/CompanyData/TestSubAreaValueExtra.idf"
    type_fn = "TestCompanyTypeValueExtra.idf"

    company_folder = "tests/test_data/CompanyData"
    hi = HydroInput(
        company_folder, area_fn, companies_fn=company_fn, companytypes_fn=type_fn
    )
    hi.sub_series(hi.subareas_idf, name="deelgebied")
    hi.sub_series(hi.companytypes_idf, name="type")


def test_split():
    company_fn = "TestModelCompanyBase.idf"
    type_fn = "TestCompanyTypeBase.idf"
    area_fn = "tests/test_data/CompanyData/TestSubAreaBase.idf"
    area_split_fn = "tests/test_data/CompanyData/TestSubAreaCompanySplit.idf"
    type_split_fn = "tests/test_data/CompanyData/TestCompanyTypeCompanySplit.idf"

    company_folder = "tests/test_data/CompanyData"
    hi = HydroInput(
        company_folder, area_fn, companies_fn=company_fn, companytypes_fn=type_fn
    )

    with pytest.raises(InputDataError):
        hi.sub_series(idfopen(area_split_fn), name="deelgebied")
    with pytest.raises(InputDataError):
        hi.sub_series(idfopen(type_split_fn), name="type")
