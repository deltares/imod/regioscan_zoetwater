import pytest
from regioscan_zoetwater.constants import *

STARTYEAR = 1950
ENDYEAR = 1952
from regioscan_zoetwater.measures import (
    calc_cost_benefit,
    calculate_dimensions,
    calc_saltdamage_algorithm,
    read_data_per_modelcompany_mm2m3_over_years,
    sum_water_usage,
    read_act,
    calculate_q_app,
    calc_fwoo_per_company,
    calc_costs,
    interpolate,
    calculate_write_droughtdamages,
    calc_fwoo_per_combination,
    calc_overall_fwoo,
    get_company_source,
    create_highvalue_mask,
    write_irr_files,
)
from regioscan_zoetwater.company import HydroInput
from regioscan_zoetwater.utils import idfopen
from collections import OrderedDict


from os.path import exists, join
from os import mkdir
import pandas as pd
import numpy as np


def roughly_equal(a, b, floatdiff=0.0001):
    return abs(a - b) < floatdiff


def roughly_equal_arr(a, b, floatdiff=0.0001):
    return (abs(a[~np.isnan(a)] - b[~np.isnan(a)]) < floatdiff).all()


@pytest.fixture
def test_hi():
    company_fn = "TestModelCompanyBase.idf"
    type_fn = "TestCompanyTypeBase.idf"
    company_folder = "tests/test_data/CompanyData/"
    area_fn = "tests/test_data/CompanyData/TestSubAreaBase.idf"
    hi = HydroInput(
        company_folder, area_fn, companies_fn=company_fn, companytypes_fn=type_fn
    )
    return hi


def test_calc_cost_benefit():

    test_input = """ls;investmentcost;T;maintenancecost;benefit_mdb;DiscountRate;NBC
    20;10000;30;500;1200;0.01;0.152902076
    30;10000;30;500;1200;0.01;0.358011902
    50;10000;30;500;1200;0.01;0.643415057
    20;5000;30;500;1200;0.01;0.557580211
    20;10000;30;1000;1200;0.01;-0.221209894
    20;10000;30;500;2000;0.01;0.92150346
    20;10000;15;500;1200;0.01;0.158779183"""

    input = []
    for i, line in enumerate(test_input.split("\n")):
        if i == 0:
            headers = line.split(";")
        else:
            values = line.split(";")
            values = [float(value) for value in values]
            input.append(dict(zip(headers, values)))

    for inp in input:
        T = int(inp.pop("T"))
        nbc = inp.pop("NBC")
        ls = inp.pop("ls")
        investmentcost = inp.pop("investmentcost")
        maintcost = inp.pop("maintenancecost")
        dr = inp.pop("DiscountRate")
        inp["lss"] = {"a": ls}
        inp["investmentcosts"] = {"a": investmentcost}
        inp["irr_maint"] = {k: 0 for k in range(1, T + 1)}
        inp["act_meas"] = {k: 0 for k in range(1, T + 1)}
        inp["act_ref"] = {k: 0 for k in range(1, T + 1)}
        inp["maintenancecost"] = {k: maintcost for k in range(1, T + 1)}
        inp["discountrate"] = 0.01
        cost, benefit, nbc_calc, benef_crop, benef_sprink = calc_cost_benefit(**inp)
        print(nbc_calc, nbc, cost, benefit)
        assert roughly_equal(nbc_calc, nbc)


def test_qapp():

    q_app_kolom = {
        1: {
            1: {1980: 10, 1981: 10, 1982: 10},
            2: {1980: 200, 1981: 100, 1982: 50},
            3: {1980: 3000, 1981: 3000, 1982: 3000},
            4: {1980: 3000, 1981: 3000, 1982: 3000},
            5: {1980: 500000, 1981: 500000, 1982: 500000},
            6: {1980: 6000000, 1981: 6000000, 1982: 6000000},
        },
        2: {2: {1980: 10, 1981: 20, 1982: 30}},
    }

    expected = {1: {"WaterUsage": 380}, 2: {"WaterUsage": 60}}

    waterusage = sum_water_usage(q_app_kolom)
    assert waterusage == expected


def test_dimensions(test_hi):
    perc = idfopen("tests/test_data/Dimension/Test90perc_Tred.idf")
    hi = test_hi
    cellsizes = [100, 250]
    effs = [20, 80]

    expected = {
        250: {
            20: [43750.0, 156.25, 93750.0, 2500.0],
            80: [10937.5, 39.0625, 23437.5, 625.0],
        },
        100: {20: [7000.0, 25.0, 15000.0, 400.0], 80: [1750.0, 6.25, 3750.0, 100.0]},
    }

    for cellsize in cellsizes:
        for eff in effs:
            perc.coords["dx"] = cellsize
            perc.coords["dy"] = -cellsize
            a = calculate_dimensions(perc, eff, hi)
            assert roughly_equal(sum(a.values()), sum(expected[cellsize][eff]))

    hv = idfopen("tests/test_data/FWOO/TestHighValue.idf").load()
    expected = [9375.0, 39.0625, 23437.5, 625.0]
    a = calculate_dimensions(perc, eff, hi, mask_highvalue=hv)
    assert roughly_equal(sum(a.values()), sum(expected))


def test_tred(test_hi):
    expected = {
        1.0: {1950: 350.0, 1951: 10.0, 1952: 24.75},
        2.0: {1950: 1.25, 1951: 10.0, 1952: 9.5},
        3.0: {1950: 750.0, 1951: 15.0, 1952: 15.75},
        4.0: {1950: 20.0, 1951: 20.0, 1952: 14.5},
    }

    hi = test_hi
    folder = "tests/test_data/Tred/"

    hi.dx = 50
    hi.dy = 50
    out = read_data_per_modelcompany_mm2m3_over_years(
        folder, hi, {"startyear": 1950, "endyear": 1952}, "tred"
    )

    for m, years in out.items():
        assert m in expected
        for year in years:
            assert year in expected[m]
            assert roughly_equal(out[m][year], expected[m][year])


def test_write_irr_files(test_hi, tmp_path):
    sources = {1: {1: 25.0}, 2: {1: 35.0}, 3: {1: 10.0}, 4: {2: 25.0}}
    q_app_kolom = {
        1: {1: {1980: 10000, 1981: 10000, 1982: 10000}},
        2: {1: {1980: 20000, 1981: 10000, 1982: 50000}},
        3: {1: {1980: 30000, 1981: 30000, 1982: 30000}},
        4: {1: {1980: 30000, 1981: 30000, 1982: 30000}},
    }

    write_irr_files(sources, tmp_path, q_app_kolom, test_hi, mask=None)
    expected_1980_sw = np.array(
        [
            [106.666664, 0, 106.666664, 106.666664],
            [40.0, 40.0, 40.0, 0.0],
            [240.0, 240.0, 40.0, 0.0],
        ]
    )
    expected_1980_gw = np.array([[0, 0, 0, 0], [0, 0, 0, 240.0], [0, 0, 0, 240.0]])

    test_sw = idfopen(join(tmp_path, "beregening_sw_19801231.idf")).load()
    test_gw = idfopen(join(tmp_path, "beregening_gw_19801231.idf")).load()
    assert roughly_equal_arr(test_sw.values, expected_1980_sw)
    assert roughly_equal_arr(test_gw.values, expected_1980_gw)

    hv = idfopen("tests/test_data/FWOO/TestHighValue.idf").load()
    write_irr_files(sources, tmp_path, q_app_kolom, test_hi, mask=hv)
    expected_1980_sw = np.array(
        [
            [106.666664, 0, 106.666664, 106.666664],
            [0.0, 53.3333, 53.3333, 0.0],
            [240.0, 240.0, 53.3333, 0.0],
        ]
    )
    expected_1980_gw = np.array([[0, 0, 0, 0], [0, 0, 0, 240.0], [0, 0, 0, 240.0]])

    test_sw = idfopen(join(tmp_path, "beregening_sw_19801231.idf")).load()
    test_gw = idfopen(join(tmp_path, "beregening_gw_19801231.idf")).load()
    assert roughly_equal_arr(test_sw.values, expected_1980_sw)
    assert roughly_equal_arr(test_gw.values, expected_1980_gw)


def test_read(test_hi):
    hi = test_hi
    expected_fn = "tests/test_data/Result_act/ExpectedActualCropYield.csv"

    folder = "tests/test_data/Result_act"

    expected = pd.read_csv(expected_fn, sep=";")
    result = read_act(folder, hi, {"startyear": 1975, "endyear": 1976})
    result_fd = pd.DataFrame.from_dict(result, orient="index")
    result
    for column in result_fd:
        assert (
            expected[str(column)].astype(int).tolist()
            == result_fd[column].astype(int).tolist()
        )


def test_salt_damage():
    a = 0, 50, 0
    b = 1.5, 50, 0.75
    c = 300, 50, 150
    d = 0, 90, 0
    e = 1.5, 90, 0.15
    f = 300, 90, 30
    tests = [a, b, c, d, e, f]
    for test in tests:
        assert roughly_equal(test[2], calc_saltdamage_algorithm(test[0], test[1]))


def test_get_company_source(test_hi):
    hi = test_hi
    hi.companies = [1, 2, 3, 4]
    pths = {"scenario_input": "tests/test_data/Sources"}
    efficiency = 90.0

    # ditch and application measure
    measure1 = {
        MEID_: 1,
        "MeasureCombinationType": MCT_DITCHSPRI,
        "Mechanism": MECH_DITCH,
        SOURCE_VALUE_: ST_SURFW,
        SOURCE_VALUETYPE_: VALUE_DIRECT,
    }
    measure2 = {
        MEID_: 2,
        "MeasureCombinationType": MCT_DITCHSPRI,
        "Mechanism": MECH_APPL,
        SOURCE_VALUE_: ST_GROUNDW,
        SOURCE_VALUETYPE_: VALUE_DIRECT,
    }
    combination = [measure1, measure2]
    expected = {1: {2: 90.0}, 2: {2: 90.0}, 3: {2: 90.0}, 4: {2: 90.0}}

    result = get_company_source(combination, efficiency, pths, hi)
    assert result.items() == expected.items()

    # buffer and application measure
    measure1 = {
        MEID_: 1,
        "MeasureCombinationType": MCT_BUFFSPRI,
        "Mechanism": MECH_BUFF,
        SOURCE_VALUE_: ST_SURFW,
        SOURCE_VALUETYPE_: VALUE_DIRECT,
    }
    measure2 = {
        MEID_: 2,
        "MeasureCombinationType": MCT_BUFFSPRI,
        "Mechanism": MECH_APPL,
        SOURCE_VALUE_: ST_GROUNDW,
        SOURCE_VALUETYPE_: VALUE_DIRECT,
    }
    combination = [measure1, measure2]
    expected = {1: {1: 90.0}, 2: {1: 90.0}, 3: {1: 90.0}, 4: {1: 90.0}}

    result = get_company_source(combination, efficiency, pths, hi)
    assert result.items() == expected.items()

    # single drain measure
    measure1 = {
        MEID_: 1,
        "MeasureCombinationType": MCT_DRAIN,
        "Mechanism": MECH_DRAIN,
        SOURCE_VALUE_: ST_SURFW,
        SOURCE_VALUETYPE_: VALUE_DIRECT,
    }
    combination = [measure1]
    expected = {1: {1: 90.0}, 2: {1: 90.0}, 3: {1: 90.0}, 4: {1: 90.0}}

    result = get_company_source(combination, efficiency, pths, hi)
    assert result.items() == expected.items()

    # single application measure
    measure1 = {
        MEID_: 1,
        "MeasureCombinationType": MCT_APPL,
        "Mechanism": MECH_APPL,
        SOURCE_VALUE_: ST_GROUNDW,
        SOURCE_VALUETYPE_: VALUE_DIRECT,
    }
    combination = [measure1]
    expected = {1: {2: 90.0}, 2: {2: 90.0}, 3: {2: 90.0}, 4: {2: 90.0}}

    result = get_company_source(combination, efficiency, pths, hi)
    assert result.items() == expected.items()

    # source from map
    measure1 = {
        MEID_: 1,
        "MeasureCombinationType": MCT_APPL,
        "Mechanism": MECH_APPL,
        SOURCE_VALUETYPE_: VALUE_MAP,
        SOURCE_VALUEMAP_: "TestSourceMeasure1.idf",
        SOURCE_VALUE_: ST_GROUNDW,
    }
    combination = [measure1]
    expected = {1: {2: 90.0}, 2: {1: 90.0}, 3: {2: 90.0}, 4: {2: 90.0}}

    result = get_company_source(combination, efficiency, pths, hi)
    assert result.items() == expected.items()


def test_calculate_q_app():

    expected = {
        1: {1: {1980: 29970.38574, 1981: 13611.16943, 1982: 7628.123779}},
        2: {1: {1980: 13893.5144, 1981: 4175.3125, 1982: 2314.372101}},
    }

    tred_sum_col = {
        1: {1980: 23976.30859375, 1981: 10888.935546875, 1982: 6102.4990234375},
        2: {1980: 13961.9384765625, 1981: 3340.25, 1982: 1851.49768066406},
    }

    dimension_col = OrderedDict([(1.0, 30846.484375), (2.0, 13893.5144042968)])

    sources = {1: {1: 80.0}, 2: {1: 80.0}}
    q_app_col = calculate_q_app(tred_sum_col, dimension_col, sources)

    for i in expected:
        for j in expected[i]:
            for year in expected[i][j]:
                assert roughly_equal(expected[i][j][year], q_app_col[i][j][year])


def test_calc_fwoo_per_company(test_hi):
    hi = test_hi
    fwoo_fn = "tests/test_data/FWOO/TestFWOOMeasure1.idf"

    result = calc_fwoo_per_company(fwoo_fn, hi)

    expected = OrderedDict([(1.0, 2.0), (2.0, 0.0), (3.0, 2.0), (4.0, 0.0)])
    assert result.items() == expected.items()

    # with highvalue_mask
    hv = idfopen("tests/test_data/FWOO/TestHighValue.idf").load()
    result = calc_fwoo_per_company(fwoo_fn, hi, mask=hv)

    expected = OrderedDict([(1.0, 0.0), (2.0, 0.0), (3.0, 2.0), (4.0, 0.0)])
    assert result.items() == expected.items()


def test_calc_fwoo_per_combination(test_hi):
    hi = test_hi
    fwoo_fn1 = "tests/test_data/FWOO/TestFWOOMeasure1.idf"
    fwoo_fn2 = "tests/test_data/FWOO/TestFWOOMeasure2.idf"
    pths = {"fwoofolder": ""}
    measure1 = {
        MEID_: 1,
        "FWOOgrid": fwoo_fn1,
        "MeasureCombinationType": MCT_DITCHSPRI,
        "Mechanism": MECH_DITCH,
    }
    measure2 = {
        MEID_: 2,
        "FWOOgrid": fwoo_fn2,
        "MeasureCombinationType": MCT_DITCHSPRI,
        "Mechanism": MECH_APPL,
    }
    combination = [measure1, measure2]

    fwoo_measure = calc_fwoo_per_combination(combination, hi, pths, highvalue_mask=None)

    expected = {
        1: OrderedDict([(1.0, 2.0), (2.0, 0.0), (3.0, 2.0), (4.0, 0.0)]),
        2: OrderedDict([(1.0, 1.0), (2.0, 0.0), (3.0, 6.0), (4.0, 5.0)]),
    }
    assert fwoo_measure.items() == expected.items()

    # test REZOWA-157
    measure1 = {
        MEID_: 1,
        "FWOOgrid": fwoo_fn1,
        "MeasureCombinationType": MCT_BUFFSPRI,
        "Mechanism": MECH_BUFF,
    }
    measure2 = {
        MEID_: 2,
        "FWOOgrid": fwoo_fn2,
        "MeasureCombinationType": MCT_BUFFSPRI,
        "Mechanism": MECH_APPL,
    }
    combination = [measure1, measure2]
    fwoo_measure = calc_fwoo_per_combination(combination, hi, pths, highvalue_mask=None)

    expected = {
        1: OrderedDict([(1.0, 2.0), (2.0, 0.0), (3.0, 2.0), (4.0, 0.0)]),
        2: OrderedDict([(1.0, 1.0), (2.0, 1.0), (3.0, 1.0), (4.0, 1.0)]),
    }
    assert fwoo_measure.items() == expected.items()


def test_calc_overall_fwoo(test_hi):
    hi = test_hi

    fwoo_measure = {
        1: OrderedDict([(1.0, 2.0), (2.0, 0.0), (3.0, 2.0), (4.0, 0.0)]),
        2: OrderedDict([(1.0, 1.0), (2.0, 1.0), (3.0, 1.0), (4.0, 1.0)]),
    }

    fwoo_col = calc_overall_fwoo(fwoo_measure, hi)

    expected = {1: 1, 2: 0, 3: 1, 4: 0}
    assert fwoo_col.items() == expected.items()


def test_create_highvalue_mask(test_hi):
    pths = {"agrcrops_fn": "tests/test_data/CompanyData/TestCrops.idf"}
    hv = create_highvalue_mask(pths, test_hi, highvalue_crops=[6, 7, 8, 9, 10]).load()
    expected = np.array(
        [[0.0, np.nan, 1.0, 1.0], [1.0, 1.0, 1.0, 1.0], [1.0, 0.0, 1.0, 1.0]]
    )
    assert roughly_equal_arr(hv.values, expected)


def test_calc_costs(test_hi):
    hi = test_hi
    mvalue = 0.03
    mtype = [1, 2]

    #     for i in mtype:
    mtype = DIMENSION_VOLUME
    dimension_col = {1: 15358.75}
    expected = {1: 460.7625}
    result = calc_costs(mtype, mvalue, hi, dimension_col)
    assert result == expected

    mvalue = 0.1
    expected = {1: 1535.875}
    result = calc_costs(mtype, mvalue, hi, dimension_col)
    assert result == expected

    mvalue = 0.03
    dimension_col = {1: 15000}
    expected = {1: 450}
    result = calc_costs(mtype, mvalue, hi, dimension_col)
    assert result == expected

    mtype = DIMENSION_AREA
    mvalue = 150
    dimension_col = 0

    expected = {1: 3750, 2: 2812.5, 3: 1875, 4: 1875}
    result = calc_costs(mtype, mvalue, hi, dimension_col)
    assert result == expected

    expected = {1: 2812.5, 2: 2812.5, 3: 1875, 4: 1875}
    hv = idfopen("tests/test_data/FWOO/TestHighValue.idf")
    result = calc_costs(mtype, mvalue, hi, dimension_col, mask=hv)
    assert result == expected

    mvalue = 123
    expected = {1: 3075}
    result = calc_costs(mtype, mvalue, hi, dimension_col)
    assert result[1] == expected[1]


# TODO: test calc_costs_from_table!


def test_interpolate():
    # def interpolate(idfmin, idfmax, pctmin, pctinter, pctmax):
    assert interpolate(0.0, 10.0, 0.0, 55.0, 100.0) == 5.5
    assert interpolate(0.0, 10.0, 0, 55, 100) == 5.5
    assert interpolate(10.0, 0.0, 0, 55, 100) == 4.5
    assert interpolate(0.3, 0.27, 0, 2, 10) == 0.294
    assert interpolate(0.3, 0.27, 10, 10, 20) == 0.3


def test_calculate_write_droughtdamages(tmp_path):
    # calculate_write_droughtdamages(percentage_arr,comp_nr,scenario_input,agricom_in_path, ini)
    comp_nr_fn = "tests/test_data/CompanyData/TestModelCompanyBase.idf"
    comp_nr = idfopen(comp_nr_fn)
    scenario_input = "tests/test_data"
    ini = {"startyear": 1950, "endyear": 1952}

    percentage_arr = comp_nr.copy(
        data=np.array(
            [[69, 60.6, 33, 55], [62.1, 2.9, 9.1, 24.6], [29.3, 75.4, 3, 70.8]]
        )
    )

    idf_droogte = calculate_write_droughtdamages(
        percentage_arr, scenario_input, tmp_path, ini
    )

    # 1950
    assert roughly_equal_arr(
        np.array(
            [
                [0.00124, np.nan, 0, 0.00045],
                [0.0758, 0.2913, 0.3636, 0.03016],
                [0.707, 0.492, 0.485, 0.01168],
            ]
        ),
        idfopen(idf_droogte["19501231.idf"]).values,
    )
