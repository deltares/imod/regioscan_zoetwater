import pytest
from os.path import join
import pandas as pd
from regioscan_zoetwater.result import (
    rank_df,
    sum_over_companytype,
    sum_over_subarea,
    get_adaptationdegree,
    rank_by_implementationdegree,
    get_implementationdegree,
)


def test_write_results_df_rank():
    test_folder = "tests/test_data/ResultRanking"
    test_fn = join(test_folder, "Ranking.csv")
    expected_fn = join(test_folder, "RankingExpected.csv")

    results = pd.read_csv(test_fn, sep=";", index_col=0)
    expected_df = pd.read_csv(expected_fn, sep=";", index_col=0)
    expected_df.index = list(expected_df.index)

    df = rank_df(results)

    pd.testing.assert_frame_equal(df, expected_df, check_dtype=False)


def test_sum_over_companytype():
    test_folder = "tests/test_data/ResultSum"
    test_fn = join(test_folder, "ResultsPerCompany.csv")
    expected_bytype_fn = join(test_folder, "ResultsPerCompanyType.csv")

    results = pd.read_csv(test_fn, sep=";")
    expected_bytype_df = pd.read_csv(
        expected_bytype_fn, sep=";", index_col=["subarea", "companytype"]
    )

    df_type = sum_over_companytype(results).set_index(
        ["subarea", "companytype"], inplace=False
    )

    expected_bytype_df = expected_bytype_df.reindex(df_type.columns, axis=1)
    pd.testing.assert_frame_equal(df_type, expected_bytype_df, check_dtype=False)


def test_summarize_subarea():
    test_folder = "tests/test_data/ResultSum"
    bytype_fn = join(test_folder, "ResultsPerCompanyType.csv")
    expected_bysubarea_fn = join(test_folder, "ResultsPerSubArea.csv")

    results = pd.read_csv(bytype_fn, sep=";")
    expected_subarea_df = pd.read_csv(
        expected_bysubarea_fn, sep=";", index_col="subarea"
    )

    df_subarea = sum_over_subarea(results).set_index("subarea", inplace=False)

    expected_subarea_df = expected_subarea_df.reindex(df_subarea.columns, axis=1)
    pd.testing.assert_frame_equal(df_subarea, expected_subarea_df, check_dtype=False)


def test_get_adaptationdegree():
    test_folder = "tests/test_data/Result_adaptation"
    adaptation_fn = join(test_folder, "Inputadaptation.csv")
    expected_fn = join(test_folder, "Expectedadaptation.csv")
    expected_control_fn = join(test_folder, "ExpectedControlResultsAdaption.csv")

    df = pd.read_csv(adaptation_fn, sep=";")
    subarea_col = df["subarea"]
    adaptation_boundary = 0.2
    result, result_control = get_adaptationdegree(subarea_col, df, adaptation_boundary)

    expected_df = pd.read_csv(expected_fn, sep=";")
    expected_control_df = pd.read_csv(expected_control_fn, sep=";")
    result.set_index("subarea", inplace=True)
    expected_df.set_index("subarea", inplace=True)

    expected_df = expected_df.reindex(result.index)
    pd.testing.assert_frame_equal(result, expected_df, check_dtype=False)

    result_control_df = pd.DataFrame(result_control)
    pd.testing.assert_frame_equal(
        result_control_df, expected_control_df, check_dtype=False
    )


def test_rank_by_implementationdegree():

    test_folder = "tests/test_data/ImplementationDegree"
    subarea_col_fn = join(test_folder, "subarea_col.csv")
    subarea_col = pd.read_csv(subarea_col_fn, sep=";")
    subarea_col.set_index("company", inplace=True)
    implementation_degree = 60
    expected = {1: 6.0, 2: 12.6}

    result, all_columns = get_implementationdegree(subarea_col, implementation_degree)
    assert result == expected
    df = subarea_col.drop(subarea_col.index[range(5)])
    df_by_impl_dg, all_columns = rank_by_implementationdegree(df, result, all_columns)

    expected_selected = {1: 5, 2: 13}
    assert all_columns["Selected"] == expected_selected
