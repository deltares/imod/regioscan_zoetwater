#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
import pandas as pd
from os.path import dirname, join
from regioscan_zoetwater.regioscan import calculate_measures
from regioscan_zoetwater.company import HydroInput
from collections import OrderedDict
from regioscan_zoetwater.paths import *
from regioscan_zoetwater.utils import read_ini, MeasuresDB
import ast
import pickle

__author__ = "Maarten Pronk"
__copyright__ = "Maarten Pronk"
__license__ = "none"


def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


# Unpack dictionary in custom way. Must be a better way...
def custom_parse_data(data):

    new_dict = {}
    for key in data.keys():
        if isfloat(key):
            use_key = float(key)
        else:
            use_key = key
        entries = []
        for i in data[key]:
            entries.append(ast.literal_eval(data[key][i]))
        new_dict[use_key] = entries

    return new_dict


def reset_keys(data):
    new_dict = {}
    for key in data.keys():
        if key.isdigit() or isfloat(key):
            use_key = float(key)
        else:
            use_key = key

        new_dict[use_key] = data[key]

    return new_dict


# Under construction:
# TODO:
# - put the expected results in the right format so they can be compared with the results.
# - Also assertion on the comparisson needs to be done.
# -Changes in the code could require changes in intializing the variabeles
#  and setting up the data to run calculate_measures().
def test_get_company_results():

    # not finished. Exit immidiatly
    return

    fn_ini = "D:/zowa/integration_test.ini"
    ini = read_ini(fn_ini)
    pths = set_paths(ini)
    #     makedirs(pths['scenario_output'], exist_ok=True)

    hi = HydroInput(pths["scenario_input"], pths["subarea_fn"])
    #     scenario_input = 'D:/zowa/tests/IntegrationTest/HydroInput_IntTest/Scenario1'
    #     subarea_fn= 'D:/zowa/tests/IntegrationTest/HydroInput_IntTest/deelgebieden.idf'
    #     hi = HydroInput(scenario_input, subarea_fn)

    test_data_folder = "D:/zowa/tests/test_data/Integration"
    #     test_data_folder = join(pths['hydroinput'],'Integration')

    act_ref_fn = join(test_data_folder, "act_ref.csv")
    maintenance_cost_reference_fn = join(
        test_data_folder, "maintenance_cost_reference.csv"
    )
    csv_waterusage_reference_fn = join(test_data_folder, "csv_waterusage_reference.csv")
    combinations_fn = join(test_data_folder, "combinations.csv")
    combinations = pd.read_csv(combinations_fn, index_col=0, sep=";").apply(OrderedDict)
    combinations = custom_parse_data(combinations)

    act_ref = pd.read_csv(act_ref_fn, index_col=0, header=0, sep=";").apply(OrderedDict)
    act_ref = reset_keys(act_ref)
    maintenance_cost_reference = pd.read_csv(
        maintenance_cost_reference_fn, index_col=0, sep=";"
    ).apply(OrderedDict)
    maintenance_cost_reference = reset_keys(maintenance_cost_reference)

    csv_waterusage_reference = pd.read_csv(
        csv_waterusage_reference_fn, index_col=0, sep=";"
    )
    csv_waterusage_reference = reset_keys(csv_waterusage_reference)

    #     with open(combinations_fn, 'rb') as f:
    #         combinations = pickle.load(f)

    subarea_col = hi.sub_series(hi.subareas_idf, name="SubArea")
    companytype_col = hi.sub_series(hi.companytypes_idf, name="CompanyType")
    measuresdb = MeasuresDB()

    #     company_mc_results, company_mc_results_success = calculate_measures(combinations,   hi,  test,  act_ref , maintenance_cost_reference, csv_waterusage_reference, subarea_col, companytype_col)
    company_mc_results, company_mc_results_success = calculate_measures(
        combinations,
        hi,
        False,
        act_ref,
        maintenance_cost_reference,
        csv_waterusage_reference,
        subarea_col,
        companytype_col,
        pths,
        measuresdb,
    )

    comp_expected_result_fn = csv_waterusage_reference_fn = join(
        test_data_folder, "company_mc_results.csv"
    )
    comp_expected_result_succes_fn = csv_waterusage_reference_fn = join(
        test_data_folder, "company_mc_results_success.csv"
    )
    comp_expected_result = pd.read_csv(
        comp_expected_result_fn, index_col=0, sep=";"
    ).apply(OrderedDict)
    comp_expected_result_succes = pd.read_csv(
        comp_expected_result_succes_fn, index_col=0, sep=";"
    ).to_dict()
    del comp_expected_result_succes["2"]
    del comp_expected_result_succes["7"]
    comp_expected_result_succes = custom_parse_data(comp_expected_result_succes)
    comp_expected_result = custom_parse_data(comp_expected_result)

    for result in company_mc_results:
        company_mc_results[i]


#     comp_expected_result= custom_parse_data(comp_expected_result)
