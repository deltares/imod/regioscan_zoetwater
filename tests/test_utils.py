#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
import regioscan_zoetwater.constants

regioscan_zoetwater.constants.STARTYEAR = 1950
regioscan_zoetwater.constants.ENDYEAR = 1952
from regioscan_zoetwater.company import HydroInput
from regioscan_zoetwater.utils import (
    idfopen,
    get_timeseries,
    reduce_timeseries_per_company_to_df,
    df_to_orddict,
    idf_from_company_value,
    interpolate_on_valuearr,
)
from collections import OrderedDict
import pandas as pd
import numpy as np

global ini
ini = {
    "startyear": regioscan_zoetwater.constants.STARTYEAR,
    "endyear": regioscan_zoetwater.constants.ENDYEAR,
}


def roughly_equal(a, b, floatdiff=0.0001):
    return abs(np.nan_to_num(a) - np.nan_to_num(b)) < floatdiff


def test_df_to_orddict():
    company_fn = "TestModelCompanyBase.idf"
    type_fn = "TestCompanyTypeBase.idf"
    company_folder = "tests/test_data/CompanyData/"
    area_fn = "tests/test_data/CompanyData/TestSubAreaBase.idf"
    hi = HydroInput(
        company_folder, area_fn, companies_fn=company_fn, companytypes_fn=type_fn
    )
    hi.companies = [1, 2, 3]
    df = pd.DataFrame(
        index=[1, 2, 3],
        columns=[1974, 1975, 1976],
        data=[[0, 1, 2], [1, 2, 3], [3, 4, 5]],
    )
    od = df_to_orddict(df, hi, first="companies")
    assert od == {
        1: {1974: 0, 1975: 1, 1976: 2},
        2: {1974: 1, 1975: 2, 1976: 3},
        3: {1974: 3, 1975: 4, 1976: 5},
    }

    od = df_to_orddict(df, hi, first="time")
    assert od == {
        1974: {1: 0, 2: 1, 3: 3},
        1975: {1: 1, 2: 2, 3: 4},
        1976: {1: 2, 2: 3, 3: 5},
    }

    df = pd.DataFrame(
        index=[1, 2, 3], columns=["dimensions"], data=[[0], [1], [2]]
    ).squeeze()
    od = df_to_orddict(df, hi, first="time")
    assert od == {1: 0, 2: 1, 3: 2}


def test_get_timeseries():
    damage_folder = "tests/test_data/Droughtdamage"
    times = [
        pd.Timestamp(1950, 12, 31),
        pd.Timestamp(1951, 12, 31),
        pd.Timestamp(1952, 12, 31),
    ]
    times = [t.to_datetime64() for t in times]

    da = get_timeseries(damage_folder, pattern="droogteschade*1231.idf", ini=ini)

    assert roughly_equal(
        da.load().values[0],
        np.array(
            [[0.004, np.nan, 0.0, 0.001], [0.2, 0.3, 0.4, 0.04], [1.0, 2.0, 0.5, 0.04]]
        ),
    ).all()

    assert (da.coords["time"].values == times).all()

    ini["startyear"] = 1951
    da = get_timeseries(damage_folder, pattern="droogteschade*1231.idf", ini=ini)

    assert roughly_equal(
        da.load().values[0],
        np.array(
            [
                [0.02, np.nan, 0.0, 0.02],
                [0.01, 0.01, 0.01, 0.04],
                [0.03, 0.03, 0.01, 0.04],
            ]
        ),
    ).all()

    assert (da.coords["time"].values == times[1:]).all()


def test_reduce_timeseries_per_company_to_df():
    company_fn = "TestModelCompanyBase.idf"
    type_fn = "TestCompanyTypeBase.idf"
    company_folder = "tests/test_data/CompanyData"
    area_fn = "tests/test_data/CompanyData/TestSubAreaBase.idf"
    hi = HydroInput(
        company_folder, area_fn, companies_fn=company_fn, companytypes_fn=type_fn
    )
    damage_folder = "tests/test_data/Droughtdamage"

    dfref = pd.DataFrame(
        index=[1, 2, 3, 4],
        columns=[1950, 1951, 1952],
        data=[
            [1.4, 0.04, 0.099],
            [0.005, 0.04, 0.038],
            [3, 0.06, 0.063],
            [0.08, 0.08, 0.058],
        ],
    )

    ini["startyear"] = 1950
    da = get_timeseries(damage_folder, pattern="droogteschade*1231.idf", ini=ini)

    df = reduce_timeseries_per_company_to_df(da, hi, func=np.sum, savemem=True)
    assert roughly_equal(dfref, df).all()

    df = reduce_timeseries_per_company_to_df(da, hi, func=np.sum, savemem=False)
    assert roughly_equal(dfref, df).all()

    ini["endyear"] = 1950
    da = get_timeseries(damage_folder, pattern="droogteschade*1231.idf", ini=ini)
    df = reduce_timeseries_per_company_to_df(da, hi, func=np.sum, savemem=False)
    assert roughly_equal(dfref[1950], df).all()


def test_idf_from_company_value():
    pass


def test_interpolate_on_valuearr_no_extrapolate():
    test_folder = "tests/test_data/Soilbuffer"
    fn_value_arr = "tests/test_data/Soilbuffer/bodembuffer.idf"

    da_ref = np.array(
        [
            [0.004, 0.0, 0.0, 0.02],
            [0.12875, 0.19125, 0.25375, 0.04],
            [1.0, 2.0, 0.5, 0.04],
        ],
        dtype=np.float32,
    )

    ini["startyear"] = 1950
    ini["endyear"] = 1950
    da = get_timeseries(
        test_folder,
        pattern="droogteschade_*_??mm.idf",
        ini=ini,
        idfopen_pattern="{name}_{time}_{valuearr}mm",
    )
    valuearr = idfopen(fn_value_arr)
    da_test = interpolate_on_valuearr(da, valuearr, dim="valuearr", extrapolate=False)

    assert np.allclose(da_ref, da_test.isel(time=0).values, atol=1e-5, equal_nan=True)


# REZOWA-171
def test_interpolate_on_valuearr_with_extrapolate():
    test_folder = "tests/test_data/Soilbuffer"
    fn_value_arr = "tests/test_data/Soilbuffer/bodembuffer.idf"

    da_ref = np.array(
        [
            [0.004, 0.0, 0.0, 0.02],
            [0.12875, 0.19125, 0.25375, 0.04],
            [1.0, 2.24625, 0.56125, 0.04],
        ],
        dtype=np.float32,
    )

    ini["startyear"] = 1950
    ini["endyear"] = 1950
    da = get_timeseries(
        test_folder,
        pattern="droogteschade_*_??mm.idf",
        ini=ini,
        idfopen_pattern="{name}_{time}_{valuearr}mm",
    )
    valuearr = idfopen(fn_value_arr)
    da_test = interpolate_on_valuearr(da, valuearr, dim="valuearr", extrapolate=True)

    assert np.allclose(da_ref, da_test.isel(time=0).values, atol=1e-5, equal_nan=True)
