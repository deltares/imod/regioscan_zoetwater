import pytest

from pandas import read_csv, DataFrame

import regioscan_zoetwater.constants

regioscan_zoetwater.constants.STARTYEAR = 1975
regioscan_zoetwater.constants.ENDYEAR = 1977

from regioscan_zoetwater.company import HydroInput
from regioscan_zoetwater.agricom import read_module_a
from regioscan_zoetwater.exceptions import InputDataError

global ini
ini = {
    "startyear": regioscan_zoetwater.constants.STARTYEAR,
    "endyear": regioscan_zoetwater.constants.ENDYEAR,
    "savemem": False,
}


def test_irrigation_costs():
    company_fn = "TestModelCompanyBase.idf"
    type_fn = "TestCompanyTypeBase.idf"
    companyfolder = "tests/test_data/CompanyData"
    costfolder = "tests/test_data/IrrigationCost"
    area_fn = "tests/test_data/CompanyData/TestSubAreaBase.idf"
    hi = HydroInput(
        companyfolder, area_fn, companies_fn=company_fn, companytypes_fn=type_fn
    )

    inv, mai = read_module_a(costfolder, hi, ini)
    inv["CompanyNumber"] = inv.index
    mai["CompanyNumber"] = mai.index
    inv.columns = inv.columns.astype(str)
    mai.columns = mai.columns.astype(str)

    inv_csv_expected_fn = (
        "tests/test_data/IrrigationCost/ExpectedIrrigationInvestmentCost.csv"
    )
    mai_csv_expected_fn = (
        "tests/test_data/IrrigationCost/ExpectedIrrigationMaintenanceCost.csv"
    )
    exp_inv = read_csv(inv_csv_expected_fn, sep=";")
    exp_mai = read_csv(mai_csv_expected_fn, sep=";")

    for column in exp_inv:
        assert exp_inv[column].astype(int).tolist() == inv[column].astype(int).tolist()

    for column in exp_mai:
        assert exp_mai[column].astype(int).tolist() == mai[column].astype(int).tolist()
