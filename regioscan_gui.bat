rem ### Batch file to start Regioscan-GUI
rem ###
rem ### Run as follows: regioscan_gui.bat <ini-file> <port:default 5002>
rem ###
rem ### Changing the port is necessary when opening multiple Regioscan-GUIs simultaneously
SET port=%2
IF "%port%"=="" (SET port=5002)

bokeh serve --show --port %port% regioscan_gui --args -i %1