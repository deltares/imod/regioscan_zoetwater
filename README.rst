==============================
Regioscan Zoetwatermaatregelen
==============================


De Regioscan Zoetwatermaatregelen is een instrument om snel inzicht te krijgen in effecten, kosten en baten 
van Zoetwatermaatregelen op agrarische bedrijven. De Regioscan bestaat uit een rekendeel (python-module) en
een interactieve GUI (gebaseerd op bokeh). Generieke maatregelinformatie is opgeslagen in een Access-database.

Rapporten over de Regioscan Zoetwatermaatregelen zijn te vinden onder reports.

Installatie
===========

De laatste release van de Regioscan Zoetwatermaatregelen is te vinden onder `releases <https://gitlab.com/deltares/imod/regioscan_zoetwater/-/releases>`__. 
Werking van de Regioscan vergt een python distributie >= 3.7, en kent verschillende dependencies. Deze zijn opgenomen in het bestand environment.yml. 
Deze kan eventueel gebruikt worden om - wanneer Anaconda wordt gebruikt - een Regioscan-specifieke environment aan te maken.

Na uitpakken van de release in een directory is de Regioscan Zoetwatermaatregelen in principe klaar voor gebruik, maar mist de Regioscan hydrologische invoer.
De benodigde hydrologische invoerbestanden zijn beschikbaar voor heel Nederland, maar vooralsnog niet eenvoudig downloadbaar. Neem hiervoor contact op
met de ontwikkelaars van de Regioscan.

Het rekendeel van de Regioscan kan worden aangeroepen via `python regioscan.py -ini <ini-bestand>`. De interactieve
GUI kan worden aangeroepen met `regioscan_gui.bat <gui ini-bestand>`.

