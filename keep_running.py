#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to restart Regioscan if crashes occur
Pass the same arguments as you would to regioscan.py directly
"""
import subprocess, sys

while 1:
    subprocess.call(["python", "regioscan.py"] + sys.argv[1:])

    with open("info.log", "rb+") as f:
        f.seek(-200, 2)  # from end of file
        message = f.read(200).decode()
        # print(message)
        if "Regioscan end" in message:
            print("Normal termination")
            break  # normal termination, quit
        if "ERROR" in message:
            print("Termination on ERROR, no restart")
            break
