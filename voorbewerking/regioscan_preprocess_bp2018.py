# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 13:16:29 2018

@author: delsman

Regioscan preprocessor - based on R scripting from Martin Mulder

Calculation steps:
    1) From LHM run, copy necessary files to MetBeregening:  (with sprinkling))
        bdgPm
        bdgPsgw
        bdgPssw
        msw_Eic
        msw_qmr
        msw_Tact
        msw_Tpot
        msw_Trel
        transol_con_rz
        Run AGRICOM to calculate crop damage
    2) Calculate ZonderBeregening
        Adjust Tact by sprinkling amount * efficiency
        Run AGRICOM to calculate crop damage
    3) Calculate Buffer measures
        Calculate 90 percentile Tred
        For both percentile and fixed buffer amounts:
            Reduce Tred by this buffer, by using buffer until empty
            Recalculate Tact and Trel
            Run AGRICOM to calculate crop damage
    3) Calculate Evenredig
        For percentages:
            Reduce Tred by given percentage, recalculate Tact, Trel
            Run AGRICOM to calculate crop damage
    4) Calculate Zout
        For salt concentrations:
            Set transol_con_rz to concentration
            Run AGRICOM to calculate crop damage
    5) Order in database for Regioscan
    
REFACTORING:
- do entire procedure for (4-year) subperiods, and delete temporary files, to deal with harddisk issues
- complication: 90 percentile buffer can only be determined using ALL annual Tred values
- workaround: do all other steps first, and cleanup. Only: store NoSprinkling Tpot and Tact
- then perform ExtBuffer.
TODO: implement automation of two-step process
"""
import os, glob
from zipfile import ZipFile
from time import strptime  # asctime,
from datetime import datetime, timedelta
from os.path import join, exists, basename  # , abspath
from shutil import copy, move, rmtree
from agricom import reclass_lhm_agricom, cropcalendar, write_agricom_ini, run_agricommp
import imod
import numpy as np
import xarray as xr
import pandas as pd

# import raster_func as rf
from multiprocessing import Pool
import warnings

warnings.filterwarnings("ignore", category=RuntimeWarning)


def get_timesteps(
    yearstart,
    yearend=None,
    start_or_end="start",
    reset_idec_year=False,
    dec_or_days="decade",
):
    """Iterator to get decadenr and date as ints for given year(s)"""
    if yearend is None:
        yearend = yearstart
    if dec_or_days == "decade":
        if start_or_end == "end":
            days = [10, 20, 31]
        else:
            days = [1, 11, 21]

        i = -1
        for year in range(yearstart, yearend + 1):
            if reset_idec_year:
                i = -1
            for m in range(1, 13):
                for d in days:
                    if d == 31:
                        # get actual last day
                        m1 = m
                        if m == 12:
                            m1 = 0
                        d = (datetime(year, m1 + 1, 1) - timedelta(days=1)).day
                    i += 1
                    yield i, year * 10000 + m * 100 + d
    else:
        # daily timesteps: use timedelta...
        for year in range(yearstart, yearend + 1):
            d = datetime(year, 1, 1)
            i = 0
            while d.year == year:
                yield i, int(d.strftime("%Y%m%d"))
                d = d + timedelta(days=1)
                i += 1


def doy(dec):
    """Calculate day-of-year and year of given decade"""
    t = strptime(str(dec), "%Y%m%d")
    return t.tm_yday - 1, t.tm_year


def dec_year(dec):
    """get decnr within year"""
    return dec % 36


def get_runzip(y, scen="REF2017BP18"):
    basedir = r"p:\11202240-kpp-dp-zoetwater\NWM_BP2018_en_historie\2018_BP2018_productieomgeving\Modelzips_Productieomgeving_NWM_Z1\{}".format(
        scen
    )

    loc = join(basedir, "ZW_LHM_{}*_{}.zip".format(y + 1, scen))

    fn = glob.glob(loc)
    if len(fn):
        print(fn[0])
    else:
        raise OSError("no zipfiles at location {}".format(loc))

    return ZipFile(fn[0])


def zipfn(fn):
    return os.path.normpath(fn).replace("\\", "/")


# def enlarge(fn):
#    nlgi = {'xll': 0.0, 'yll': 300000.0, 'dx': 250.0, 'dy': 250.0, 'nrow': 1300, 'ncol': 1200, 'proj': 1, 'ang': 0.0, 'crs': 'crs'}
#    arr = rf.raster2arr(fn,gi=nlgi)
#    arr.arr2raster(fn)


def run_agricom(agricom_ini, agricomdict):
    # run Agricom for each year in parallel
    write_agricom_ini(agricom_ini, agricomdict)

    p = Pool()
    # res = []
    # use multiprocessing to run Agricom per year
    results = []
    for y in range(period[0], period[1] + 1):
        results += [
            p.apply_async(run_agricommp, [y, agricom_ini, agricomdict])
        ]  # , callback=collect_result)#.get()
    output = [p.get() for p in results]
    p.close()
    p.join()  # 3600)
    p.terminate()
    # strip year from bodem, landuse, etc
    try:
        for fn in ["areaal", "beregeningstype", "bodemtype", "landgebruik"]:
            copy(
                join(
                    agricomdict["DirOut"], "{}".format(period[0]), "{}.idf".format(fn)
                ),
                join(agricomdict["DirOut"], "{}.idf".format(fn)),
            )
        for y in range(period[0], period[1] + 1):
            rmtree(join(agricomdict["DirOut"], "{}".format(y)))
    except FileNotFoundError:
        pass


def idfload(path, **kwargs):
    print("Loading {}".format(path))
    ret = imod.idf.open(path, **kwargs)
    return ret


def idfwrite(path, da, **kwargs):
    try:
        imod.idf.write(path, da.squeeze(drop=True), **kwargs)
    except ValueError:
        print(da.squeeze(drop=True))
        raise
    return None


def array_translate(da, translate_dict):
    da = da.load()
    da.values[~da.isnull()] = np.vectorize(translate_dict.get)(da.values[~da.isnull()])
    return da


if __name__ == "__main__":
    bpscen = "S2050BP18"  #'REF2017BP18'
    yearstart = 2004
    yearend = 2011
    # periods = [[1996,2006]]  # to do: create periods from startyear - endyear
    periods = [[s, min(s + 3, yearend)] for s in range(yearstart, yearend + 1, 4)]
    do_steps = [
        "WithSprinkling",
        "NoSprinkling",
        "SoilBuffer",
        "Proportional",
        "Salt",
        "RegioscanDB",
        "Cleanup",
    ]
    # do_steps = ['WithSprinkling','ExtBuffer','RegioscanDB','Cleanup']
    # do_steps = ['WithSprinkling','Cleanup']
    overwrite = False
    store_tmp = False
    do_salt = True
    dec_or_days = "decade"
    extbuffer_percentages = [90]
    soilbuffer_mms = [2, 5, 10, 20]
    proportional_pcts = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    salt_concentrations = [1000, 2000, 3000, 4000, 5000]
    spr_eff = 0.25

    resultbasedir = r"c:\Users\delsman\Documents\Regioscan\Preprocess_NL"
    resultdir = join(resultbasedir, bpscen)
    dir_withsprinkling = join(resultdir, "WithSprinkling")
    dir_nosprinkling = join(resultdir, "NoSprinkling")
    dir_buffer = join(resultdir, "Tmp_Buffer")
    dir_proportional = join(resultdir, "Tmp_Proportional")
    dir_salt = join(resultdir, "Tmp_Salt")
    dir_regioscandb = join(resultdir, "RegioscanDB")
    dir_store = join(resultdir, "Store_ExtBuffer")
    agricom_dir = r"p:\11202619-regioscan2\Agricom_WWL"
    svat_dir = join(
        agricom_dir, r"inputfiles"
    )  # nsvat_per idf_svat niet beschikbaar in BP2018 runs

    filepatterns = {
        "bdgPm": join("bdgPm", "bdgPm_<DATE>_l1.idf"),
        "bdgPsgw": join("bdgPsgw", "bdgPsgw_<DATE>_l1.idf"),
        "bdgPssw": join("bdgPssw", "bdgPssw_<DATE>_l1.idf"),
        "msw_Eic": join("msw_Eic", "msw_Eic_<DATE>_l1.idf"),
        "msw_Tact": join("msw_Tact", "msw_Tact_<DATE>_l1.idf"),
        "msw_Tpot": join("msw_Tpot", "msw_Tpot_<DATE>_l1.idf"),
        "msw_Trel": join("msw_Trel", "msw_Trel_<DATE>_l1.idf"),
        "transol_con_rz": join("transol_con_rz", "transol_con_rz_<DATE>.idf"),
        "Qappl": join("Qappl", "Qappl_<DATE>.idf"),
        "Qfill": join("Qfill", "Qfill_<DATE>.idf"),
        "Tred": join("Tred", "Tred_<DATE>_l1.idf"),
        "Tbuf": join("Tbuf", "Tbuf_<DATE>.idf"),
    }
    prms = [
        "bdgPm",
        "bdgPsgw",
        "bdgPssw",
        "msw_Eic",
        "msw_Tact",
        "msw_Tpot",
        "msw_Trel",
    ]
    if do_salt:
        prms += ["transol_con_rz"]
    simgro_files = [
        "mod2svat.inp",
        "area_svat.inp",
        "scap_svat.inp",
        "svat_per.key",
        "svat_per.tim",
        "idf_svat.inp",
    ]
    prms_notrel = [p for p in prms if p != "msw_Trel"]  # for convenience
    prms_unchanged = [
        p for p in prms if p not in ["msw_Tact", "msw_Trel", "bdgPsgw", "bdgPssw"]
    ]  # for convenience

    agricomdict = {
        "NamZnl": join(resultbasedir, "nl.idf"),
        "TimStart": "{}0101".format(periods[0][0]),
        "TimEnd": "{}1231".format(periods[0][1]),
        "Format": "idf",
        "AgrInp": join(agricom_dir, "inputfiles"),
        "DirInp": "",
        "DirOut": "",
        "DirOutYear": "",
        "idf_svat": True,
        "zoutschade": "ja",
        "beregening": "ja",
        "Exe": join(agricom_dir, "AGRICOM_LHM340.exe"),
    }
    if do_salt:
        agricomdict["zoutschade"] = "ja"
    # if nodata_buffer is not None:
    #    agricomdict['idf_svat'] = True
    if not do_salt and "Salt" in do_steps:
        do_steps.remove("Salt")

    if not exists(resultdir):
        os.mkdir(resultdir)

    for period in periods:
        agricomdict["TimStart"] = "{}0101".format(period[0])
        agricomdict["TimEnd"] = "{}1231".format(period[1])

        #### STEP 1: copy necessary files to MetBeregening
        if "WithSprinkling" in do_steps:
            print("EXECUTING With Sprinkling...")
            dir_to = dir_withsprinkling

            if not exists(dir_to):
                os.mkdir(dir_to)
            for prm in prms:  # loop through parameters
                if not exists(join(dir_to, prm)):
                    os.mkdir(join(dir_to, prm))
            if not exists(dir_store):
                os.mkdir(dir_store)
            if not exists(join(dir_store, "msw_Tpot")):
                os.mkdir(join(dir_store, "msw_Tpot"))

            # COPY METASWAP FILES
            for y in range(period[0], period[1] + 1):
                with get_runzip(y, bpscen) as runzip:
                    for prm in prms:  # loop through parameters
                        print(prm)
                        fnto = join(dir_to, prm)
                        decm1 = None
                        for idec, dec in get_timesteps(
                            y, start_or_end="end"
                        ):  # loop through all dates within period
                            fnfrom = join(
                                "Metaswap",
                                filepatterns[prm].replace("<DATE>", str(dec)),
                            )
                            if prm == "transol_con_rz":
                                fnfrom = fnfrom.replace(".idf", "_l1.idf")
                            if overwrite or not exists(
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                )
                            ):
                                try:
                                    zi = runzip.getinfo(zipfn(fnfrom))
                                    zi.filename = basename(zi.filename)
                                    runzip.extract(zi, fnto)
                                    if prm == "transol_con_rz":
                                        move(
                                            join(dir_to, prm, basename(fnfrom)),
                                            join(
                                                dir_to,
                                                filepatterns[prm].replace(
                                                    "<DATE>", str(dec)
                                                ),
                                            ),
                                        )
                                except KeyError:
                                    # hack for transol files in leap years
                                    copy(
                                        join(
                                            dir_to,
                                            filepatterns[prm].replace(
                                                "<DATE>", str(decm1)
                                            ),
                                        ),
                                        join(
                                            dir_to,
                                            filepatterns[prm].replace(
                                                "<DATE>", str(dec)
                                            ),
                                        ),
                                    )
                                # set correct extent
                                # enlarge(join(dir_to,filepatterns[prm].replace('<DATE>',str(dec))))
                                if prm == "msw_Tpot":
                                    copy(
                                        join(
                                            dir_to,
                                            filepatterns[prm].replace(
                                                "<DATE>", str(dec)
                                            ),
                                        ),
                                        join(
                                            dir_store,
                                            filepatterns[prm].replace(
                                                "<DATE>", str(dec)
                                            ),
                                        ),
                                    )
                            decm1 = dec

            with get_runzip(y, bpscen) as runzip:
                # overall simgro files: mod2svat, area_svat, scap_svat, svat_per.key remain the same, only svat_per.tim needs updating
                for fn in [
                    "mod2svat.inp",
                    "area_svat.inp",
                    "scap_svat.inp",
                ]:  # , 'svat_per.key']:
                    fnfrom = join("Metaswap", fn)
                    if overwrite or not exists(join(dir_to, fn)):
                        print(fnfrom)
                        zi = runzip.getinfo(zipfn(fnfrom))
                        zi.filename = basename(zi.filename)
                        runzip.extract(zi, dir_to)
            # svat_per.key and idf_svat
            for fn in ["svat_per.key", "idf_svat.inp"]:
                if not exists(join(dir_to, fn)):
                    copy(join(svat_dir, fn), join(dir_to, fn))

            # create new svat_per.tim for entire period
            with open(join(dir_to, "svat_per.tim"), "w") as f:
                for idec, dec in get_timesteps(*period, start_or_end="start"):
                    f.write("{:5d}.000000  {}\n".format(*doy(dec)))
                f.write("{:5d}.000000  {}\n".format(0, period[1] + 1))

            if "ExtBuffer" not in do_steps:
                if overwrite or not exists(
                    join(dir_to, "grids", "droogteschade_{}1231.idf".format(period[1]))
                ):
                    # RUN AGRICOM
                    agricomdict["DirInp"] = dir_to
                    agricomdict["DirOut"] = join(dir_to, "grids")
                    agricomdict["beregening"] = "ja"
                    agricom_ini = join(resultdir, "agricom_withsprinkling.inp")
                    run_agricom(agricom_ini, agricomdict)

                # CREATE AND STORE MAPS ABOUT AGRICOM LANDUSE FOR FUTURE USE
                # reclass landuse to agricom landuse
                luse = idfload(join(dir_to, "grids", "landgebruik.idf"))
                luse.values = np.vectorize(reclass_lhm_agricom.get)(luse.values)
                idfwrite(join(dir_to, "grids", "agricom_luse.idf"), luse)

                # get start and end decade of crops
                start_dec = luse.copy(deep=True)
                start_dec.values = np.vectorize(lambda x: cropcalendar[x][0])(
                    start_dec.values
                )
                idfwrite(join(dir_to, "grids", "agricom_startdec.idf"), start_dec)

                end_dec = luse.copy(deep=True)
                end_dec.values = np.vectorize(lambda x: cropcalendar[x][1])(
                    end_dec.values
                )
                idfwrite(join(dir_to, "grids", "agricom_enddec.idf"), end_dec)

        #### STEP 2: Recalculate Tact for water applied during sprinkling
        if "NoSprinkling" in do_steps:
            print("EXECUTING No Sprinkling...")
            dir_from = dir_withsprinkling
            dir_to = dir_nosprinkling

            if not exists(dir_to):
                os.mkdir(dir_to)
            for prm in prms + ["Tred"]:  # loop through parameters
                if not exists(join(dir_to, prm)):
                    os.mkdir(join(dir_to, prm))
            if not exists(dir_store):
                os.mkdir(dir_store)
            if not exists(join(dir_store, "msw_Tact")):
                os.mkdir(join(dir_store, "msw_Tact"))

            iPstot_tmin1 = None
            if overwrite or not exists(
                join(
                    dir_to,
                    filepatterns["msw_Trel"].replace("<DATE>", str(period[1]) + "1231"),
                )
            ):
                Tpot = idfload(join(dir_from, "msw_Tpot", "msw_Tpot*.idf")) * -1000.0
                Tact = idfload(join(dir_from, "msw_Tact", "msw_Tact*.idf")) * -1000.0
                Tred = Tpot - Tact
                Psgw = idfload(join(dir_from, "bdgPsgw", "bdgPsgw*.idf")) * 1000.0
                Pssw = idfload(join(dir_from, "bdgPssw", "bdgPssw*.idf")) * 1000.0
                Pstot = Psgw + Pssw
                start_dec = idfload(
                    join(dir_withsprinkling, "grids", "agricom_startdec.idf")
                ).sel(y=Tpot.y, x=Tpot.x)
                end_dec = idfload(
                    join(dir_withsprinkling, "grids", "agricom_enddec.idf")
                ).sel(y=Tpot.y, x=Tpot.x)

                for idec, dec in get_timesteps(
                    *period, start_or_end="end", dec_or_days=dec_or_days
                ):
                    print(idec, dec)
                    iPstot = Pstot.isel(time=idec, layer=0)
                    iTred = Tred.isel(time=idec, layer=0)
                    if dec_year(idec) == 0:
                        iPstot_tmin1 = iPstot.copy(deep=True) * 0.0  # Reset every year
                    if iPstot.sum().values > 0:
                        # correct Tact for sprinkling
                        iTpot = Tpot.isel(time=idec, layer=0)
                        iTact = Tact.isel(time=idec, layer=0)
                        iPstot += iPstot_tmin1  # add previous timestep leftover
                        iPstot = iPstot.where(
                            dec_year(idec) + 1 >= start_dec, 0.0
                        )  # set to zero outside growing season
                        iPstot = iPstot.where(dec_year(idec) + 1 <= end_dec, 0.0)

                        iPsapp = iPstot.copy() * spr_eff
                        iPsapp = iPsapp.where(iPsapp < iTact, iTact.values)
                        iPstot_tmin1 = (
                            iPstot - iPsapp / spr_eff
                        )  # keep sprinkling amount for next timestep  => dit deed Martin niet

                        # idfwrite(join(dir_nosprinkling,'iPstot_tmin1{:d}_l1.idf'.format(dec)), iPstot_tmin1)
                        print(
                            "leftover sprinkling: %.0f of %.0f total mm"
                            % (iPstot_tmin1.sum().values, iPstot.sum().values)
                        )
                        iTact -= iPsapp
                        iPstot *= 0.0
                        iTrel = iTact / iTpot
                        # break

                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["msw_Tact"].replace("<DATE>", str(dec)),
                            ),
                            iTact / -1000.0,
                        )
                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["msw_Trel"].replace("<DATE>", str(dec)),
                            ),
                            iTrel,
                        )
                        idfwrite(
                            join(
                                dir_to, filepatterns["Tred"].replace("<DATE>", str(dec))
                            ),
                            iTred,
                        )
                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["bdgPsgw"].replace("<DATE>", str(dec)),
                            ),
                            iPstot,
                        )
                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["bdgPssw"].replace("<DATE>", str(dec)),
                            ),
                            iPstot,
                        )
                    else:
                        # copy files
                        for prm in ["msw_Tact", "msw_Trel", "bdgPsgw", "bdgPssw"]:
                            copy(
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )
                        idfwrite(
                            join(
                                dir_to, filepatterns["Tred"].replace("<DATE>", str(dec))
                            ),
                            iTred,
                        )
                    # copy to store
                    copy(
                        join(
                            dir_to, filepatterns["msw_Tact"].replace("<DATE>", str(dec))
                        ),
                        join(
                            dir_store,
                            filepatterns["msw_Tact"].replace("<DATE>", str(dec)),
                        ),
                    )

            # Done, run Agricom
            if overwrite or not exists(
                join(
                    dir_nosprinkling,
                    "grids",
                    "droogteschade_{}1231.idf".format(period[1]),
                )
            ):
                # copy/move unchanged files
                for prm in prms_unchanged:
                    print(prm)
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        if overwrite or not exists(
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec)))
                        ):
                            move(
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )

                for fn in simgro_files:
                    fnfrom = join(dir_from, fn)
                    fnto = join(dir_to, fn)
                    if overwrite or not exists(fnto):
                        print(fnfrom)
                        copy(fnfrom, fnto)

                # RUN AGRICOM
                agricomdict["DirInp"] = dir_to
                agricomdict["DirOut"] = join(dir_to, "grids")
                agricomdict["beregening"] = "ja"
                agricom_ini = join(resultdir, "agricom_nosprinkling.inp")
                # write_agricom_ini(agricom_ini, agricomdict)
                run_agricom(agricom_ini, agricomdict)

                # move other files back
                for prm in prms_unchanged:
                    print(prm)
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        move(
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec))),
                            join(
                                dir_from, filepatterns[prm].replace("<DATE>", str(dec))
                            ),
                        )

        ##############################################################################################
        #### STEP 3: Buffer, recalculate Tact for water applied during sprinkling from Buffer
        #### STEPS 1 AND 2 MUST HAVE BEEN EXECUTED FOR ENTIRE PERIOD BEFORE RUNNING THIS STEP
        ##############################################################################################
        if "ExtBuffer" in do_steps:
            print("EXECUTING ExtBuffer...")
            dir_from = dir_withsprinkling
            if "NoSprinkling" in do_steps:
                dir_from = dir_nosprinkling

            start_dec = idfload(join(dir_store, "agricom_startdec.idf"))
            end_dec = idfload(join(dir_store, "agricom_enddec.idf"))

            if not exists(join(dir_store, "Tred")):
                os.mkdir(join(dir_store, "Tred"))

            for bufpct in extbuffer_percentages:
                print("ExtBuffer of size percentile {}".format(bufpct))

                dir_to = f"{dir_buffer}_{bufpct:03d}"
                if not exists(dir_to):
                    os.mkdir(dir_to)
                for prm in prms + ["Qappl", "Tred", "Tbuf"]:  # loop through parameters
                    if not exists(join(dir_to, prm)):
                        os.mkdir(join(dir_to, prm))

                # loop through years and use buffer for reducing Tred
                if overwrite or not exists(
                    join(
                        dir_to,
                        filepatterns["msw_Trel"].replace(
                            "<DATE>", str(period[1]) + "1231"
                        ),
                    )
                ):

                    Tpot = (
                        idfload(join(dir_store, "msw_Tpot", "msw_Tpot*.idf")) * -1000.0
                    )
                    Tact = (
                        idfload(join(dir_store, "msw_Tact", "msw_Tact*.idf")) * -1000.0
                    )  # tact is coming from store
                    start_dec = start_dec.sel(y=Tpot.y, x=Tpot.x)
                    end_dec = end_dec.sel(y=Tpot.y, x=Tpot.x)

                    # set Tred to zero outside Agricom growing season
                    if not exists(
                        join(
                            dir_store,
                            filepatterns["Tred"].replace(
                                "<DATE>", str(yearend) + "1220"
                            ),
                        )
                    ):  # somehow last decade doesn't work...
                        print("Correct Tred")
                        Tred = Tpot - Tact
                        for idec, dec in get_timesteps(
                            yearstart,
                            yearend,
                            start_or_end="end",
                            dec_or_days=dec_or_days,
                        ):
                            inseason = xr.zeros_like(start_dec).where(
                                dec_year(idec) + 1 < start_dec, 1.0
                            )
                            inseason = inseason.where(
                                dec_year(idec) + 1 <= end_dec, 0.0
                            )
                            iTred = Tred.isel(time=idec, layer=0).load()
                            iTred = iTred.where(inseason == 1.0, 0.0)
                            """
                            inseason = (start_dec.values > dec_year(idec)+1)&(end_dec.values <= dec_year(idec)+1)
                            iTred = Tred[idec,0].load()
                            iTred.values *= inseason.astype(int)"""
                            idfwrite(
                                join(
                                    dir_store,
                                    filepatterns["Tred"].replace("<DATE>", str(dec)),
                                ),
                                iTred,
                            )
                    Tred = idfload(join(dir_store, "Tred", "Tred*.idf"))

                    # calculate necessary percentile
                    if overwrite or not exists(
                        join(dir_to, "Tred_{:03d}.idf".format(bufpct))
                    ):
                        print("Annual sum Tred")
                        Tred_year = Tred.groupby("time.year").sum(dim="time")

                        print("Calculate percentile")
                        # Tbuffer = Tred_year.quantile(bufpct/100.,dim='year')[0]  SLOW...
                        Tbuffer = xr.ones_like(Tpot.isel(time=0, layer=0))
                        print("Tbuffer:", Tbuffer)
                        print("Tred_year:", Tred_year)
                        Tbuffer.values = np.percentile(
                            Tred_year.isel(layer=0).values, q=bufpct, axis=0
                        )
                        idfwrite(
                            join(dir_to, "Tred_{:03d}.idf".format(bufpct)), Tbuffer
                        )
                    else:
                        Tbuffer = idfload(
                            join(dir_to, "Tred_{:03d}.idf".format(bufpct))
                        )
                    buftot = Tbuffer.sum().values

                    # calculate new Tred using the calculated buffer
                    # for maps in store, idec is not the right index, correct for year in period vs year in total period
                    ideccorr = (period[0] - yearstart) * 36
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        print(idec, dec)

                        # new year: reset buffer:
                        if dec_year(idec) == 0:
                            Tbufyear = Tbuffer.copy(deep=True)

                        iTred = Tred.isel(time=idec + ideccorr, layer=0)
                        iTpot = Tpot.isel(time=idec + ideccorr, layer=0)
                        iTact = Tact.isel(time=idec + ideccorr, layer=0)

                        # correct Tact for water applied from buffer
                        # applied water = min(Tred, Tbuffer)
                        # appl = xr.zeros_like(Tbuffer).where(iTred<=Tbuffer,iTred,Tbuffer)
                        appl = iTred.where(iTred <= Tbufyear, Tbufyear)
                        appl = appl.where(
                            dec_year(idec) + 1 >= start_dec, 0.0
                        )  # limit to growing season
                        appl = appl.where(dec_year(idec) + 1 <= end_dec, 0.0)

                        iTred = iTred - appl
                        iTact = iTact + appl
                        Tbufyear = Tbufyear - appl

                        print(
                            "Applied %.0f m. Leftover buffer: %.0f of %.0f total m"
                            % (
                                appl.sum().values / 1000.0,
                                Tbufyear.sum().values / 1000.0,
                                buftot / 1000.0,
                            )
                        )

                        iTrel = iTact / iTpot

                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["msw_Tact"].replace("<DATE>", str(dec)),
                            ),
                            iTact / -1000.0,
                        )
                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["msw_Trel"].replace("<DATE>", str(dec)),
                            ),
                            iTrel,
                        )
                        if store_tmp:
                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["Qappl"].replace("<DATE>", str(dec)),
                                ),
                                appl,
                            )
                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["Tred"].replace("<DATE>", str(dec)),
                                ),
                                iTred,
                            )
                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["Tbuf"].replace("<DATE>", str(dec)),
                                ),
                                Tbufyear,
                            )

                # Done, run Agricom
                if overwrite or not exists(
                    join(dir_to, "grids", "droogteschade_{}1231.idf".format(period[1]))
                ):
                    # move other files
                    for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                        print(prm)
                        for idec, dec in get_timesteps(
                            *period, start_or_end="end", dec_or_days=dec_or_days
                        ):
                            if overwrite or not exists(
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                )
                            ):
                                move(
                                    join(
                                        dir_from,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                    join(
                                        dir_to,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                )

                    for fn in simgro_files:
                        fnfrom = join(dir_from, fn)
                        fnto = join(dir_to, fn)
                        if overwrite or not exists(fnto):
                            print(fnfrom)
                            copy(fnfrom, fnto)

                    # RUN AGRICOM
                    agricomdict["DirInp"] = dir_to
                    agricomdict["DirOut"] = join(dir_to, "grids")
                    agricomdict["beregening"] = "nee"
                    agricom_ini = join(
                        resultdir, "agricom_buffer_{}.inp".format(bufpct)
                    )
                    # write_agricom_ini(agricom_ini, agricomdict)
                    run_agricom(agricom_ini, agricomdict)

                    # move other files back
                    for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                        print(prm)
                        for idec, dec in get_timesteps(
                            *period, start_or_end="end", dec_or_days=dec_or_days
                        ):
                            move(
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )

        ##############################################################################################
        #### STEP 4: Soilbuffer, recalculate Tact for water applied during sprinkling from SoilBuffer.
        ####         Refill soil buffer with excess precip
        ##############################################################################################
        if "SoilBuffer" in do_steps:
            print("EXECUTING SoilBuffer...")
            dir_from = dir_withsprinkling

            Tpot = (
                idfload(join(dir_withsprinkling, "msw_Tpot", "msw_Tpot*.idf")) * -1000.0
            )
            Tact = (
                idfload(join(dir_nosprinkling, "msw_Tact", "msw_Tact*.idf")) * -1000.0
            )
            Pm = idfload(join(dir_withsprinkling, "bdgPm", "bdgPm_*.idf")) * 1000.0
            start_dec = idfload(
                join(dir_withsprinkling, "grids", "agricom_startdec.idf")
            ).sel(y=Tpot.y, x=Tpot.x)
            end_dec = idfload(
                join(dir_withsprinkling, "grids", "agricom_enddec.idf")
            ).sel(y=Tpot.y, x=Tpot.x)
            Tred = Tpot - Tact

            for bufmm in soilbuffer_mms:
                print(f"SoilBuffer of size {bufmm} mm")

                dir_to = f"{dir_buffer}_{bufmm:02d}mm"
                if not exists(dir_to):
                    os.mkdir(dir_to)
                for prm in prms + [
                    "Qappl",
                    "Qfill",
                    "Tred",
                    "Tbuf",
                ]:  # loop through parameters
                    if not exists(join(dir_to, prm)):
                        os.mkdir(join(dir_to, prm))

                # calculate necessary buffer
                Tbuffer = xr.ones_like(Tred[0, 0]) * float(bufmm)
                idfwrite(join(dir_to, "Tred_{:02d}mm.idf".format(bufmm)), Tbuffer)
                buftot = Tbuffer.sum().values

                # loop through years and use buffer for reducing Tred
                if overwrite or not exists(
                    join(
                        dir_to,
                        filepatterns["msw_Trel"].replace(
                            "<DATE>", str(period[1] * 10000 + 1231)
                        ),
                    )
                ):
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        print(idec, dec)

                        # new year: reset buffer:
                        if dec_year(idec) == 0:
                            Tbufyear = Tbuffer.copy(deep=True)

                        iTred = Tred.isel(time=idec, layer=0).load()
                        # set Tred to zero outside growing season
                        inseason = xr.zeros_like(start_dec).where(
                            dec_year(idec) + 1 < start_dec, 1.0
                        )
                        inseason = inseason.where(dec_year(idec) + 1 <= end_dec, 0.0)
                        iTred = iTred.where(inseason == 1.0, 0.0)

                        if (
                            iTred.sum().values > 0
                            or Tbufyear.sum().values < buftot - 1.0
                        ):
                            iTpot = Tpot.isel(time=idec, layer=0)
                            iTact = Tact.isel(time=idec, layer=0)
                            iPm = Pm.isel(time=idec, layer=0)

                            # correct Tact for water applied from buffer
                            # applied water = min(Tred, Tbuffer)
                            # appl = xr.zeros_like(Tbuffer).where(iTred<=Tbuffer,iTred,Tbuffer)
                            appl = iTred.where(iTred <= Tbufyear, Tbufyear)
                            appl = appl.where(
                                dec_year(idec) + 1 >= start_dec, 0.0
                            )  # limit to growing season
                            appl = appl.where(dec_year(idec) + 1 <= end_dec, 0.0)

                            iTred = iTred - appl
                            iTact = iTact + appl
                            Tbufyear = Tbufyear - appl

                            print(
                                "Applied %.0f m. Leftover buffer: %.0f of %.0f total m"
                                % (
                                    appl.sum().values / 1000.0,
                                    Tbufyear.sum().values / 1000.0,
                                    buftot / 1000.0,
                                )
                            )

                            # refill buffer, when:
                            # - Tred == 0
                            # - Pm - Tpot > 0
                            # - Tbuf < bufmm
                            # negeert kwel, zijdelingse afstroming, etc
                            # en, wellicht nog belangrijker: alle neerslag vult extra buffer aan, terwijl gehele bodem leeg was.
                            # daarvoor een standaard store van 30 mm aangenomen. De extra buffer vult evenredig met de hele store
                            fill = (iPm - iTpot) * Tbuffer / (30 * Tbuffer)
                            fill = fill.where(fill > 0.0, 0.0)
                            fill = fill.where(Tred[idec, 0] == 0.0, 0.0)
                            Tbufred = Tbuffer - Tbufyear
                            fill = fill.where(fill < Tbufred, Tbufred)

                            Tbufyear = Tbufyear + fill
                            print(
                                "Refilled buffer with %.0f m. Leftover buffer: %.0f of %.0f max m"
                                % (
                                    fill.sum().values / 1000.0,
                                    Tbufyear.sum().values / 1000.0,
                                    buftot / 1000.0,
                                )
                            )

                            iTrel = iTact / iTpot

                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["msw_Tact"].replace(
                                        "<DATE>", str(dec)
                                    ),
                                ),
                                iTact / -1000.0,
                            )
                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["msw_Trel"].replace(
                                        "<DATE>", str(dec)
                                    ),
                                ),
                                iTrel,
                            )
                            if store_tmp:
                                idfwrite(
                                    join(
                                        dir_to,
                                        filepatterns["Qappl"].replace(
                                            "<DATE>", str(dec)
                                        ),
                                    ),
                                    appl,
                                )
                                idfwrite(
                                    join(
                                        dir_to,
                                        filepatterns["Qfill"].replace(
                                            "<DATE>", str(dec)
                                        ),
                                    ),
                                    fill,
                                )
                                idfwrite(
                                    join(
                                        dir_to,
                                        filepatterns["Tbuf"].replace(
                                            "<DATE>", str(dec)
                                        ),
                                    ),
                                    Tbufyear,
                                )
                                idfwrite(
                                    join(
                                        dir_to,
                                        filepatterns["Tred"].replace(
                                            "<DATE>", str(dec)
                                        ),
                                    ),
                                    iTred,
                                )
                        else:
                            # copy files
                            for prm in ["msw_Tact", "msw_Trel"]:
                                copy(
                                    join(
                                        dir_from,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                    join(
                                        dir_to,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                )

                # Done, run Agricom
                if overwrite or not exists(
                    join(dir_to, "grids", "droogteschade_{}1231.idf".format(period[1]))
                ):
                    # move other files
                    for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                        print(prm)
                        for idec, dec in get_timesteps(
                            *period, start_or_end="end", dec_or_days=dec_or_days
                        ):
                            if overwrite or not exists(
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                )
                            ):
                                move(
                                    join(
                                        dir_from,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                    join(
                                        dir_to,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                )

                    for fn in simgro_files:
                        fnfrom = join(dir_from, fn)
                        fnto = join(dir_to, fn)
                        if overwrite or not exists(fnto):
                            print(fnfrom)
                            copy(fnfrom, fnto)

                    # RUN AGRICOM
                    agricomdict["DirInp"] = dir_to
                    agricomdict["DirOut"] = join(dir_to, "grids")
                    agricomdict["beregening"] = "nee"
                    agricom_ini = join(
                        resultdir, "agricom_buffer_{:02d}mm.inp".format(bufmm)
                    )
                    # write_agricom_ini(agricom_ini, agricomdict)
                    run_agricom(agricom_ini, agricomdict)

                    # move other files back
                    for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                        print(prm)
                        for idec, dec in get_timesteps(
                            *period, start_or_end="end", dec_or_days=dec_or_days
                        ):
                            move(
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )

        ##############################################################################################
        #### STEP 5: Proportional, recalculate Tact for proportionally reduced transpiration reduction
        ##############################################################################################
        if "Proportional" in do_steps:
            print("EXECUTING Proportional...")
            dir_from = dir_withsprinkling

            Tpot = (
                idfload(join(dir_withsprinkling, "msw_Tpot", "msw_Tpot*.idf")) * -1000.0
            )
            Tact = (
                idfload(join(dir_nosprinkling, "msw_Tact", "msw_Tact*.idf")) * -1000.0
            )
            start_dec = idfload(
                join(dir_withsprinkling, "grids", "agricom_startdec.idf")
            ).sel(y=Tpot.y, x=Tpot.x)
            end_dec = idfload(
                join(dir_withsprinkling, "grids", "agricom_enddec.idf")
            ).sel(y=Tpot.y, x=Tpot.x)
            Tred = Tpot - Tact

            for pct in proportional_pcts:
                print(f"Proportional reduction of {pct} %")

                dir_to = f"{dir_proportional}_{pct:03d}"
                if not exists(dir_to):
                    os.mkdir(dir_to)
                for prm in prms + ["Tred"]:  # loop through parameters
                    if not exists(join(dir_to, prm)):
                        os.mkdir(join(dir_to, prm))

                # loop through years and reduce Tred proportionally
                if overwrite or not exists(
                    join(
                        dir_to,
                        filepatterns["msw_Trel"].replace(
                            "<DATE>", str(period[1] * 10000 + 1231)
                        ),
                    )
                ):
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        print(idec, dec)

                        iTred = Tred.isel(time=idec, layer=0).load()
                        # set Tred to zero outside growing season
                        inseason = xr.zeros_like(start_dec).where(
                            dec_year(idec) + 1 < start_dec, 1.0
                        )
                        inseason = inseason.where(dec_year(idec) + 1 <= end_dec, 0.0)
                        iTred = iTred.where(inseason == 1.0, 0.0)
                        if iTred.sum().values > 0:
                            iTpot = Tpot.isel(time=idec, layer=0)
                            iTact = Tact.isel(time=idec, layer=0)

                            # correct Tact for reduced reduction
                            iTact = iTact + (iTred * pct / 100.0)
                            iTred = iTred * (1.0 - pct / 100.0)
                            iTrel = iTact / iTpot

                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["msw_Tact"].replace(
                                        "<DATE>", str(dec)
                                    ),
                                ),
                                iTact / -1000.0,
                            )
                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["msw_Trel"].replace(
                                        "<DATE>", str(dec)
                                    ),
                                ),
                                iTrel,
                            )
                            if store_tmp:
                                idfwrite(
                                    join(
                                        dir_to,
                                        filepatterns["Tred"].replace(
                                            "<DATE>", str(dec)
                                        ),
                                    ),
                                    iTred,
                                )
                        else:
                            # copy files
                            for prm in ["msw_Tact", "msw_Trel"]:
                                copy(
                                    join(
                                        dir_from,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                    join(
                                        dir_to,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                )

                # Done, run Agricom
                if overwrite or not exists(
                    join(dir_to, "grids", "droogteschade_{}1231.idf".format(period[1]))
                ):
                    # move other files
                    for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                        print(prm)
                        for idec, dec in get_timesteps(
                            *period, start_or_end="end", dec_or_days=dec_or_days
                        ):
                            if overwrite or not exists(
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                )
                            ):
                                move(
                                    join(
                                        dir_from,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                    join(
                                        dir_to,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                )

                    for fn in simgro_files:
                        fnfrom = join(dir_from, fn)
                        fnto = join(dir_to, fn)
                        if overwrite or not exists(fnto):
                            print(fnfrom)
                            copy(fnfrom, fnto)

                    # RUN AGRICOM
                    agricomdict["DirInp"] = dir_to
                    agricomdict["DirOut"] = join(dir_to, "grids")
                    agricomdict["beregening"] = "nee"
                    agricom_ini = join(
                        resultdir, "agricom_proportional_{}.inp".format(pct)
                    )
                    # write_agricom_ini(agricom_ini, agricomdict)
                    run_agricom(agricom_ini, agricomdict)

                    # move other files back
                    for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                        print(prm)
                        for idec, dec in get_timesteps(
                            *period, start_or_end="end", dec_or_days=dec_or_days
                        ):
                            move(
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )

        ##############################################################################################
        #### STEP 6: Salt, recalculate salt damage for different rootzone concentrations
        ##############################################################################################
        if "Salt" in do_steps:
            print("EXECUTING Salt...")
            dir_from = dir_withsprinkling

            for conc in salt_concentrations:
                print(f"Salt concentration of {conc} mg/l")

                dir_to = f"{dir_salt}_{conc}"
                if not exists(dir_to):
                    os.mkdir(dir_to)
                for prm in prms:  # create subdirs
                    if not exists(join(dir_to, prm)):
                        os.mkdir(join(dir_to, prm))

                transol = idfload(
                    join(
                        dir_from,
                        filepatterns["transol_con_rz"].replace(
                            "<DATE>", str(period[1] * 10000 + 1231)
                        ),
                    )
                )
                iConc = xr.ones_like(transol) * conc / 1000.0  # (in g/l)
                # loop through years and write salt concentration
                if overwrite or not exists(
                    join(
                        dir_to,
                        filepatterns["transol_con_rz"].replace(
                            "<DATE>", str(period[1] * 10000 + 1231)
                        ),
                    )
                ):
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        print(idec, dec)
                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["transol_con_rz"].replace(
                                    "<DATE>", str(dec)
                                ),
                            ),
                            iConc,
                        )

                # Done, run Agricom
                if overwrite or not exists(
                    join(dir_to, "grids", "droogteschade_{}1231.idf".format(period[1]))
                ):
                    # move other files
                    prms_nosalt = prms.copy()
                    prms_nosalt.remove("transol_con_rz")
                    for prm in prms_nosalt:
                        print(prm)
                        for idec, dec in get_timesteps(
                            *period, start_or_end="end", dec_or_days=dec_or_days
                        ):
                            if overwrite or not exists(
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                )
                            ):
                                move(
                                    join(
                                        dir_from,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                    join(
                                        dir_to,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                )

                    for fn in simgro_files:
                        fnfrom = join(dir_from, fn)
                        fnto = join(dir_to, fn)
                        if overwrite or not exists(fnto):
                            print(fnfrom)
                            copy(fnfrom, fnto)

                    # RUN AGRICOM
                    agricomdict["DirInp"] = dir_to
                    agricomdict["DirOut"] = join(dir_to, "grids")
                    agricom_ini = join(resultdir, "agricom_salt_{}.inp".format(conc))
                    # write_agricom_ini(agricom_ini, agricomdict)
                    run_agricom(agricom_ini, agricomdict)

                    # move other files back
                    for prm in prms_nosalt:
                        print(prm)
                        for idec, dec in get_timesteps(
                            *period, start_or_end="end", dec_or_days=dec_or_days
                        ):
                            move(
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )

        ##############################################################################################
        #### Finalize: create Regioscan Database from calculation results
        ##############################################################################################
        if "RegioscanDB" in do_steps:
            print("EXECUTING RegioscanDB...")

            dir_to = dir_regioscandb
            if not exists(dir_to):
                os.mkdir(dir_to)
            for subdir in [
                "Buffer",
                "Evenredig",
                "MetBeregening",
                "ZonderBeregening",
                "Zout",
            ]:  # loop through parameters
                if not exists(join(dir_to, subdir)):
                    os.mkdir(join(dir_to, subdir))

            # Met en zonder beregening
            for dir_from, dir_to in zip(
                [dir_withsprinkling, dir_nosprinkling],
                ["MetBeregening", "ZonderBeregening"],
            ):
                if exists(dir_from) and "ExtBuffer" not in do_steps:
                    dir_to = join(dir_regioscandb, dir_to)
                    tpot = idfload(join(dir_from, "grids", "Tpot_*1231.idf"))
                    tact = idfload(join(dir_from, "grids", "Tact_*1231.idf"))
                    tred = tpot - tact
                    # print(tred)
                    fns = [
                        "droogteschade",
                        "totschadefractie",
                        "Tpot",
                        "Tact",
                        "beregening_sw",
                        "beregening_gw",
                    ]  # ,'zoutschade'
                    for i, year in enumerate(range(period[0], period[1] + 1)):
                        for fn in fns:
                            copy(
                                join(
                                    dir_from, "grids", "{}_{}1231.idf".format(fn, year)
                                ),
                                dir_to,
                            )
                        idfwrite(join(dir_to, "Tred_{}1231.idf".format(year)), tred[i])

            # Buffer
            dir_to = join(dir_regioscandb, "Buffer")
            for bufpct in extbuffer_percentages:
                dir_from = f"{dir_buffer}_{bufpct:03d}"
                if exists(dir_from):
                    tpot = idfload(join(dir_from, "grids", "Tpot_*1231.idf"))
                    tact = idfload(join(dir_from, "grids", "Tact_*1231.idf"))
                    tred = tpot - tact
                    for i, year in enumerate(range(period[0], period[1] + 1)):
                        copy(
                            join(
                                dir_from,
                                "grids",
                                "droogteschade_{}1231.idf".format(year),
                            ),
                            join(
                                dir_to,
                                "droogteschade_{}_{:03d}.idf".format(year, bufpct),
                            ),
                        )
                        idfwrite(
                            join(dir_to, "Tred_{}1231_{:03d}.idf".format(year, bufpct)),
                            tred[i],
                        )
                    # enlarge Tred percentile grid
                    # copy(join(dir_from,'Tred_{:03d}.idf'.format(bufpct)),join(dir_to,'Tred_{:2d}perc.idf'.format(bufpct)))
                    tred_pct = idfload(join(dir_from, "Tred_{:03d}.idf".format(bufpct)))
                    tred_pct = imod.prepare.reproject(tred_pct, like=tred[0])
                    idfwrite(
                        join(dir_to, "Tred_{:2d}perc.idf".format(bufpct)), tred_pct
                    )

            for bufmm in soilbuffer_mms:
                dir_from = f"{dir_buffer}_{bufmm:02d}mm"
                if exists(dir_from):
                    tpot = idfload(join(dir_from, "grids", "Tpot_*1231.idf"))
                    tact = idfload(join(dir_from, "grids", "Tact_*1231.idf"))
                    tred = tpot - tact
                    for i, year in enumerate(range(period[0], period[1] + 1)):
                        copy(
                            join(
                                dir_from,
                                "grids",
                                "droogteschade_{}1231.idf".format(year),
                            ),
                            join(
                                dir_to,
                                "droogteschade_{}_{:02d}mm.idf".format(year, bufmm),
                            ),
                        )
                        idfwrite(
                            join(
                                dir_to, "Tred_{}1231_{:02d}mm.idf".format(year, bufmm)
                            ),
                            tred[i],
                        )

            # Evenredig
            dir_to = join(dir_regioscandb, "Evenredig")
            for pct in proportional_pcts:
                dir_from = f"{dir_proportional}_{pct:03d}"
                if exists(dir_from):
                    for year in range(period[0], period[1] + 1):
                        copy(
                            join(
                                dir_from,
                                "grids",
                                "droogteschade_{}1231.idf".format(year),
                            ),
                            join(
                                dir_to, "droogteschade_{}_{:03d}.idf".format(year, pct)
                            ),
                        )

            # Salt
            dir_to = join(dir_regioscandb, "Zout")
            for conc in salt_concentrations:
                dir_from = f"{dir_salt}_{conc}"
                if exists(dir_from):
                    for year in range(period[0], period[1] + 1):
                        copy(
                            join(
                                dir_from, "grids", "zoutschade_{}1231.idf".format(year)
                            ),
                            join(dir_to, "zoutschade_{}_{:04d}.idf".format(year, conc)),
                        )

        ##############################################################################################
        #### Cleanup: delete temporary calculation results
        ##############################################################################################
        if "Cleanup" in do_steps:
            print("EXECUTING Cleanup...")

            def cleanup_dir(dir_to, rmdir=False):
                if exists(dir_to):
                    if rmdir:
                        rmtree(dir_to)
                    else:
                        for prm in filepatterns.keys():
                            for idec, dec in get_timesteps(
                                *period, start_or_end="end", dec_or_days=dec_or_days
                            ):
                                fn = join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                )
                                if exists(fn):
                                    os.remove(fn)
                        for fn in simgro_files:
                            if exists(join(dir_to, fn)):
                                os.remove(join(dir_to, fn))
                        if exists(join(dir_to, "grids")):
                            for year in range(period[0], period[1] + 1):
                                for fn in glob.glob(
                                    join(dir_to, "grids", "*{}1231.idf".format(year))
                                ):
                                    os.remove(fn)
                            for fn in [
                                "areaal.idf",
                                "beregeningstype.idf",
                                "bodemtype.idf",
                                "landgebruik.idf",
                            ]:
                                if exists(join(dir_to, "grids", fn)):
                                    os.remove(join(dir_to, "grids", fn))

            rmdir = period[1] == yearend

            # First save NoSprinkling Tact
            if not exists(dir_store):
                os.mkdir(dir_store)
            for fn in ["agricom_startdec.idf", "agricom_enddec.idf"]:
                if not exists(join(dir_store, fn)):
                    move(join(dir_withsprinkling, "grids", fn), dir_store)

            # Met en zonder beregening
            cleanup_dir(dir_withsprinkling, rmdir)
            cleanup_dir(dir_nosprinkling, rmdir)

            # Buffer
            for bufpct in extbuffer_percentages:
                dir_bufpct = f"{dir_buffer}_{bufpct:03d}"
                cleanup_dir(dir_bufpct, rmdir)

            for bufmm in soilbuffer_mms:
                dir_bufmm = f"{dir_buffer}_{bufmm:02d}mm"
                cleanup_dir(dir_bufmm, rmdir)

            # Evenredig
            for pct in proportional_pcts:
                dir_pct = f"{dir_proportional}_{pct:03d}"
                cleanup_dir(dir_pct, rmdir)

            # Salt
            for conc in salt_concentrations:
                dir_conc = f"{dir_salt}_{conc}"
                cleanup_dir(dir_conc, rmdir)

        with open("done.txt", "w") as f:
            f.write("done")
