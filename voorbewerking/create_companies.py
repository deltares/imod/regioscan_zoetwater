# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 09:39:55 2019

@author: delsman

Script to create modelbedrijven and bedrijfstypen grids from WUR_EcR data
"""
from pathlib import Path
from imod.idf import load as idfopen, write as idfwrite
import pandas as pd
import xarray as xr
import numpy as np

# import random

scen = "S2050BP18"  #'REF2017BP18'
basedir = Path(
    r"c:\Users\delsman\OneDrive - Stichting Deltares\Documents\Regioscan Zoetwatermaatregelen\Landelijke maatregeldatabase"
)
fnlgn = basedir / "Basiskaarten" / scen / "landgebruik.idf"
fnLB66 = basedir / "Bedrijfstypen" / "LBG.IDF"
fnxlsx = basedir / "Bedrijfstypen" / "Regioscan_22jan_def_pr_4 feb15.15 uur.xlsx"
resultdir = basedir / "Resultaatkaarten" / scen
if not resultdir.exists():
    resultdir.mkdir()
""" 
# STEP 1: create database of modelbedrijven per LB area
print ('STEP 1: Create database of modelbedrijven per LB area')
xlsx = pd.ExcelFile(fnxlsx)
sheetnames = xlsx.sheet_names
lbs = []
cols = None
for sh in sheetnames:
    if sh == 'Bedrijfstypen':
        comptype = pd.read_excel(xlsx,sh,index_col=0,names=['Bedrijfstype'])
    else:
        # een lb gebied
        lb = pd.read_excel(xlsx,sh)
        if cols is None:
            cols = lb.columns
        lb66_bt = lb['lb66_bt'].dropna()
        LB66 = lb['LB66'].dropna()
        # zoek naar 'model' kolom 'bt_new', tot 'tot'
        s = lb.loc[(lb['bt_new']=='model')|(lb['bt_new']=='Model')].index[0]+1
        e = lb.loc[(lb['bt_new']=='tot')|(lb['bt_new']=='Tot')].index[-1]-1
        lb2 = lb.loc[s:e, cols].reset_index(drop=True)
        lb2.loc[:,'lb66_bt'] = lb66_bt
        lb2.loc[:,'LB66'] = LB66
        lbs += [lb2.astype(int)]
lbs = pd.concat(lbs,ignore_index=True)        
lbs.to_csv(resultdir / 'LB_bedrijfstype.csv')
comptype.to_csv(resultdir / 'Bedrijfstype.csv')
"""
# STEP 2: assign crops to modelcompanies, as close together as possible
luse = idfopen(fnlgn)
LB66 = idfopen(fnLB66)
cellsize = 250.0
lbs = pd.read_csv(resultdir / "LB_bedrijfstype.csv", index_col=0)
ren = {
    "v1_gras": "c1",
    "v2_mais": "c2",
    "v3_aardappelen": "c3",
    "v4_suikerbiet": "c4",
    "v5_granen": "c5",
    "v6_overig": "c6",
    "v7_boomteelt": "c7",
    "v9_fruitteelt": "c9",
    "v10_bollen": "c10",
}
lbs = lbs.rename(columns=ren)


def unique(arr, ret_counts=False):
    un, ct = np.unique(arr, return_counts=True)
    unl = un[~np.isnan(un)].tolist()
    ctl = ct[~np.isnan(un)].tolist()
    if ret_counts:
        return unl, ctl
    else:
        return unl


def count(arr):
    d = {}
    for i in [1, 2, 3, 4, 5, 6, 7, 9, 10]:
        d[f"c{i}"] = (arr == i).sum()
    return pd.Series(d)


def get_LBluse(LB, ct, lbs):
    lb = lbs.loc[(lbs["LB66"] == LB) & (lbs["bt_new"] == ct)]
    if len(lb):
        return lb.iloc[0].to_dict()
    else:
        return None


# copy crops to farms
farms = xr.zeros_like(luse).load()
farms.values[np.isnan(luse) | np.isnan(LB66)] = np.nan
ctype = xr.zeros_like(luse).load()
ctype.values[np.isnan(luse) | np.isnan(LB66)] = np.nan
LB66adj = xr.zeros_like(luse).load()
LB66adj.values[np.isnan(luse) | np.isnan(LB66)] = np.nan

# STEP 2A: adjust WEcR table to fit Agricom landuse
np.random.seed(0)
cols = ["bt_new", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c9", "c10", "aantal"]
ccols = cols[1:-1]

print("STEP 2A: Adjust WEcR table to fit Agricom landuse")
lbsadj = []
for lb in unique(LB66):
    lb = int(lb)
    print(f"deelgebied {lb}")
    # bepaal aantal cellen per landgebruik/bedrijfstype volgens WEcR
    lbsi = lbs.loc[lbs["LB66"] == lb, cols].set_index("bt_new")
    lbsi = lbsi.loc[lbsi["aantal"] > 0]
    lbsisummed = (lbsi.loc[:, cols[1:-1]].T * lbsi["aantal"]).T
    lbsisum = lbsisummed.sum()

    # bepaal aantal cellen per landgebruik volgens LHM/Agricom
    lusesum = count(luse.values[LB66 == lb])

    # maak WEcR totalen evenredig gelijk aan luse totalen
    fact = lusesum[lbsisum > 0] / lbsisum[lbsisum > 0]
    lbsisummedadj = (lbsisummed * fact).fillna(0)

    # voeg missende totalen toe aan overig categorie
    if 8 in lbsi.index:
        lbsisummedadj.loc[8, lusesum[lbsisum == 0].index] = lusesum[lbsisum == 0]
    else:  # als geen overig, dan in laatste type...
        lbsisummedadj.loc[lbsi.index[-1], lusesum[lbsisum == 0].index] = lusesum[
            lbsisum == 0
        ]

    # terug naar aantal
    lbsiadj = (lbsisummedadj.T / lbsi["aantal"]).T

    # maak dataframe met modelbedrijven, vul met aantallen
    df = pd.DataFrame(
        data=0,
        index=range(lbsi["aantal"].sum()),
        columns=["LB66", "bt_new"] + ccols,
        dtype=int,
    )
    df.loc[:, "LB66"] = lb
    s = 0
    for bt in lbsi.index:
        cnt = lbsi.loc[bt, "aantal"]
        lbsia_bt = lbsiadj.loc[bt]
        e = s + cnt
        idx = df.iloc[s:e].index
        df.loc[idx, "bt_new"] = bt
        for c in ccols:
            n = lbsia_bt[c]
            if n:
                n1 = int(n)
                n2 = n1 + 1
                # create list with values above and below average ncells  TODO: check op totalen!
                l = [n2] * int((n - n1) * cnt)
                l += [n1] * (cnt - len(l))
                df.loc[idx, c] = np.random.permutation(l).astype(
                    int
                )  # ken aantal cellen random toe aan de modelbedrijven
        s = e

    # gooi lege bedrijven weg
    df = df.loc[df[ccols].sum(axis=1) > 0]

    # check op overblijvende verschillen tussen totalen
    corr = (lusesum - df.sum()).dropna()

    # ken deze random toe aan modelbedrijven met dit landgebruik
    for c in corr.loc[corr > 0].index:
        corri = corr[c] / abs(corr[c])
        for i in range(int(abs(corr[c]))):
            idx = df.loc[df[c] > 0].index
            if len(idx):
                idx = np.random.choice(idx)
            else:
                # desnoods zonder landgebruik, dan zal corri wel positief moeten zijn
                assert corri > 0
                idx = np.random.choice(df.index)

            df.loc[idx, c] += int(corri)

    # gooi lege bedrijven weg
    df = df.loc[df[ccols].sum(axis=1) > 0]

    # check op overblijvende verschillen tussen totalen
    corr = (lusesum - df.sum()).dropna()
    assert corr.sum() == 0

    lbsadj += [df]

lbsadj = pd.concat(lbsadj, ignore_index=True)
lbsadj.index += 1
lbsadj.to_csv(resultdir / "LB_bedrijfstype_aangepast.csv")

lbsadj = pd.read_csv(resultdir / "LB_bedrijfstype_aangepast.csv", index_col=0)

# STEP 2b: assign companies to luse grid
print("STEP 2B: Assign companies to landuse grid")


def cval(c):
    return int(c[1:])


xi, yi = np.indices(farms.shape)
farmid = 0
ctncells = {}
for lb in unique(LB66):
    lb = int(lb)
    print(f"deelgebied {lb}")
    #    farms.mask = farmsstart.mask | (deelgeb.arr != areaid)
    # per landgebruik vanaf 10 naar 1:
    # - welk bedrijfstype heeft die nodig?
    # voor dat bedrijfstype:
    #  - hoeveel cellen per landgebruik?
    #  - vul dichtstbijzijnde
    dfi = lbsadj.loc[lbsadj["LB66"] == lb]
    lbmap = LB66 == lb

    # do analysis only for LB
    lbluse = luse.values[lbmap]
    lbfarms = farms.values[lbmap]
    lbctype = ctype.values[lbmap]
    lbLB66adj = LB66adj.values[lbmap]
    lbxi = xi[lbmap]
    lbyi = yi[lbmap]
    idxlist = list(dfi.index)
    np.random.shuffle(idxlist)  #  random, anders bedrijfstypen teveel geclusterd
    for fi in idxlist:
        # loop modelbedrijven langs, ken landgebruik toe

        row = dfi.loc[fi]
        lstart = cval(
            row[ccols].idxmax()
        )  # find a random cell within LB66 to start from

        # determine startpoint
        # calc distances from first cell that is still 0, (=[b][0])
        # and has the right landuse!!
        b = (lbfarms == 0) & (lbluse == lstart)
        assert b.sum() > 0

        x = (lbxi - lbxi[b][0]) * cellsize
        y = (lbyi - lbyi[b][0]) * cellsize
        for c in row[ccols][row > 0].index:
            # select ncells closest cells
            dist = np.ma.array((x ** 2 + y ** 2) ** 0.5)
            dist.mask = np.isnan(lbfarms)

            # adjust distance
            dist[lbluse != cval(c)] = 1e9
            np.warnings.filterwarnings("ignore")
            dist[lbfarms > 0] = 1e9  # previously assigned

            # select the nearest ncells cells
            distsort = np.argsort(dist, axis=None)
            nearest = np.unravel_index(distsort[: row[c]], lbfarms.shape)

            # set closest cells to farmid, companytype, LB
            lbfarms[nearest] = fi
            lbctype[nearest] = row["bt_new"]
            lbLB66adj[nearest] = lb

    # paste in NL maps
    farms.values[lbmap] = lbfarms
    ctype.values[lbmap] = lbctype
    LB66adj.values[lbmap] = lbLB66adj

    # break
idfwrite(resultdir / "modelbedrijven.idf", farms, nodata=-9999.0)
idfwrite(resultdir / "bedrijftypen.idf", ctype, nodata=-9999.0)
idfwrite(resultdir / "deelgebieden.idf", LB66, nodata=-9999.0)

assert (farms.values == 0).sum() == 0
