# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 10:56:39 2018

@author: delsman

Agricom.py: utility functions for agricom

"""
from subprocess import run
from time import asctime
from os.path import join, abspath
from numpy import nan
from os import remove
from multiprocessing import Pool

cropcalendar = {
    1: [10, 27],  # Agricom landuse, decade start, decade end
    2: [13, 27],
    3: [13, 25],
    4: [13, 28],
    5: [10, 22],
    6: [10, 30],
    7: [10, 27],
    8: [1, 36],
    9: [10, 27],
    10: [14, 25],
    11: [7, 20],
    12: [10, 27],
    13: [10, 27],
    14: [1, 36],
    15: [1, 36],
    -9999: [-9999, -9999],
    nan: [nan, nan],
    None: [nan, nan],
}

reclass_lhm_agricom = {
    -9999.0: -9999.0,
    nan: nan,
    1.0: 1.0,
    2.0: 2.0,
    3.0: 3.0,
    4.0: 4.0,
    5.0: 5.0,
    6.0: 6.0,
    7.0: 7.0,
    9.0: 9.0,
    10.0: 11.0,
    21.0: 10.0,
}

agricomdict = {
    "NamZnl": "nl.idf",
    "TimStart": "20000101",
    "TimEnd": "20001231",
    "Format": "idf",
    "AgrInp": "",
    "DirInp": "",
    "DirOut": "",
    "DirOutYear": "",
    "idf_svat": False,
    "zoutschade": "ja",
    "beregening": "ja",
    "Exe": "",
}


def write_agricom_ini(fn, prmdict):
    """Write Agricom input file"""

    with open(fn, "w") as f:
        f.write("* Controlfile AGRICOM ZoetWaterOpgave\n")
        f.write("* Created at: {}\n\n".format(asctime()))
        f.write(
            "*--------------------------------------------------------------------------------------------------\n\n"
        )
        f.write("* General settings\n")
        f.write("*------------------------------------------------\n")
        f.write(
            "TimStart                                        {}\n".format(
                prmdict["TimStart"]
            )
        )
        f.write(
            "TimEnd                                          {}\n\n".format(
                prmdict["TimEnd"]
            )
        )
        f.write("OptDelTimPrn                                    OneYear\n\n")
        f.write("AGRICOM preprocessor                            ja\n\n")
        f.write("AGRICOM main                                    nee\n\n")
        f.write("Format log                                      xml\n\n")
        f.write("Format kosten                                   f12.3\n")
        f.write("Format schade                                   f12.3\n")
        f.write("Format opbrengst                                f12.3\n")
        f.write("Format waarde                                   f12.3\n\n")
        f.write(
            "Zonal                                           {}\n\n\n".format(
                prmdict["NamZnl"]
            )
        )
        f.write("* Input settings\n")
        f.write("*------------------------------------------------\n")
        f.write("Format SIMGRO                                   idf\n")
        f.write("Format TRANSOL                                  idf\n\n")
        f.write(
            "DirLayersSIMGRO                                 {}\n\n".format(
                prmdict["DirInp"]
            )
        )
        f.write(
            "key                                             {}\n".format(
                join(prmdict["DirInp"], "svat_per")
            )
        )
        f.write(
            "area_svat                                       {}\n".format(
                join(prmdict["DirInp"], "area_svat")
            )
        )
        f.write(
            "scap_svat                                       {}\n".format(
                join(prmdict["DirInp"], "scap_svat")
            )
        )
        if prmdict["idf_svat"]:
            f.write(
                "idf_svat                                        {}\n\n".format(
                    join(prmdict["DirInp"], "idf_svat")
                )
            )
        else:
            f.write(
                "mod2svat                                        {}\n\n".format(
                    join(prmdict["DirInp"], "mod2svat")
                )
            )

        f.write("* Settings NHI2ACM\n")
        f.write("*------------------------------------------------\n")
        f.write(
            "Format output                                   {}\n\n".format(
                prmdict["Format"]
            )
        )
        f.write("droogteschade                                   ja\n")
        f.write("inundatieschade                                 nee\n")
        f.write("verdrassingsschade                              nee\n")
        f.write(
            "zoutschade                                      {}\n\n".format(
                prmdict["zoutschade"]
            )
        )
        f.write(
            "beregeningskosten                               {}\n\n".format(
                prmdict["beregening"]
            )
        )
        f.write("* Parametrisatie NHI2ACM\n")
        f.write("*------------------------------------------------\n")
        f.write(
            "AGRICOM vertaling landgebruik                   {}\n".format(
                join(prmdict["AgrInp"], "landgebruik")
            )
        )
        f.write(
            "gewasparameters                                 {}\n".format(
                join(prmdict["AgrInp"], "gewasparameters_BP18_ref")
            )
        )
        f.write(
            "resterend gewas                                 {}\n".format(
                join(prmdict["AgrInp"], "resterendgewas_BP18_ref")
            )
        )
        f.write(
            "AGRICOM gewasdata opbrengst                     {}\n".format(
                join(prmdict["AgrInp"], "gewasopbrengst")
            )
        )
        f.write(
            "AGRICOM gewasdata waarde                        {}\n\n".format(
                join(prmdict["AgrInp"], "gewaswaarde")
            )
        )
        f.write("* Results NHI2ACM\n")
        f.write("*------------------------------------------------\n")
        f.write(
            "uitv. areaal                                    {}\n".format(
                join(prmdict["DirOutYear"], "areaal")
            )
        )
        f.write(
            "uitv. landgebruik                               {}\n".format(
                join(prmdict["DirOutYear"], "landgebruik")
            )
        )
        f.write(
            "uitv. bodemtype                                 {}\n".format(
                join(prmdict["DirOutYear"], "bodemtype")
            )
        )
        f.write(
            "uitv. inundatieperiode                          {}\n".format(
                join(prmdict["DirOut"], "inundatieperiode_<Date>")
            )
        )
        f.write(
            "uitv. pot. verdamping                           {}\n".format(
                join(prmdict["DirOut"], "Tpot_<Date>")
            )
        )
        f.write(
            "uitv. dervingsfractie droogte                   {}\n".format(
                join(prmdict["DirOut"], "droogteschade_<Date>")
            )
        )
        f.write(
            "uitv. dervingsfractie inundatie                 {}\n".format(
                join(prmdict["DirOut"], "inundatieschade_<Date>")
            )
        )
        f.write(
            "uitv. dervingsfractie verdrassing               {}\n".format(
                join(prmdict["DirOut"], "verdrassingsschade_<Date>")
            )
        )
        f.write(
            "uitv. dervingsfractie zout                      {}\n".format(
                join(prmdict["DirOut"], "zoutschade_<Date>")
            )
        )
        f.write(
            "uitv. dervingsfractie totaal                    {}\n".format(
                join(prmdict["DirOut"], "totschadefractie_<Date>")
            )
        )
        f.write(
            "uitv. overlevingsfractie                        {}\n\n".format(
                join(prmdict["DirOut"], "overlevingsfractie_<Date>")
            )
        )
        f.write(
            "uitv. beregeningstype                           {}\n".format(
                join(prmdict["DirOutYear"], "beregeningstype")
            )
        )
        f.write(
            "uitv. oppw.beregening                           {}\n".format(
                join(prmdict["DirOut"], "beregening_sw_<Date>")
            )
        )
        f.write(
            "uitv. gw.beregening                             {}\n\n".format(
                join(prmdict["DirOut"], "beregening_gw_<Date>")
            )
        )
        f.write("* optional output\n")
        f.write(
            "uitv. act. verdamping                           {}\n".format(
                join(prmdict["DirOut"], "Tact_<Date>")
            )
        )


def run_agricom(fn, prmdict):
    """Run Agricom"""
    return run([abspath(prmdict["Exe"]), fn])


def run_agricommp(year, agricom_ini, prmdict):
    """Run for specific year, then clean up"""
    prmdict["TimStart"] = "{}0101".format(year)
    prmdict["TimEnd"] = "{}1231".format(year)
    prmdict["DirOutYear"] = join(prmdict["DirOut"], str(year))
    agricom_ini = agricom_ini[:-4] + "_{}.inp".format(year)
    # Write ini for year
    write_agricom_ini(agricom_ini, prmdict)
    # Run Agricom
    res = run(
        [abspath(prmdict["Exe"]), agricom_ini], timeout=36000
    )  # run_agricom(agricom_ini,prmdict)
    if res.returncode == 0:
        # clean up...
        for ext in ["inp", "err", "xml"]:
            try:
                remove(agricom_ini[:-3] + ext)
            except:
                pass
    return res.returncode
