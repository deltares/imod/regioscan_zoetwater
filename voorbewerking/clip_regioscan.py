import geopandas as gpd
import imod
import pandas as pd
import numpy as np
from pathlib import Path
from shutil import copyfile
from jinja2 import Environment, FileSystemLoader

##### AREA SHAPE DICT
areas = {f"UH{i}": f"UH{i}.shp" for i in range(1, 7)}
areas["UHtot"] = "Regioscan_UH.shp"

regioscandir = Path("p:/archivedprojects/11202240-kpp-dp-zoetwater/Regioscan")
calcdir = regioscandir / "calc_NL"
hydrodir = regioscandir / "HydroInput_NL"
outputdir = Path(".")
scenarios = ["huidig", "stoom2050"]  #
bpscenarios = {"huidig": "REF2017BP18", "stoom2050": "S2050BP18"}

do_feasibility = True
do_hydroinput = True
do_calcresults = True

startyear = 1974
endyear = 2003
port = 5001

# load template environment
try:
    env = Environment(
        loader=FileSystemLoader(
            (Path(__file__).parent.parent / "regioscan_zoetwater/templates").as_posix()
        )
    )
except NameError:
    env = Environment(
        loader=FileSystemLoader(
            (Path.cwd() / "regioscan_zoetwater/templates").as_posix()
        )
    )


def get_companynumbers(companies):
    selection = np.unique(companies.values)
    selection = selection[np.isfinite(selection)]
    return selection


def select_area(df, companies):
    selection = df.loc[df.CompanyNumber.isin(companies)]
    return selection


def clip_slices(da, like):
    da = da.where(like.notnull())
    x = da.dropna(dim="x", how="all")["x"]
    y = da.dropna(dim="y", how="all")["y"]
    return {"x": slice(x.min(), x.max()), "y": slice(y.max(), y.min())}


def clip_area(da, slices, where):
    return da.sel(**slices).where(where)


def dissolve_and_id(gdf, name):
    gdf["name"] = name
    gdf = gdf.dissolve(by="name")["geometry"].to_frame()
    gdf["ID"] = 1
    return gdf


def scenario_on_pdrive(scenario, hydro_or_calc="hydro"):
    if hydro_or_calc == "hydro":
        return f"{bpscenarios[scenario]}_30jr"
    else:
        return bpscenarios[scenario]


# for every area in areas dict:
for area, areafn in areas.items():

    # read shapefile
    areashp = gpd.read_file(areafn)
    areashp = dissolve_and_id(areashp, area)
    deelgebieden = imod.idf.open(hydrodir / f"deelgebieden.idf")
    is_area = imod.prepare.rasterize(areashp, deelgebieden) == 1

    calc_out = outputdir / f"calc_{area}"
    hydro_out = outputdir / f"HydroInput_{area}"
    calc_out.mkdir(exist_ok=True, parents=True)
    hydro_out.mkdir(exist_ok=True, parents=True)
    # clip deelgebieden as a like
    sel_slices = clip_slices(deelgebieden, is_area.where(is_area))
    imod.idf.write(
        hydro_out / "deelgebieden.idf", clip_area(deelgebieden, sel_slices, is_area)
    )

    # Kansrijkheid
    if do_feasibility:
        (hydro_out / "Kansrijkheidsgrids").mkdir(exist_ok=True, parents=True)
        # loop trough all files and clip
        for fn in (hydrodir / "Kansrijkheidsgrids").rglob("**/*"):
            print(fn)
            if fn.is_dir():
                (hydro_out / fn.relative_to(hydrodir)).mkdir(exist_ok=True)
            elif fn.suffix.lower() == ".idf":
                da = imod.idf.open(fn, pattern="{name}")
                imod.idf.write(
                    hydro_out / fn.relative_to(hydrodir),
                    clip_area(da, sel_slices, is_area),
                )
            else:
                # just copy over
                copyfile(fn, hydro_out / fn.relative_to(hydrodir))

    for scenario in scenarios:
        # break
        if do_hydroinput:
            hydroscendir_out = hydro_out / scenario
            hydroscendir_out.mkdir(exist_ok=True, parents=True)

            # loop trough all files and clip
            hydroscendir = hydrodir / scenario_on_pdrive(scenario, "hydro")
            for fn in hydroscendir.rglob("**/*"):
                print(fn)
                if fn.is_dir():
                    (hydroscendir_out / fn.relative_to(hydroscendir)).mkdir(
                        exist_ok=True
                    )
                elif fn.suffix.lower() == ".idf":
                    da = imod.idf.open(fn, pattern="{name}")
                    imod.idf.write(
                        hydroscendir_out / fn.relative_to(hydroscendir),
                        clip_area(da, sel_slices, is_area),
                    )
                else:
                    # just copy over
                    copyfile(fn, hydroscendir_out / fn.relative_to(hydroscendir))

        if do_calcresults:
            calcscendir_out = calc_out / scenario
            calcscendir_out.mkdir(exist_ok=True, parents=True)
            companies = imod.idf.open(hydro_out / f"{scenario}/modelbedrijven.idf")
            compnumbers = get_companynumbers(companies)

            # loop trough all files and clip
            calcscendir = calcdir / scenario_on_pdrive(scenario, "calc")
            for fn in calcscendir.rglob("**/*"):
                print(fn)
                if fn.is_dir():
                    (calcscendir_out / fn.relative_to(calcscendir)).mkdir(exist_ok=True)
                elif fn.suffix.lower() == ".idf":
                    da = imod.idf.open(fn, pattern="{name}")
                    imod.idf.write(
                        calcscendir_out / fn.relative_to(calcscendir),
                        clip_area(da, sel_slices, is_area),
                    )
                elif fn.suffix.lower() == ".csv":
                    df = pd.read_csv(fn, sep=";", index_col=False)
                    try:
                        df = select_area(df, compnumbers)
                    except:
                        pass
                    df.to_csv(
                        calcscendir_out / fn.relative_to(calcscendir),
                        sep=";",
                        index=False,
                    )
                elif fn.suffix.lower() in [".inp", ".xml", ".pickle"]:
                    pass
                else:
                    # just copy over
                    copyfile(fn, calcscendir_out / fn.relative_to(calcscendir))

        # create ini's
        for template_fn, out_fn in zip(
            ["regioscan.ini", "regioscan_gui.ini"],
            ["ini/regioscan_{case}_{scenario}.ini", "ini/gui_{case}_{scenario}.ini"],
        ):
            template = env.get_template(template_fn)
            out_fn = outputdir / out_fn.format(case=area, scenario=scenario)
            with open(out_fn, "w") as f:
                f.write(
                    template.render(
                        scenario=scenario,
                        startyear=startyear,
                        endyear=endyear,
                        case=area,
                        hydropath=hydro_out.as_posix(),
                        calcpath=calc_out.as_posix(),
                    )
                )

        # create batchfile
        port += 1
        with open(outputdir / f"run_regioscan_{area}_{scenario}.bat", "w") as f:
            f.write("call miniconda3/condabin/activate regioscan\n")
            f.write(f"call regioscan_gui.bat ini/gui_{area}_{scenario}.ini {port}\n")
            f.write("pause\n")
