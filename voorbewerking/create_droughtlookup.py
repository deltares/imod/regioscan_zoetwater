# -*- coding: utf-8 -*-
"""
Created on Fri May 19 09:58:11 2017

@author: delsman

Script to reformat Ruud's result tables into database format

DBASE:
ID               autonumber
Measure          measure id (conv drainage bij geen drainage: 10, regelb drainage bij geen drainage: 11, regelb drainage bij conv. drainage: 12, regelb drainage + sub bij geen drainage: 13, regelb drainage + sub bij conv. drainage: 14)
SoilType         soil id
GroundwaterType  gw id
CropType         crop id
Drainage         1 yes or 0 no
EffectValueType  effecttype id => 1
EffectValue      percentage for drain, or mm Tred for ditch

RUUD:
Voor de naamgeving van de bestanden geldt:
soilID   Description
1          soil physical unit 1 The Netherlands
2          soil physical unit 2 The Netherlands, etc

En voor de gewassen:
Crop1: gras
Crop2: mais
Crop3: aardappel
Crop4: suikerbiet
Crop5: zomergerst

Voor de kolommen in de bestanden geldt:
GLGtrad: m-mv
Treddry_nodrainnosub (ref): Transpiratiereductie [cm/jaar] als gevolg van droogtestress in de situatie met conventionele drainage - Transpiratiereductie als gevolg van droogtestress in de situatie zonder drainage
Drainnosub: Transpiratiereductie [cm/jaar] als gevolg van droogtestress in de situatie met peilgestuurde drainage zonder subirrigatie - Transpiratiereductie als gevolg van droogtestress in de situatie zonder drainage
Drainsub: Transpiratiereductie [cm/jaar] als gevolg van droogtestress in de situatie met peilgestuurde drainage met subirrigatie - Transpiratiereductie als gevolg van droogtestress in de situatie zonder drainage
"GLGtrad","Treddry_nodrainnosub (ref)","Treddry_draintrad - ref","Treddry_drainnosub - ref","Treddry_drainsub - ref (90th perc)","Treddry_drainsub - ref (10th perc)","Tredwet_nodrainnosub (ref)","Tredwet_draintrad - ref","Tredwet_drainnosub - ref","Tredwet_drainsub - ref (90th perc)","Tredwet_drainsub - ref (10th perc)"

22/8: nieuw: bij kleine hoeveelheid Tred niet meer op nul zetten, maar voorgaande waarde gebruiken, en waarde flexibel

10	Conventionele drainage
11	Regelbare drainage
13	Regelbare drainage subinfiltratie
15	Reguliere beregening grondwater
16	Reguliere beregening oppervlaktewater
17	Drains2buffer
19	Spaarwater systeemgerichte drainage
21	Spaarwater subirrigatie
26	Bodemverbeteringsmaatregelen. Verdeel 2 of 5 mm ahv soiltype
27	Perceelstuw. aanname
28	Slootbodemverhoging


"""
from __future__ import division

# import raster_func as rf
import numpy as np
import pandas as pd
import time
from imod.idf import open as idfopen, write as idfwrite
from pathlib import Path

scen = "REF2017BP18"
basedir = Path(
    r"c:\Users\delsman\OneDrive - Stichting Deltares\Documents\Regioscan Zoetwatermaatregelen\Landelijke maatregeldatabase"
)
mapdir = basedir / "Basiskaarten"
fn_pawn250 = mapdir / "pawn250.idf"
fn_glg = mapdir / f"{scen}/glg_1974-2003.idf"
fn_gvgscen = mapdir / f"{scen}/gvg_1974-2003.idf"
fn_gvgref = mapdir / "REF2017BP18/gvg_1974-2003.idf"
fn_gvginc_ps = mapdir / "FIG6B_GVG_VERHOGING_STUWEN.IDF"
fn_gvginc_sb = mapdir / "FIG6A_GVG_VERHOGING_SLOOTBODEM.IDF"
fn_crop = mapdir / f"{scen}/landgebruik.idf"
fn_draincond = mapdir / "COND_B_250.IDF"
resultdir = basedir / f"Resultaatkaarten/{scen}"
resultdir.mkdir(exist_ok=True)

drmeasures = [
    10,
    11,
    13,
    17,
    19,
    21,
]  # skipped all the 'existing' variants [10,11,12,13,14,17,19,20,21,22]
dimeasures = [27, 28]
soils = range(1, 22)
crops = range(1, 6)
drains = [150.0]
glg = np.arange(-3.0, -0.9, 0.1)
gvg = np.arange(-3.5, 0.6, 0.1)
cutoff = 0.05  # cm/yr

############## RECLASS MAPS ###################
# for soil buffer
soilbuff_translate = {}
for soil in soils:
    if soil in [1, 2, 3, 4, 6, 13, 15, 16, 17, 18, 19, 20, 21]:
        soilbuff_translate[soil] = 2.0
    else:
        soilbuff_translate[soil] = 5.0

crop_translate = {c: c for c in list(range(1, 11)) + [21]}
crop_translate[6] = 1  # overige landbouw als gras
crop_translate[7] = 4  # boomteelt als bieten
crop_translate[8] = np.nan
crop_translate[9] = 4  # fruit als bieten
crop_translate[21] = 4  # fruit als bieten
crop_translate[10] = 1  # bollen als gras
# => vnl. gebaseerd op Figuur D.1 in Agricom 2.01 manual


def reclass(arr, recldict):
    retarr = arr.copy(deep=True).load()
    recldict[np.nan] = np.nan
    retarr.values = np.vectorize(recldict.get)(retarr.values)
    return retarr


def classify(arr, valrange):
    retarr = arr.copy(deep=True).load()
    retarr.values[arr <= valrange[0]] = 0
    i = 0
    for v1, v2 in zip(valrange[:-1], valrange[1:]):
        i += 1
        retarr.values[(v1 < arr) & (arr <= v2)] = i
    retarr.values[arr > valrange[-1]] = i + 1
    return retarr


"""
cropreclass = reclass(idfopen(fn_crop),crop_translate)
idfwrite(join(resultdir,'cropreclass.idf'),cropreclass)
soilarr = idfopen(fn_pawn250)
idfwrite(join(resultdir,'soilreclass.idf'),soilarr)
soilbuffer = reclass(soilarr,soilbuff_translate)
idfwrite(join(resultdir,'bodembuffer.idf'),soilbuffer)
glgreclass = classify(idfopen(fn_glg)*-1.,glg[1:]) + 1
idfwrite(join(resultdir,'glgreclass.idf'),glgreclass)
drainreclass = classify(idfopen(fn_draincond),drains)
drainreclass = drainreclass.fillna(0)
idfwrite(join(resultdir,'buisdrainage.idf'),drainreclass)
# GVG
gvgref = idfopen(fn_gvgref)
gvgscen = idfopen(fn_gvgscen)
idfwrite(join(resultdir,'gvg.idf'),gvgscen) # no reclassing
gvginc_ps = gvgref - idfopen(fn_gvginc_ps) / 100.  # make absolute
gvginc_sb = gvgref - idfopen(fn_gvginc_sb) / 100.  # make absolute
pcstuw = 100. * (gvgscen - gvginc_ps)
pcstuw = pcstuw.where(pcstuw > 0, 0.)
sbverh = 100. * (gvgscen - gvginc_sb)
sbverh = sbverh.where(sbverh > 0, 0.)
idfwrite(join(resultdir,'perceelstuwen.idf'), pcstuw)
idfwrite(join(resultdir,'slootbodemverhoging.idf'), sbverh)
"""
############## CALCULATE EFFECT LOOKUP ###################
measuresdf = {i: [] for i in drmeasures + dimeasures}
drresultdir = basedir / "Database drainagemaatregelen/20181204_regioscan_drainage"
diresultdir = basedir / "Natschade/GVG_RSdry_RSwet"
outputdir = basedir / "toDatabase"
for soil in soils:
    for crop in crops:
        print("Soil: {}, crop: {}".format(soil, crop))

        ## First: drainage measures
        fn = drresultdir / f"dTS.spline.Results_soil{soil}_crop{crop}.csv"
        df = pd.read_csv(fn)
        df["nodrainabs"] = (
            df["Treddry_nodrainnosub (ref)"] + df["Tredwet_nodrainnosub (ref)"]
        )  # .apply(lambda x:max(0,x)) # remove negatives
        df["draintradabs"] = (
            df["nodrainabs"]
            + df["Treddry_draintrad - ref"]
            + df["Tredwet_draintrad - ref"]
        )
        df["drainnosubabs"] = (
            df["nodrainabs"]
            + df["Treddry_drainnosub - ref"]
            + df["Tredwet_drainnosub - ref"]
        )
        # df["drainnosubabs"].ix[df["drainnosubabs"] < 0.] = 0. # include negative effects
        df["drainsubabs"] = df["nodrainabs"] + (
            0.5 * df["Treddry_drainsub - ref (10th perc)"]
            + 0.5 * df["Treddry_drainsub - ref (90th perc)"]
            + 0.5 * df["Tredwet_drainsub - ref (10th perc)"]
            + 0.5 * df["Tredwet_drainsub - ref (90th perc)"]
        )  # changed to average of 90 and 10 pct
        # df["drainsubabs"].ix[df["drainsubabs"] < 0.] = 0. # include negative effects
        df["gwk"] = range(1, len(glg) + 1)
        df["soil"] = soil
        df["crop"] = crop
        df["drain"] = 0
        df["evt"] = 1

        for measure in drmeasures:
            for drain in [0, 1]:
                df["measure"] = measure
                df[
                    "pct_effect"
                ] = 0.0  # pct_effect: positief bij reductie transpiratiereductie
                df["drain"] = drain
                if measure in [10, 17]:  # conv drainage bij geen drainage
                    df.loc[:, "pct_effect"] = (
                        -(df["draintradabs"] - df["nodrainabs"])
                        / df["nodrainabs"]
                        * 100.0
                    )
                    df.loc[df["nodrainabs"] < cutoff, "pct_effect"] = df.loc[
                        df["nodrainabs"] > cutoff, "pct_effect"
                    ].iloc[-1]

                elif measure in [11, 19]:  # regelb drainage bij geen drainage
                    if drain == 0:
                        df.loc[:, "pct_effect"] = (
                            -(df["drainnosubabs"] - df["nodrainabs"])
                            / df["nodrainabs"]
                            * 100.0
                        )
                        df.loc[df["nodrainabs"] < cutoff, "pct_effect"] = df.loc[
                            df["nodrainabs"] > cutoff, "pct_effect"
                        ].iloc[-1]
                    else:
                        df.loc[:, "pct_effect"] = (
                            -(df["drainnosubabs"] - df["draintradabs"])
                            / df["draintradabs"]
                            * 100.0
                        )
                        df.loc[df["draintradabs"] < cutoff, "pct_effect"] = df.loc[
                            df["draintradabs"] > cutoff, "pct_effect"
                        ].iloc[-1]

                elif measure in [
                    12,
                    20,
                ]:  # regelb drainage bij conv. drainage   # FOR LEGACY
                    df.loc[:, "pct_effect"] = (
                        -(df["drainnosubabs"] - df["draintradabs"])
                        / df["draintradabs"]
                        * 100.0
                    )
                    df.loc[df["draintradabs"] < cutoff, "pct_effect"] = df.loc[
                        df["draintradabs"] > cutoff, "pct_effect"
                    ].iloc[-1]

                elif measure in [13, 21]:  # regelb drainage + sub bij geen drainage
                    if drain == 0:
                        df.loc[:, "pct_effect"] = (
                            -(df["drainsubabs"] - df["nodrainabs"])
                            / df["nodrainabs"]
                            * 100.0
                        )
                        df.loc[df["nodrainabs"] < cutoff, "pct_effect"] = df.loc[
                            df["nodrainabs"] > cutoff, "pct_effect"
                        ].iloc[-1]
                    else:
                        df.loc[:, "pct_effect"] = (
                            -(df["drainsubabs"] - df["draintradabs"])
                            / df["draintradabs"]
                            * 100.0
                        )
                        df.loc[df["draintradabs"] < cutoff, "pct_effect"] = df.loc[
                            df["draintradabs"] > cutoff, "pct_effect"
                        ].iloc[-1]

                elif measure in [
                    14,
                    22,
                ]:  # regelb drainage + sub bij conv drainage   # FOR LEGACY
                    df.loc[:, "pct_effect"] = (
                        -(df["drainsubabs"] - df["draintradabs"])
                        / df["draintradabs"]
                        * 100.0
                    )
                    df.loc[df["draintradabs"] < cutoff, "pct_effect"] = df.loc[
                        df["draintradabs"] > cutoff, "pct_effect"
                    ].iloc[-1]

                # ---> maximize effects to -50 - 100%
                df.loc[df["pct_effect"] < -50.0, "pct_effect"] = -50.0  # REZOWA-171
                df.loc[df["pct_effect"] > 100.0, "pct_effect"] = 100.0  # REZOWA-171
                # df.loc[df["pct_effect"] < 0,"pct_effect"] = 0.
                df.loc[:, "pct_effect"] = df["pct_effect"].round(decimals=1)
                measuresdf[measure] += [
                    df[["measure", "soil", "gwk", "crop", "drain", "evt", "pct_effect"]]
                ]

        # Then: ditch measures
        fn = diresultdir / f"20191114_TSGVG.spline.Results_soil{soil}_crop{crop}.csv"
        df = pd.read_csv(fn)
        df["gwk"] = range(len(glg) + 1, len(glg) + len(gvg) + 1)
        df["soil"] = soil
        df["crop"] = crop
        df["evt"] = 7
        df["pct_effect"] = (
            df["TSdry"] + df["TSwet"]
        ) * 10.0  # pct_effect voor ditch measures is direct de transpiratiereductie (van cm/jr naar mm/jr)
        df.loc[
            df["pct_effect"] < -100, "pct_effect"
        ] = -100.0  # negatieve transpiratiereductie: neem mee #REZOWA-171
        df.loc[:, "pct_effect"] = df["pct_effect"].round(decimals=1)
        for measure in dimeasures:
            for drain in [0, 1]:
                df["measure"] = measure
                df["drain"] = drain
                measuresdf[measure] += [
                    df[["measure", "soil", "gwk", "crop", "drain", "evt", "pct_effect"]]
                ]

        # break
# break
start = 1
for measure in drmeasures + dimeasures:
    measuresdf[measure] = pd.concat(measuresdf[measure], ignore_index=True)
    measuresdf[measure] = measuresdf[measure].reset_index(drop=True)
    measuresdf[measure].index = measuresdf[measure].index + start
    start += len(measuresdf[measure])

dftotal = pd.concat([measuresdf[measure] for measure in drmeasures + dimeasures])
dftotal.to_csv(
    outputdir / f"EffectDroughtLookup_{time.strftime('%Y%m%d')}.csv", index_label="id"
)


# GroundwaterTypeTable
dfgwl = pd.DataFrame(
    index=range(len(glg) + len(gvg)), columns=["id", "low", "high", "mid", "desc"]
)
dfgwl.loc[:, "id"] = list(range(1, len(glg) + len(gvg) + 1))
gwl = pd.Series(list(glg) + list(gvg))
dfgwl.loc[:, "mid"] = gwl * -1.0
dfgwl.loc[:, "low"] = ((gwl + gwl.shift(1)) / 2.0).fillna(-9999.0) * -1.0
dfgwl.loc[:, "high"] = ((gwl + gwl.shift(-1)) / 2.0).fillna(9999.0) * -1.0
dfgwl.loc[len(glg) - 1, ["high"]] = -9999
dfgwl.loc[len(glg), ["low"]] = 9999
dfgwl.loc[:, ["low", "high", "mid"]] = dfgwl.loc[:, ["low", "high", "mid"]].round(
    decimals=2
)
dfgwl.loc[:, "desc"] = (
    dfgwl.loc[:, "low"].astype(str) + " - " + dfgwl.loc[:, "high"].astype(str)
)
dfgwl.loc[dfgwl["low"] == 9999.0, "desc"] = (
    "< " + dfgwl.loc[dfgwl["low"] == 9999.0, "high"].astype(str) + " m-mv"
)
dfgwl.loc[dfgwl["high"] == -9999.0, "desc"] = (
    "> " + dfgwl.loc[dfgwl["high"] == -9999.0, "low"].astype(str) + " m-mv"
)
dfgwl.loc[list(range(len(glg))), "desc"] = (
    "GLG " + dfgwl.loc[list(range(len(glg))), "desc"]
)
dfgwl.loc[list(range(len(glg), len(glg) + len(gvg))), "desc"] = (
    "GVG " + dfgwl.loc[list(range(len(glg), len(glg) + len(gvg))), "desc"]
)
dfgwl[["id", "desc", "low", "high", "mid"]].to_csv(
    outputdir / f"GroundwaterType_{time.strftime('%Y%m%d')}.csv", index=False
)
