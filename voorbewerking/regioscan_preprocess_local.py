# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 13:16:29 2018

@author: delsman

Regioscan preprocessor - based on R scripting from Martin Mulder

Calculation steps:
    1) From LHM run, copy necessary files to MetBeregening:  (with sprinkling))
        bdgPm
        bdgPsgw
        bdgPssw
        msw_Eic
        msw_qmr
        msw_Tact
        msw_Tpot
        msw_Trel
        transol_con_rz
        Run AGRICOM to calculate crop damage
    2) Calculate ZonderBeregening
        Adjust Tact by sprinkling amount * efficiency
        Run AGRICOM to calculate crop damage
    3) Calculate Buffer measures
        Calculate 90 percentile Tred
        For both percentile and fixed buffer amounts:
            Reduce Tred by this buffer, by using buffer until empty
            Recalculate Tact and Trel
            Run AGRICOM to calculate crop damage
    3) Calculate Evenredig
        For percentages:
            Reduce Tred by given percentage, recalculate Tact, Trel
            Run AGRICOM to calculate crop damage
    4) Calculate Zout
        For salt concentrations:
            Set transol_con_rz to concentration
            Run AGRICOM to calculate crop damage
    5) Order in database for Regioscan
    
REFACTORING:
- do entire procedure for (4-year) subperiods, and delete temporary files, to deal with harddisk issues
- complication: 90 percentile buffer can only be determined using ALL annual Tred values
- workaround: do all other steps first, and cleanup. Only: store NoSprinkling Tpot and Tact
- then perform ExtBuffer.
TODO: implement automation of two-step process
"""
import os, glob
from zipfile import ZipFile
from time import strptime  # asctime,
from datetime import datetime, timedelta
from os.path import join, exists, basename  # , abspath
from pathlib import Path
from shutil import copy, move, rmtree
from agricom import reclass_lhm_agricom, cropcalendar, write_agricom_ini, run_agricommp
import imod
import numpy as np
import xarray as xr
import pandas as pd

# import raster_func as rf
from multiprocessing import Pool
import warnings

warnings.filterwarnings("ignore", category=RuntimeWarning)


def get_timesteps(
    yearstart,
    yearend=None,
    start_or_end="start",
    reset_idec_year=False,
    dec_or_days="decade",
):
    """Iterator to get decadenr and date as ints for given year(s)"""
    if yearend is None:
        yearend = yearstart
    if dec_or_days == "decade":
        if start_or_end == "end":
            days = [10, 20, 31]
        else:
            days = [1, 11, 21]

        i = -1
        for year in range(yearstart, yearend + 1):
            if reset_idec_year:
                i = -1
            for m in range(1, 13):
                for d in days:
                    if d == 31:
                        # get actual last day
                        m1 = m
                        if m == 12:
                            m1 = 0
                        d = (datetime(year, m1 + 1, 1) - timedelta(days=1)).day
                    i += 1
                    yield i, year * 10000 + m * 100 + d
    else:
        # daily timesteps: use timedelta...
        for year in range(yearstart, yearend + 1):
            d = datetime(year, 1, 1)
            i = 0
            while d.year == year:
                yield i, int(d.strftime("%Y%m%d"))
                d = d + timedelta(days=1)
                i += 1


def doy(dec):
    """Calculate day-of-year and year of given decade"""
    t = strptime(str(dec), "%Y%m%d")
    return t.tm_yday - 1, t.tm_year


def dec_year(dec):
    """get decnr within year"""
    return dec % 36


def run_agricom(agricom_ini, agricomdict):
    # run Agricom for each year in parallel
    write_agricom_ini(agricom_ini, agricomdict)

    p = Pool()
    # res = []
    # use multiprocessing to run Agricom per year
    results = []
    for y in range(period[0], period[1] + 1):
        results += [
            p.apply_async(run_agricommp, [y, agricom_ini, agricomdict])
        ]  # , callback=collect_result)#.get()
    output = [p.get() for p in results]
    p.close()
    p.join()  # 3600)
    p.terminate()
    # strip year from bodem, landuse, etc
    try:
        for fn in ["areaal", "beregeningstype", "bodemtype", "landgebruik"]:
            copy(
                join(
                    agricomdict["DirOut"], "{}".format(period[0]), "{}.idf".format(fn)
                ),
                join(agricomdict["DirOut"], "{}.idf".format(fn)),
            )
        for y in range(period[0], period[1] + 1):
            rmtree(join(agricomdict["DirOut"], "{}".format(y)))
    except FileNotFoundError:
        pass


def idfload(path, **kwargs):
    print("Loading {}".format(path))
    ret = imod.idf.open(path, **kwargs)
    return ret


def idfwrite(path, da, **kwargs):
    try:
        imod.idf.write(path, da.squeeze(drop=True), **kwargs)
    except ValueError:
        print(da.squeeze(drop=True))
        raise
    return None


def idf_buffer(da, buffer=None, like=None):
    """Create empty buffer around da"""
    if like is None and buffer is not None:
        dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(da)
        coords = imod.util._xycoords(
            (xmin - buffer, xmax + buffer, ymin - buffer, ymax + buffer), (dx, dy)
        )
        new = xr.DataArray(
            data=np.ones((len(coords["y"]), len(coords["x"]))) * np.nan,
            coords=coords,
            dims=("y", "x"),
        )
    elif like is not None:
        new = like.copy() * np.nan
    else:
        raise ValueError("Supply either buffer or like")

    new.loc[{"y": da["y"], "x": da["x"]}] = da
    return new


def fix_idf_svat(fn_idf_svat, f_out, da_subset, da_full):
    """Function to create a new idf_svat for use with agricom
    File format: svat_id  rownr_full  colnr_full   x   y
    Format spec is important: 3i10,2f15.3"""
    x = da_subset["x"].values
    y = da_subset["y"].values
    dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(da_full)

    def get_rc(x_, y_):
        c = int((x_ - xmin) // dx)
        r = int((ymax - y_) // -dy)
        return r, c

    with open(fn_idf_svat, "r") as f, open(f_out, "w") as f2:
        for line in f:
            svat_id, row_sub, col_sub = [int(v) for v in line.split()]
            x_, y_ = x[col_sub - 1], y[row_sub - 1]
            r, c = get_rc(x_, y_)
            f2.write(f"{svat_id: 10d}{r: 10d}{c: 10d}{x_:15.3f}{y_:15.3f}\n")


def array_translate(da, translate_dict):
    da = da.load()
    da.values[~da.isnull()] = np.vectorize(translate_dict.get)(da.values[~da.isnull()])
    return da


if __name__ == "__main__":
    bpscen = "Huidig"
    yearstart = 1997
    yearend = 2016
    periods = [[1997, 2016]]
    # periods = [[s,min(s+3,yearend)] for s in range(yearstart,yearend+1,4)]
    do_steps = [
        "Proportional"
    ]  #'WithSprinkling','SoilBuffer','Proportional','RegioscanDB']#,'Cleanup'] #,'Salt' 'NoSprinkling','ExtBuffer',
    # do_steps = ['WithSprinkling','ExtBuffer','RegioscanDB','Cleanup']
    overwrite = False
    store_tmp = False
    do_salt = False
    extbuffer_percentages = [90]
    soilbuffer_mms = [2, 5, 10, 20]
    proportional_pcts = [90, 100]  # 0,10,20,30,40,50,60,70,80,90,100]
    salt_concentrations = [1000, 2000, 3000, 4000, 5000]

    dir_inputdata = Path(r"p:\11202619-regioscan2\Case_Twello\Azure_resultaten")
    dir_out = Path(r"p:\11202619-regioscan2\Case_Twello\Twello_Azure_nobuf")
    dir_output = dir_out / bpscen
    dir_withsprinkling = dir_output / "WithSprinkling"
    dir_nosprinkling = dir_output / "NoSprinkling"
    dir_buffer = dir_output / "Tmp_Buffer"
    dir_proportional = dir_output / "Tmp_Proportional"
    dir_salt = dir_output / "Tmp_Salt"
    dir_regioscandb = dir_output / "RegioscanDB"
    dir_store = dir_output / "Store_ExtBuffer"
    agricom_exe = (
        r"p:\11202240-kpp-dp-zoetwater\Regioscan\Agricom_WWL\Exe\AGRICOM_LHM340.exe"
    )
    svatkey = dir_inputdata / "svat_per.key"  # niet beschikbaar in BP2018 runs
    dec_or_days = "decade"
    nodata_buffer = 3000.0
    spr_eff = 0.4  # new: previously always .25. See Van Bakel, 2019

    filepatterns = {
        "bdgPm": join("bdgPm", "bdgPm_<DATE>_l1.idf"),
        "bdgPsgw": join("bdgPsgw", "bdgPsgw_<DATE>_l1.idf"),
        "bdgPssw": join("bdgPssw", "bdgPssw_<DATE>_l1.idf"),
        "msw_Eic": join("msw_Eic", "msw_Eic_<DATE>_l1.idf"),
        "msw_Tact": join("msw_Tact", "msw_Tact_<DATE>_l1.idf"),
        "msw_Tpot": join("msw_Tpot", "msw_Tpot_<DATE>_l1.idf"),
        "msw_Trel": join("msw_Trel", "msw_Trel_<DATE>_l1.idf"),
        "transol_con_rz": join("transol_con_rz", "transol_con_rz_<DATE>.idf"),
        "Qappl": join("Qappl", "Qappl_<DATE>.idf"),
        "Qfill": join("Qfill", "Qfill_<DATE>.idf"),
        "Tred": join("Tred", "Tred_<DATE>_l1.idf"),
        "Tbuf": join("Tbuf", "Tbuf_<DATE>.idf"),
    }
    prms = [
        "bdgPm",
        "bdgPsgw",
        "bdgPssw",
        "msw_Eic",
        "msw_Tact",
        "msw_Tpot",
        "msw_Trel",
    ]
    if do_salt:
        prms += ["transol_con_rz"]
    simgro_files = [
        "mod2svat.inp",
        "area_svat.inp",
        "scap_svat.inp",
        "svat_per.key",
        "svat_per.tim",
        "idf_svat.inp",
    ]
    prms_notrel = [p for p in prms if p != "msw_Trel"]  # for convenience
    prms_unchanged = [
        p for p in prms if p not in ["msw_Tact", "msw_Trel", "bdgPsgw", "bdgPssw"]
    ]  # for convenience

    agricomdict = {
        "NamZnl": str(dir_out / "namznl.idf"),
        "TimStart": "{}0101".format(periods[0][0]),
        "TimEnd": "{}1231".format(periods[0][1]),
        "Format": "idf",
        "AgrInp": r"p:\11202240-kpp-dp-zoetwater\Regioscan\Agricom_WWL\inputfiles",
        "DirInp": "",
        "DirOut": "",
        "DirOutYear": "",
        "idf_svat": True,
        "zoutschade": "nee",
        "beregening": "ja",
        "Exe": agricom_exe,
    }
    if do_salt:
        agricomdict["zoutschade"] = "ja"
    # if nodata_buffer is not None:
    #    agricomdict['idf_svat'] = True
    if not do_salt and "Salt" in do_steps:
        do_steps.remove("Salt")
    """    if Path("done.txt").exists():
        Path("done.txt").remove()
    else:
        # move files back
        for prm in prms_unchanged:
            print(prm)
            if exists(join(dir_nosprinkling,filepatterns[prm].replace('<DATE>',f"{periods[0][1]}1231"))):
                for idec, dec in get_timesteps(*periods[0],start_or_end='end',dec_or_days=dec_or_days):
                    move(join(dir_nosprinkling,filepatterns[prm].replace('<DATE>',str(dec))),
                            join(dir_withsprinkling,filepatterns[prm].replace('<DATE>',str(dec))))
    """

    for period in periods:
        agricomdict["TimStart"] = "{}0101".format(period[0])
        agricomdict["TimEnd"] = "{}1231".format(period[1])

        #### STEP 1: copy necessary files to MetBeregening
        if "WithSprinkling" in do_steps:
            print("EXECUTING With Sprinkling...")
            dir_to = dir_withsprinkling

            dir_to.mkdir(exist_ok=True, parents=True)
            for prm in prms:  # loop through parameters
                (dir_to / prm).mkdir(exist_ok=True, parents=True)
            dir_store.mkdir(exist_ok=True)
            (dir_store / "msw_Tpot").mkdir(exist_ok=True, parents=True)

            # SUMMARIZE METASWAP FILES
            # for y in range(period[0],period[1]+1):
            times = [datetime(period[0], 1, 1)]
            for idec, dec in get_timesteps(
                *period, start_or_end="end", dec_or_days="decade"
            ):
                times += [datetime.strptime(str(dec), "%Y%m%d")]

            for prm in prms_notrel:  # loop through parameters, handle trel separately
                if overwrite or not exists(
                    join(
                        dir_to, filepatterns[prm].replace("<DATE>", f"{period[0]}1231")
                    )
                ):
                    print(prm)
                    fnto = dir_to / prm / prm
                    fnfrom = dir_inputdata / filepatterns[prm].replace(
                        "_<DATE>_l1.idf", "*"
                    )

                    # sum values per decade
                    da = imod.idf.open(fnfrom)
                    dars = da.groupby_bins("time", bins=times, labels=times[1:]).sum(
                        dim="time"
                    )
                    dars = dars.rename({"time_bins": "time"})
                    imod.idf.save(
                        str(fnto),
                        dars,
                        nodata=-9999.0,
                        pattern="{name}_{time:%Y%m%d}_l{layer}{extension}",
                    )

                    if prm == "msw_Tpot":
                        imod.idf.save(
                            str(dir_store / prm / prm),
                            dars,
                            nodata=-9999.0,
                            pattern="{name}_{time:%Y%m%d}_l{layer}{extension}",
                        )

                    """  NOT NECESSARY ANYMORE: AGRICOM WORKS FOR SMALLER-SIZE GRID
                    if nodata_buffer is not None:
                        for idec, dec in get_timesteps(*period,start_or_end='end',dec_or_days='decade'):  # loop through all dates within period
                            fnfrom = dir_to / filepatterns[prm].replace('<DATE>',str(dec))
                            fnto = dir_to / filepatterns[prm].replace('<DATE>',str(dec))
                            
                            # create buffer
                            tmp = imod.idf.open(fnfrom,pattern='{name}')
                            tmp2 = idf_buffer(tmp,buffer=nodata_buffer)
                            idfwrite(fnto,tmp2,nodata=-9999.)
                            
                            if prm == 'msw_Tpot':
                                copy(fnto,dir_store / filepatterns[prm].replace('<DATE>',str(dec)))
                    """

            if overwrite or not exists(
                join(
                    dir_to,
                    filepatterns["msw_Trel"].replace("<DATE>", f"{period[0]}1231"),
                )
            ):
                # recalculate Trel
                tpot = imod.idf.open(
                    dir_to / filepatterns["msw_Tpot"].replace("_<DATE>_l1.idf", "*")
                )
                tact = imod.idf.open(
                    dir_to / filepatterns["msw_Tact"].replace("_<DATE>_l1.idf", "*")
                )
                trel = tact / tpot
                imod.idf.save(
                    str(dir_to / "msw_Trel" / "msw_Trel"),
                    trel,
                    nodata=-9999.0,
                    pattern="{name}_{time:%Y%m%d}_l{layer}{extension}",
                )

            # create namznl.idf
            namznl = imod.idf.open(
                dir_to / filepatterns["msw_Tpot"].replace("<DATE>", f"{period[0]}1231"),
                pattern="{name}",
            ).load()
            namznl.values[~namznl.isnull()] = 1.0
            idfwrite("namznl.idf", namznl)

            # overall simgro files: mod2svat, area_svat, scap_svat, svat_per.key remain the same, only svat_per.tim needs updating
            for fn in simgro_files:
                fnfrom = dir_inputdata / fn
                fnto = dir_to / fn
                if overwrite or not fnto.exists():
                    print(fnfrom)
                    copy(fnfrom, fnto)

            # include coords in idf_svat
            tmp_buffer = idf_buffer(namznl, buffer=nodata_buffer)
            fix_idf_svat(
                dir_inputdata / "idf_svat.inp",
                dir_to / "idf_svat.inp",
                namznl,
                tmp_buffer,
            )

            # create new svat_per.tim for entire period
            with open(join(dir_to, "svat_per.tim"), "w") as f:
                for idec, dec in get_timesteps(
                    *period, start_or_end="start", dec_or_days=dec_or_days
                ):
                    f.write("{:5d}.000000  {}\n".format(*doy(dec)))
                f.write("{:5d}.000000  {}\n".format(0, period[1] + 1))

            if "ExtBuffer" not in do_steps:
                if overwrite or not exists(
                    join(dir_to, "grids", "droogteschade_{}1231.idf".format(period[1]))
                ):
                    # RUN AGRICOM
                    agricomdict["DirInp"] = dir_to
                    agricomdict["DirOut"] = join(dir_to, "grids")
                    agricomdict["beregening"] = "ja"
                    agricom_ini = join(bpscen, "agricom_withsprinkling.inp")
                    run_agricom(agricom_ini, agricomdict)

                # CREATE AND STORE MAPS ABOUT AGRICOM LANDUSE FOR FUTURE USE
                # reclass landuse to agricom landuse
                luse = imod.idf.open(join(dir_to, "grids", "landgebruik.idf"))
                luse = array_translate(luse, reclass_lhm_agricom)
                idfwrite(join(dir_to, "grids", "agricom_luse.idf"), luse)

                # get start and end decade of crops
                start_dec_dict = {k: v[0] for k, v in cropcalendar.items()}
                end_dec_dict = {k: v[1] for k, v in cropcalendar.items()}
                start_dec = luse.copy(deep=True)
                start_dec = array_translate(start_dec, start_dec_dict)
                idfwrite(join(dir_to, "grids", "agricom_startdec.idf"), start_dec)

                end_dec = luse.copy(deep=True)
                end_dec = array_translate(end_dec, end_dec_dict)
                idfwrite(join(dir_to, "grids", "agricom_enddec.idf"), end_dec)
                # copy to store
                for fn in ["agricom_startdec.idf", "agricom_enddec.idf"]:
                    copy(join(dir_to, "grids", fn), dir_store)

        #### STEP 2: Recalculate Tact for water applied during sprinkling
        if "NoSprinkling" in do_steps:
            print("EXECUTING No Sprinkling...")
            dir_from = dir_withsprinkling
            dir_to = dir_nosprinkling

            if not exists(dir_to):
                os.mkdir(dir_to)
            for prm in prms + ["Tred"]:  # loop through parameters
                if not exists(join(dir_to, prm)):
                    os.mkdir(join(dir_to, prm))
            if not exists(dir_store):
                os.mkdir(dir_store)
            if not exists(join(dir_store, "msw_Tact")):
                os.mkdir(join(dir_store, "msw_Tact"))

            iPstot_tmin1 = None
            if overwrite or not exists(
                join(
                    dir_to,
                    filepatterns["msw_Trel"].replace("<DATE>", str(period[1]) + "1231"),
                )
            ):
                Tpot = idfload(join(dir_from, "msw_Tpot", "msw_Tpot*.idf")) * -1000.0
                Tact = idfload(join(dir_from, "msw_Tact", "msw_Tact*.idf")) * -1000.0
                Tred = Tpot - Tact
                Psgw = idfload(join(dir_from, "bdgPsgw", "bdgPsgw*.idf")) * 1000.0
                Pssw = idfload(join(dir_from, "bdgPssw", "bdgPssw*.idf")) * 1000.0
                Pstot = Psgw + Pssw
                start_dec = idfload(
                    join(dir_withsprinkling, "grids", "agricom_startdec.idf")
                )
                end_dec = idfload(
                    join(dir_withsprinkling, "grids", "agricom_enddec.idf")
                )

                for idec, dec in get_timesteps(
                    *period, start_or_end="end", dec_or_days=dec_or_days
                ):
                    print(idec, dec)
                    iPstot = Pstot.isel(time=idec, layer=0)
                    iTred = Tred.isel(time=idec, layer=0)
                    if dec_year(idec) == 0:
                        iPstot_tmin1 = iPstot.copy(deep=True) * 0.0  # Reset every year
                    if iPstot.sum().values > 0:
                        # correct Tact for sprinkling
                        iTpot = Tpot.isel(time=idec, layer=0)
                        iTact = Tact.isel(time=idec, layer=0)
                        iPstot += iPstot_tmin1  # add previous timestep leftover
                        iPstot = iPstot.where(
                            dec_year(idec) + 1 >= start_dec, 0.0
                        )  # set to zero outside growing season
                        iPstot = iPstot.where(dec_year(idec) + 1 <= end_dec, 0.0)

                        iPsapp = iPstot.copy() * spr_eff
                        iPsapp = iPsapp.where(iPsapp < iTact, iTact.values)
                        iPstot_tmin1 = (
                            iPstot - iPsapp / spr_eff
                        )  # keep sprinkling amount for next timestep  => dit deed Martin niet

                        # idfwrite(join(dir_nosprinkling,'iPstot_tmin1{:d}_l1.idf'.format(dec)), iPstot_tmin1)
                        print(
                            "leftover sprinkling: %.0f of %.0f total mm"
                            % (iPstot_tmin1.sum().values, iPstot.sum().values)
                        )
                        iTact -= iPsapp
                        iPstot *= 0.0
                        iTrel = iTact / iTpot
                        # break

                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["msw_Tact"].replace("<DATE>", str(dec)),
                            ),
                            iTact / -1000.0,
                        )
                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["msw_Trel"].replace("<DATE>", str(dec)),
                            ),
                            iTrel,
                        )
                        idfwrite(
                            join(
                                dir_to, filepatterns["Tred"].replace("<DATE>", str(dec))
                            ),
                            iTred,
                        )
                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["bdgPsgw"].replace("<DATE>", str(dec)),
                            ),
                            iPstot,
                        )
                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["bdgPssw"].replace("<DATE>", str(dec)),
                            ),
                            iPstot,
                        )
                    else:
                        # copy files
                        for prm in ["msw_Tact", "msw_Trel", "bdgPsgw", "bdgPssw"]:
                            copy(
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )
                        idfwrite(
                            join(
                                dir_to, filepatterns["Tred"].replace("<DATE>", str(dec))
                            ),
                            iTred,
                        )
                    # copy to store
                    copy(
                        join(
                            dir_to, filepatterns["msw_Tact"].replace("<DATE>", str(dec))
                        ),
                        join(
                            dir_store,
                            filepatterns["msw_Tact"].replace("<DATE>", str(dec)),
                        ),
                    )

            # copy/move unchanged files
            for prm in prms_unchanged:
                print(prm)
                for idec, dec in get_timesteps(
                    *period, start_or_end="end", dec_or_days=dec_or_days
                ):
                    if overwrite or not exists(
                        join(dir_to, filepatterns[prm].replace("<DATE>", str(dec)))
                    ):
                        move(
                            join(
                                dir_from, filepatterns[prm].replace("<DATE>", str(dec))
                            ),
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec))),
                        )

            for fn in simgro_files:
                fnfrom = join(dir_from, fn)
                fnto = join(dir_to, fn)
                if overwrite or not exists(fnto):
                    print(fnfrom)
                    copy(fnfrom, fnto)

            # Done, run Agricom
            if overwrite or not exists(
                join(
                    dir_nosprinkling,
                    "grids",
                    "droogteschade_{}1231.idf".format(period[1]),
                )
            ):
                # RUN AGRICOM
                agricomdict["DirInp"] = dir_to
                agricomdict["DirOut"] = join(dir_to, "grids")
                agricomdict["beregening"] = "ja"
                agricom_ini = join(bpscen, "agricom_nosprinkling.inp")
                # write_agricom_ini(agricom_ini, agricomdict)
                run_agricom(agricom_ini, agricomdict)

            # move other files back
            for prm in prms_unchanged:
                print(prm)
                for idec, dec in get_timesteps(
                    *period, start_or_end="end", dec_or_days=dec_or_days
                ):
                    move(
                        join(dir_to, filepatterns[prm].replace("<DATE>", str(dec))),
                        join(dir_from, filepatterns[prm].replace("<DATE>", str(dec))),
                    )

        ##############################################################################################
        #### STEP 3: Buffer, recalculate Tact for water applied during sprinkling from Buffer
        #### STEPS 1 AND 2 MUST HAVE BEEN EXECUTED FOR ENTIRE PERIOD BEFORE RUNNING THIS STEP
        ##############################################################################################
        if "ExtBuffer" in do_steps:
            print("EXECUTING ExtBuffer...")
            dir_from = dir_withsprinkling
            if "NoSprinkling" in do_steps:
                dir_from = dir_nosprinkling

            start_dec = idfload(join(dir_store, "agricom_startdec.idf"))
            end_dec = idfload(join(dir_store, "agricom_enddec.idf"))

            if not exists(join(dir_store, "Tred")):
                os.mkdir(join(dir_store, "Tred"))

            for bufpct in extbuffer_percentages:
                print("ExtBuffer of size percentile {}".format(bufpct))

                dir_to = f"{dir_buffer}_{bufpct:03d}"
                if not exists(dir_to):
                    os.mkdir(dir_to)
                for prm in prms + ["Qappl", "Tred", "Tbuf"]:  # loop through parameters
                    if not exists(join(dir_to, prm)):
                        os.mkdir(join(dir_to, prm))

                # loop through years and use buffer for reducing Tred
                if overwrite or not exists(
                    join(
                        dir_to,
                        filepatterns["msw_Trel"].replace(
                            "<DATE>", str(period[1]) + "1231"
                        ),
                    )
                ):

                    Tpot = (
                        idfload(join(dir_store, "msw_Tpot", "msw_Tpot*.idf")) * -1000.0
                    )
                    Tact = (
                        idfload(join(dir_store, "msw_Tact", "msw_Tact*.idf")) * -1000.0
                    )  # tact is coming from store

                    # set Tred to zero outside Agricom growing season
                    if not exists(
                        join(
                            dir_store,
                            filepatterns["Tred"].replace(
                                "<DATE>", str(yearend) + "1220"
                            ),
                        )
                    ):  # somehow last decade doesn't work...
                        print("Correct Tred")
                        Tred = Tpot - Tact
                        for idec, dec in get_timesteps(
                            yearstart,
                            yearend,
                            start_or_end="end",
                            dec_or_days=dec_or_days,
                        ):
                            inseason = xr.zeros_like(start_dec).where(
                                dec_year(idec) + 1 < start_dec, 1.0
                            )
                            inseason = inseason.where(
                                dec_year(idec) + 1 <= end_dec, 0.0
                            )
                            iTred = Tred.isel(time=idec, layer=0).load()
                            iTred = iTred.where(inseason == 1.0, 0.0)
                            """
                            inseason = (start_dec.values > dec_year(idec)+1)&(end_dec.values <= dec_year(idec)+1)
                            iTred = Tred[idec,0].load()
                            iTred.values *= inseason.astype(int)"""
                            idfwrite(
                                join(
                                    dir_store,
                                    filepatterns["Tred"].replace("<DATE>", str(dec)),
                                ),
                                iTred,
                            )
                    Tred = idfload(join(dir_store, "Tred", "Tred*.idf"))

                    # calculate necessary percentile
                    if overwrite or not exists(
                        join(dir_to, "Tred_{:03d}.idf".format(bufpct))
                    ):
                        print("Annual sum Tred")
                        Tred_year = Tred.groupby("time.year").sum(dim="time")

                        print("Calculate percentile")
                        # Tbuffer = Tred_year.quantile(bufpct/100.,dim='year')[0]  SLOW...
                        Tbuffer = xr.ones_like(Tpot.isel(time=0, layer=0))
                        Tbuffer.values = np.percentile(
                            Tred_year.values, q=bufpct, axis=0
                        )
                        idfwrite(
                            join(dir_to, "Tred_{:03d}.idf".format(bufpct)), Tbuffer
                        )
                    else:
                        Tbuffer = idfload(
                            join(dir_to, "Tred_{:03d}.idf".format(bufpct))
                        )
                    buftot = Tbuffer.sum().values

                    # calculate new Tred using the calculated buffer
                    # for maps in store, idec is not the right index, correct for year in period vs year in total period
                    ideccorr = (period[0] - yearstart) * 36
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        print(idec, dec)

                        # new year: reset buffer:
                        if dec_year(idec) == 0:
                            Tbufyear = Tbuffer.copy(deep=True)

                        iTred = Tred.isel(time=idec + ideccorr, layer=0)
                        iTpot = Tpot.isel(time=idec + ideccorr, layer=0)
                        iTact = Tact.isel(time=idec + ideccorr, layer=0)

                        # correct Tact for water applied from buffer
                        # applied water = min(Tred, Tbuffer)
                        # appl = xr.zeros_like(Tbuffer).where(iTred<=Tbuffer,iTred,Tbuffer)
                        appl = iTred.where(iTred <= Tbufyear, Tbufyear)
                        appl = appl.where(
                            dec_year(idec) + 1 >= start_dec, 0.0
                        )  # limit to growing season
                        appl = appl.where(dec_year(idec) + 1 <= end_dec, 0.0)

                        iTred = iTred - appl
                        iTact = iTact + appl
                        Tbufyear = Tbufyear - appl

                        print(
                            "Applied %.0f m. Leftover buffer: %.0f of %.0f total m"
                            % (
                                appl.sum().values / 1000.0,
                                Tbufyear.sum().values / 1000.0,
                                buftot / 1000.0,
                            )
                        )

                        iTrel = iTact / iTpot

                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["msw_Tact"].replace("<DATE>", str(dec)),
                            ),
                            iTact / -1000.0,
                        )
                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["msw_Trel"].replace("<DATE>", str(dec)),
                            ),
                            iTrel,
                        )
                        if store_tmp:
                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["Qappl"].replace("<DATE>", str(dec)),
                                ),
                                appl,
                            )
                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["Tred"].replace("<DATE>", str(dec)),
                                ),
                                iTred,
                            )
                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["Tbuf"].replace("<DATE>", str(dec)),
                                ),
                                Tbufyear,
                            )

                # move other files
                for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                    print(prm)
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        if overwrite or not exists(
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec)))
                        ):
                            move(
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )

                for fn in simgro_files:
                    fnfrom = join(dir_from, fn)
                    fnto = join(dir_to, fn)
                    if overwrite or not exists(fnto):
                        print(fnfrom)
                        copy(fnfrom, fnto)

                # Done, run Agricom
                if overwrite or not exists(
                    join(dir_to, "grids", "droogteschade_{}1231.idf".format(period[1]))
                ):
                    # RUN AGRICOM
                    agricomdict["DirInp"] = dir_to
                    agricomdict["DirOut"] = join(dir_to, "grids")
                    agricomdict["beregening"] = "nee"
                    agricom_ini = join(bpscen, "agricom_buffer_{}.inp".format(bufpct))
                    # write_agricom_ini(agricom_ini, agricomdict)
                    run_agricom(agricom_ini, agricomdict)

                # move other files back
                for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                    print(prm)
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        move(
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec))),
                            join(
                                dir_from, filepatterns[prm].replace("<DATE>", str(dec))
                            ),
                        )

        ##############################################################################################
        #### STEP 4: Soilbuffer, recalculate Tact for water applied during sprinkling from SoilBuffer.
        ####         Refill soil buffer with excess precip
        ##############################################################################################
        if "SoilBuffer" in do_steps:
            print("EXECUTING SoilBuffer...")
            dir_from = dir_withsprinkling

            start_dec = idfload(
                join(dir_withsprinkling, "grids", "agricom_startdec.idf")
            )
            end_dec = idfload(join(dir_withsprinkling, "grids", "agricom_enddec.idf"))
            Tpot = (
                idfload(join(dir_withsprinkling, "msw_Tpot", "msw_Tpot*.idf")) * -1000.0
            )
            Tact = (
                idfload(join(dir_nosprinkling, "msw_Tact", "msw_Tact*.idf")) * -1000.0
            )
            Pm = idfload(join(dir_withsprinkling, "bdgPm", "bdgPm_*.idf")) * 1000.0
            Tred = Tpot - Tact

            for bufmm in soilbuffer_mms:
                print(f"SoilBuffer of size {bufmm} mm")

                dir_to = f"{dir_buffer}_{bufmm:02d}mm"
                if not exists(dir_to):
                    os.mkdir(dir_to)
                for prm in prms + [
                    "Qappl",
                    "Qfill",
                    "Tred",
                    "Tbuf",
                ]:  # loop through parameters
                    if not exists(join(dir_to, prm)):
                        os.mkdir(join(dir_to, prm))

                # calculate necessary buffer
                Tbuffer = xr.ones_like(Tred[0, 0]) * float(bufmm)
                idfwrite(join(dir_to, "Tred_{:02d}mm.idf".format(bufmm)), Tbuffer)
                buftot = Tbuffer.sum().values

                # loop through years and use buffer for reducing Tred
                if overwrite or not exists(
                    join(
                        dir_to,
                        filepatterns["msw_Trel"].replace(
                            "<DATE>", str(period[1] * 10000 + 1231)
                        ),
                    )
                ):
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        print(idec, dec)

                        # new year: reset buffer:
                        if dec_year(idec) == 0:
                            Tbufyear = Tbuffer.copy(deep=True)

                        iTred = Tred.isel(time=idec, layer=0).load()
                        # set Tred to zero outside growing season
                        inseason = xr.zeros_like(start_dec).where(
                            dec_year(idec) + 1 < start_dec, 1.0
                        )
                        inseason = inseason.where(dec_year(idec) + 1 <= end_dec, 0.0)
                        iTred = iTred.where(inseason == 1.0, 0.0)

                        if (
                            iTred.sum().values > 0
                            or Tbufyear.sum().values < buftot - 1.0
                        ):
                            iTpot = Tpot.isel(time=idec, layer=0)
                            iTact = Tact.isel(time=idec, layer=0)
                            iPm = Pm.isel(time=idec, layer=0)

                            # correct Tact for water applied from buffer
                            # applied water = min(Tred, Tbuffer)
                            # appl = xr.zeros_like(Tbuffer).where(iTred<=Tbuffer,iTred,Tbuffer)
                            appl = iTred.where(iTred <= Tbufyear, Tbufyear)
                            appl = appl.where(
                                dec_year(idec) + 1 >= start_dec, 0.0
                            )  # limit to growing season
                            appl = appl.where(dec_year(idec) + 1 <= end_dec, 0.0)

                            iTred = iTred - appl
                            iTact = iTact + appl
                            Tbufyear = Tbufyear - appl

                            print(
                                "Applied %.0f m. Leftover buffer: %.0f of %.0f total m"
                                % (
                                    appl.sum().values / 1000.0,
                                    Tbufyear.sum().values / 1000.0,
                                    buftot / 1000.0,
                                )
                            )

                            # refill buffer, when:
                            # - Tred == 0
                            # - Pm - Tpot > 0
                            # - Tbuf < bufmm
                            # negeert kwel, zijdelingse afstroming, etc
                            # en, wellicht nog belangrijker: alle neerslag vult extra buffer aan, terwijl gehele bodem leeg was.
                            # daarvoor een standaard store van 30 mm aangenomen. De extra buffer vult evenredig met de hele store
                            fill = (iPm - iTpot) * Tbuffer / (30 * Tbuffer)
                            fill = fill.where(fill > 0.0, 0.0)
                            fill = fill.where(Tred[idec, 0] == 0.0, 0.0)
                            Tbufred = Tbuffer - Tbufyear
                            fill = fill.where(fill < Tbufred, Tbufred)

                            Tbufyear = Tbufyear + fill
                            print(
                                "Refilled buffer with %.0f m. Leftover buffer: %.0f of %.0f max m"
                                % (
                                    fill.sum().values / 1000.0,
                                    Tbufyear.sum().values / 1000.0,
                                    buftot / 1000.0,
                                )
                            )

                            iTrel = iTact / iTpot

                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["msw_Tact"].replace(
                                        "<DATE>", str(dec)
                                    ),
                                ),
                                iTact / -1000.0,
                            )
                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["msw_Trel"].replace(
                                        "<DATE>", str(dec)
                                    ),
                                ),
                                iTrel,
                            )
                            if store_tmp:
                                idfwrite(
                                    join(
                                        dir_to,
                                        filepatterns["Qappl"].replace(
                                            "<DATE>", str(dec)
                                        ),
                                    ),
                                    appl,
                                )
                                idfwrite(
                                    join(
                                        dir_to,
                                        filepatterns["Qfill"].replace(
                                            "<DATE>", str(dec)
                                        ),
                                    ),
                                    fill,
                                )
                                idfwrite(
                                    join(
                                        dir_to,
                                        filepatterns["Tbuf"].replace(
                                            "<DATE>", str(dec)
                                        ),
                                    ),
                                    Tbufyear,
                                )
                                idfwrite(
                                    join(
                                        dir_to,
                                        filepatterns["Tred"].replace(
                                            "<DATE>", str(dec)
                                        ),
                                    ),
                                    iTred,
                                )
                        else:
                            # copy files
                            for prm in ["msw_Tact", "msw_Trel"]:
                                copy(
                                    join(
                                        dir_from,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                    join(
                                        dir_to,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                )

                # move other files
                for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                    print(prm)
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        if overwrite or not exists(
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec)))
                        ):
                            move(
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )

                for fn in simgro_files:
                    fnfrom = join(dir_from, fn)
                    fnto = join(dir_to, fn)
                    if overwrite or not exists(fnto):
                        print(fnfrom)
                        copy(fnfrom, fnto)

                # Done, run Agricom
                if overwrite or not exists(
                    join(dir_to, "grids", "droogteschade_{}1231.idf".format(period[1]))
                ):
                    # RUN AGRICOM
                    agricomdict["DirInp"] = dir_to
                    agricomdict["DirOut"] = join(dir_to, "grids")
                    agricomdict["beregening"] = "nee"
                    agricom_ini = join(
                        bpscen, "agricom_buffer_{:02d}mm.inp".format(bufmm)
                    )
                    # write_agricom_ini(agricom_ini, agricomdict)
                    run_agricom(agricom_ini, agricomdict)

                # move other files back
                for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                    print(prm)
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        move(
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec))),
                            join(
                                dir_from, filepatterns[prm].replace("<DATE>", str(dec))
                            ),
                        )

        ##############################################################################################
        #### STEP 5: Proportional, recalculate Tact for proportionally reduced transpiration reduction
        ##############################################################################################
        if "Proportional" in do_steps:
            print("EXECUTING Proportional...")
            dir_from = dir_withsprinkling

            start_dec = idfload(
                join(dir_withsprinkling, "grids", "agricom_startdec.idf")
            )
            end_dec = idfload(join(dir_withsprinkling, "grids", "agricom_enddec.idf"))

            Tpot = (
                idfload(join(dir_withsprinkling, "msw_Tpot", "msw_Tpot*.idf")) * -1000.0
            )
            Tact = (
                idfload(join(dir_nosprinkling, "msw_Tact", "msw_Tact*.idf")) * -1000.0
            )
            Tred = Tpot - Tact

            for pct in proportional_pcts:
                print(f"Proportional reduction of {pct} %")

                dir_to = f"{dir_proportional}_{pct:03d}"
                if not exists(dir_to):
                    os.mkdir(dir_to)
                for prm in prms + ["Tred"]:  # loop through parameters
                    if not exists(join(dir_to, prm)):
                        os.mkdir(join(dir_to, prm))

                # loop through years and reduce Tred proportionally
                if overwrite or not exists(
                    join(
                        dir_to,
                        filepatterns["msw_Trel"].replace(
                            "<DATE>", str(period[1] * 10000 + 1231)
                        ),
                    )
                ):
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        print(idec, dec)

                        iTred = Tred.isel(time=idec, layer=0).load()
                        # set Tred to zero outside growing season
                        inseason = xr.zeros_like(start_dec).where(
                            dec_year(idec) + 1 < start_dec, 1.0
                        )
                        inseason = inseason.where(dec_year(idec) + 1 <= end_dec, 0.0)
                        iTred = iTred.where(inseason == 1.0, 0.0)
                        if iTred.sum().values > 0:
                            iTpot = Tpot.isel(time=idec, layer=0)
                            iTact = Tact.isel(time=idec, layer=0)

                            # correct Tact for reduced reduction
                            iTact = iTact + (iTred * pct / 100.0)
                            iTred = iTred * (1.0 - pct / 100.0)
                            iTrel = iTact / iTpot

                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["msw_Tact"].replace(
                                        "<DATE>", str(dec)
                                    ),
                                ),
                                iTact / -1000.0,
                            )
                            idfwrite(
                                join(
                                    dir_to,
                                    filepatterns["msw_Trel"].replace(
                                        "<DATE>", str(dec)
                                    ),
                                ),
                                iTrel,
                            )
                            if store_tmp:
                                idfwrite(
                                    join(
                                        dir_to,
                                        filepatterns["Tred"].replace(
                                            "<DATE>", str(dec)
                                        ),
                                    ),
                                    iTred,
                                )
                        else:
                            # copy files
                            for prm in ["msw_Tact", "msw_Trel"]:
                                copy(
                                    join(
                                        dir_from,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                    join(
                                        dir_to,
                                        filepatterns[prm].replace("<DATE>", str(dec)),
                                    ),
                                )

                # move other files
                for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                    print(prm)
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        if overwrite or not exists(
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec)))
                        ):
                            move(
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )

                for fn in simgro_files:
                    fnfrom = join(dir_from, fn)
                    fnto = join(dir_to, fn)
                    if overwrite or not exists(fnto):
                        print(fnfrom)
                        copy(fnfrom, fnto)

                # Done, run Agricom
                if overwrite or not exists(
                    join(dir_to, "grids", "droogteschade_{}1231.idf".format(period[1]))
                ):
                    # RUN AGRICOM
                    agricomdict["DirInp"] = dir_to
                    agricomdict["DirOut"] = join(dir_to, "grids")
                    agricomdict["beregening"] = "nee"
                    agricom_ini = join(
                        bpscen, "agricom_proportional_{}.inp".format(pct)
                    )
                    # write_agricom_ini(agricom_ini, agricomdict)
                    run_agricom(agricom_ini, agricomdict)

                # move other files back
                for prm in prms_unchanged:  # + ['bdgPssw','bdgPsgw']:
                    print(prm)
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        move(
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec))),
                            join(
                                dir_from, filepatterns[prm].replace("<DATE>", str(dec))
                            ),
                        )

        ##############################################################################################
        #### STEP 6: Salt, recalculate salt damage for different rootzone concentrations
        ##############################################################################################
        if "Salt" in do_steps:
            print("EXECUTING Salt...")
            dir_from = dir_withsprinkling

            for conc in salt_concentrations:
                print(f"Salt concentration of {conc} mg/l")

                dir_to = f"{dir_salt}_{conc}"
                if not exists(dir_to):
                    os.mkdir(dir_to)
                for prm in prms:  # create subdirs
                    if not exists(join(dir_to, prm)):
                        os.mkdir(join(dir_to, prm))

                transol = idfload(
                    join(
                        dir_from,
                        filepatterns["transol_con_rz"].replace(
                            "<DATE>", str(period[1] * 10000 + 1231)
                        ),
                    )
                )
                iConc = xr.ones_like(transol) * conc / 1000.0  # (in g/l)
                # loop through years and write salt concentration
                if overwrite or not exists(
                    join(
                        dir_to,
                        filepatterns["transol_con_rz"].replace(
                            "<DATE>", str(period[1] * 10000 + 1231)
                        ),
                    )
                ):
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        print(idec, dec)
                        idfwrite(
                            join(
                                dir_to,
                                filepatterns["transol_con_rz"].replace(
                                    "<DATE>", str(dec)
                                ),
                            ),
                            iConc,
                        )

                # move other files
                prms_nosalt = prms.copy()
                prms_nosalt.remove("transol_con_rz")
                for prm in prms_nosalt:
                    print(prm)
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        if overwrite or not exists(
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec)))
                        ):
                            move(
                                join(
                                    dir_from,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                                join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                ),
                            )

                for fn in simgro_files:
                    fnfrom = join(dir_from, fn)
                    fnto = join(dir_to, fn)
                    if overwrite or not exists(fnto):
                        print(fnfrom)
                        copy(fnfrom, fnto)

                # Done, run Agricom
                if overwrite or not exists(
                    join(dir_to, "grids", "droogteschade_{}1231.idf".format(period[1]))
                ):
                    # RUN AGRICOM
                    agricomdict["DirInp"] = dir_to
                    agricomdict["DirOut"] = join(dir_to, "grids")
                    agricom_ini = join(bpscen, "agricom_salt_{}.inp".format(conc))
                    # write_agricom_ini(agricom_ini, agricomdict)
                    run_agricom(agricom_ini, agricomdict)

                # move other files back
                for prm in prms_nosalt:
                    print(prm)
                    for idec, dec in get_timesteps(
                        *period, start_or_end="end", dec_or_days=dec_or_days
                    ):
                        move(
                            join(dir_to, filepatterns[prm].replace("<DATE>", str(dec))),
                            join(
                                dir_from, filepatterns[prm].replace("<DATE>", str(dec))
                            ),
                        )

        ##############################################################################################
        #### Finalize: create Regioscan Database from calculation results
        ##############################################################################################
        if "RegioscanDB" in do_steps:
            print("EXECUTING RegioscanDB...")

            dir_to = dir_regioscandb
            if not exists(dir_to):
                os.mkdir(dir_to)
            for subdir in [
                "Buffer",
                "Evenredig",
                "MetBeregening",
                "ZonderBeregening",
                "Zout",
            ]:  # loop through parameters
                if not exists(join(dir_to, subdir)):
                    os.mkdir(join(dir_to, subdir))

            # Met en zonder beregening
            for dir_from, dir_to in zip(
                [dir_withsprinkling, dir_nosprinkling],
                ["MetBeregening", "ZonderBeregening"],
            ):
                if exists(dir_from):
                    dir_to = join(dir_regioscandb, dir_to)
                    tpot = idfload(join(dir_from, "grids", "Tpot_*1231.idf"))
                    tact = idfload(join(dir_from, "grids", "Tact_*1231.idf"))
                    tred = tpot - tact
                    # print(tred)
                    fns = [
                        "droogteschade",
                        "totschadefractie",
                        "Tpot",
                        "Tact",
                        "beregening_sw",
                        "beregening_gw",
                    ]  # ,'zoutschade'
                    for i, year in enumerate(range(period[0], period[1] + 1)):
                        for fn in fns:
                            copy(
                                join(
                                    dir_from, "grids", "{}_{}1231.idf".format(fn, year)
                                ),
                                dir_to,
                            )
                        idfwrite(join(dir_to, "Tred_{}1231.idf".format(year)), tred[i])

            # Buffer
            dir_to = join(dir_regioscandb, "Buffer")
            for bufpct in extbuffer_percentages:
                dir_from = f"{dir_buffer}_{bufpct:03d}"
                if exists(dir_from):
                    copy(
                        join(dir_from, "Tred_{:03d}.idf".format(bufpct)),
                        join(dir_to, "Tred_{:2d}perc.idf".format(bufpct)),
                    )
                    tpot = idfload(join(dir_from, "grids", "Tpot_*1231.idf"))
                    tact = idfload(join(dir_from, "grids", "Tact_*1231.idf"))
                    tred = tpot - tact
                    for year in range(period[0], period[1] + 1):
                        copy(
                            join(
                                dir_from,
                                "grids",
                                "droogteschade_{}1231.idf".format(year),
                            ),
                            join(
                                dir_to,
                                "droogteschade_{}_{:03d}.idf".format(year, bufpct),
                            ),
                        )
                        idfwrite(
                            join(dir_to, "Tred_{}1231_{:03d}.idf".format(year, bufpct)),
                            tred[i],
                        )

            for bufmm in soilbuffer_mms:
                dir_from = f"{dir_buffer}_{bufmm:02d}mm"
                if exists(dir_from):
                    tpot = idfload(join(dir_from, "grids", "Tpot_*1231.idf"))
                    tact = idfload(join(dir_from, "grids", "Tact_*1231.idf"))
                    tred = tpot - tact
                    for i, year in enumerate(range(period[0], period[1] + 1)):
                        copy(
                            join(
                                dir_from,
                                "grids",
                                "droogteschade_{}1231.idf".format(year),
                            ),
                            join(
                                dir_to,
                                "droogteschade_{}_{:02d}mm.idf".format(year, bufmm),
                            ),
                        )
                        idfwrite(
                            join(
                                dir_to, "Tred_{}1231_{:02d}mm.idf".format(year, bufmm)
                            ),
                            tred[i],
                        )

            # Evenredig
            dir_to = join(dir_regioscandb, "Evenredig")
            for pct in proportional_pcts:
                dir_from = f"{dir_proportional}_{pct:03d}"
                if exists(dir_from):
                    for year in range(period[0], period[1] + 1):
                        copy(
                            join(
                                dir_from,
                                "grids",
                                "droogteschade_{}1231.idf".format(year),
                            ),
                            join(
                                dir_to, "droogteschade_{}_{:03d}.idf".format(year, pct)
                            ),
                        )

            # Salt
            dir_to = join(dir_regioscandb, "Zout")
            for conc in salt_concentrations:
                dir_from = f"{dir_salt}_{conc}"
                if exists(dir_from):
                    for year in range(period[0], period[1] + 1):
                        copy(
                            join(
                                dir_from, "grids", "zoutschade_{}1231.idf".format(year)
                            ),
                            join(dir_to, "zoutschade_{}_{:04d}.idf".format(year, conc)),
                        )

        ##############################################################################################
        #### Cleanup: delete temporary calculation results
        ##############################################################################################
        if "Cleanup" in do_steps:
            print("EXECUTING Cleanup...")

            def cleanup_dir(dir_to, rmdir=False):
                if exists(dir_to):
                    if rmdir:
                        rmtree(dir_to)
                    else:
                        for prm in filepatterns.keys():
                            for idec, dec in get_timesteps(
                                *period, start_or_end="end", dec_or_days=dec_or_days
                            ):
                                fn = join(
                                    dir_to,
                                    filepatterns[prm].replace("<DATE>", str(dec)),
                                )
                                if exists(fn):
                                    os.remove(fn)
                        for fn in simgro_files:
                            if exists(join(dir_to, fn)):
                                os.remove(join(dir_to, fn))
                        if exists(join(dir_to, "grids")):
                            for year in range(period[0], period[1] + 1):
                                for fn in glob.glob(
                                    join(dir_to, "grids", "*{}1231.idf".format(year))
                                ):
                                    os.remove(fn)
                            for fn in [
                                "areaal.idf",
                                "beregeningstype.idf",
                                "bodemtype.idf",
                                "landgebruik.idf",
                            ]:
                                if exists(join(dir_to, "grids", fn)):
                                    os.remove(join(dir_to, "grids", fn))

            rmdir = period[1] == yearend

            # First save NoSprinkling Tact
            if not exists(dir_store):
                os.mkdir(dir_store)
            for fn in ["agricom_startdec.idf", "agricom_enddec.idf"]:
                if not exists(join(dir_store, fn)):
                    move(join(dir_withsprinkling, fn), dir_store)

            # Met en zonder beregening
            cleanup_dir(dir_withsprinkling, rmdir)
            cleanup_dir(dir_nosprinkling, rmdir)

            # Buffer
            for bufpct in extbuffer_percentages:
                dir_bufpct = f"{dir_buffer}_{bufpct:03d}"
                cleanup_dir(dir_bufpct, rmdir)

            for bufmm in soilbuffer_mms:
                dir_bufmm = f"{dir_buffer}_{bufmm:02d}mm"
                cleanup_dir(dir_bufmm, rmdir)

            # Evenredig
            for pct in proportional_pcts:
                dir_pct = f"{dir_proportional}_{pct:03d}"
                cleanup_dir(dir_pct, rmdir)

            # Salt
            for conc in salt_concentrations:
                dir_conc = f"{dir_salt}_{conc}"
                cleanup_dir(dir_conc, rmdir)

        with open("done.txt", "w") as f:
            f.write("done")
