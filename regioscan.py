#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Run Regioscan from command prompt
"""
from regioscan_zoetwater.regioscan import run

if __name__ == "__main__":
    run()
