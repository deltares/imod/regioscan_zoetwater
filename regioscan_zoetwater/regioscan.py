#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following line in the
entry_points section in setup.cfg:

    console_scripts =
     fibonacci = regioscan_zoetwater.skeleton:run

Then run `python setup.py install` which will install the command `fibonacci`
inside your current environment.
Besides console scripts, the header (i.e. until logging...) of this file can
also be used as template for Python modules.

Note: This skeleton file can be safely removed if not needed!
"""
from __future__ import division, print_function, absolute_import

import argparse
import sys
import logging.config
from os.path import abspath, join, exists, dirname
from os import scandir, getenv, makedirs
from collections import OrderedDict


import numpy as np
import pandas as pd
import yaml
import pickle
from pandas import DataFrame

from regioscan_zoetwater.paths import set_paths
from regioscan_zoetwater import __version__
from regioscan_zoetwater.utils import (
    csv_from_series,
    read_ini,
    MeasuresDB,
    create_default_impl_deg_table,
)
from regioscan_zoetwater.reference import run_reference_agricom
from regioscan_zoetwater.measures import calculate_measures, calc_drought_by_table
from regioscan_zoetwater.result import process_results
from regioscan_zoetwater.company import HydroInput
from regioscan_zoetwater.constants import *
from regioscan_zoetwater.reference import calculate_reference

# disable pandas SettingWithCopy warning
pd.options.mode.chained_assignment = None  # default='warn'
# disable numpy All-NaN slice warning
np.warnings.filterwarnings("ignore", r"All-NaN (slice|axis) encountered")

__author__ = "Maarten Pronk"
__copyright__ = "Maarten Pronk"
__license__ = "none"


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(description="Regioscan Zoetwatermaatregelen")
    parser.add_argument(
        "-i",
        "-ini",
        dest="fn_ini",
        help="filename initialisation file",
        action="store",
        default="regioscan.ini",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        help="set loglevel to INFO",
        action="store_const",
        const=logging.INFO,
    )
    parser.add_argument(
        "-vv",
        "--very-verbose",
        dest="loglevel",
        help="set loglevel to DEBUG",
        action="store_const",
        const=logging.DEBUG,
    )
    return parser.parse_args(args)


def setup_logging(path="config/logger.yaml", default_level=logging.INFO):
    """Setup logging configuration."""
    currentdir = dirname(abspath(__file__))
    if exists(join(currentdir, path)):
        with open(join(currentdir, path), "rt") as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)
    logging.captureWarnings(True)


def main(args):
    """Main entry point allowing external calls.

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(default_level=args.loglevel)
    logging.info("Starting Regioscan calculations...")

    ini = read_ini(args.fn_ini)
    logging.info(
        "Calculation period: {} to {}".format(ini["startyear"], ini["endyear"])
    )
    pths = set_paths(ini)
    makedirs(pths["scenario_output"], exist_ok=True)

    if ini["use_existing"] and exists(join(pths["scenario_output"], "hi.pickle")):
        logging.info("Reading company information from existing data")
        with open(join(pths["scenario_output"], "hi.pickle"), "rb") as fp:
            hi = pickle.load(fp)
            companies_df = pickle.load(fp)
    else:
        logging.info("Reading company information")
        hi = HydroInput(pths["scenario_input"], pths["subarea_fn"])
        companies_df = hi.companies_df()
        companies_df.to_csv(join(pths["scenario_output"], "CompaniesGeneral.csv"))

        with open(join(pths["scenario_output"], "hi.pickle"), "wb") as fp:
            pickle.dump(hi, fp, pickle.HIGHEST_PROTOCOL)
            pickle.dump(companies_df, fp, pickle.HIGHEST_PROTOCOL)

    if ini["use_existing"] and exists(
        join(pths["referencefolder"], "reference.pickle")
    ):
        logging.info("Reading reference situation from existing data")

        with open(join(pths["referencefolder"], "reference.pickle"), "rb") as fp:
            maintenance_cost_reference = pickle.load(fp)
            csv_waterusage_reference = pickle.load(fp)
            act_ref = pickle.load(fp)
    else:
        # Run Reference agricom
        logging.info("Calculating reference situation")
        logging.info("Running Agricom")
        run_reference_agricom(
            pths["hydroinput"], pths["scenario_input"], pths["referencefolder"], ini
        )

        logging.info("Calculating costs and benefits")
        (
            maintenance_cost_reference,
            csv_waterusage_reference,
            act_ref,
        ) = calculate_reference(hi.companies_idf, hi, pths, ini)
        with open(join(pths["referencefolder"], "reference.pickle"), "wb") as fp:
            pickle.dump(maintenance_cost_reference, fp, pickle.HIGHEST_PROTOCOL)
            pickle.dump(csv_waterusage_reference, fp, pickle.HIGHEST_PROTOCOL)
            pickle.dump(act_ref, fp, pickle.HIGHEST_PROTOCOL)

    # Read measure combinations from database
    measuresdb = MeasuresDB(fn=pths["database"])
    combinations = measuresdb.get_measurecombinations()

    if not exists(ini["fn_impl_table"]):
        create_default_impl_deg_table(ini["fn_impl_table"], combinations, hi)

    combinations = {
        mcid: item for mcid, item in combinations.items() if mcid in ini["allowed_mcs"]
    }

    # if ini['use_existing']:
    #    logging.info("Reading measures results from existing data")
    ## TODO: read company_mc_results, company_mc_results_success from files
    # else:
    # with warnings.catch_warnings():
    #    warnings.simplefilter('error')

    company_mc_results, company_mc_results_success = calculate_measures(
        combinations,
        hi,
        ini,
        act_ref,
        maintenance_cost_reference,
        csv_waterusage_reference,
        pths,
        measuresdb,
    )

    process_results(
        company_mc_results, company_mc_results_success, companies_df, hi, ini, pths
    )
    logging.info("Regioscan end")


def run(args=sys.argv[1:]):
    """Entry point for console_scripts."""
    print("\n################################################")
    print("################################################")
    print("#####                                      #####")
    print("#####    Regioscan Zoetwatermaatregelen    #####")
    print("#####                                      #####")
    print("################################################")
    print("################################################\n")
    main(args)


if __name__ == "__main__":
    run()
