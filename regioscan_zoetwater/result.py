import numpy as np
import logging
from os.path import join, exists
from os import makedirs
from math import floor
from pandas import DataFrame, Series, concat
from regioscan_zoetwater.constants import *
from regioscan_zoetwater.utils import csv_from_dict, csv2idf, order_cols


def write_results_df(results):
    df = DataFrame.from_dict(
        DataFrame.from_dict(
            {(i, j): results[i][j] for i in results.keys() for j in results[i].keys()},
            orient="index",
        )
    )
    df.reset_index(inplace=True)
    df.rename(
        columns={"WaterUsage": QAPP_GW_SW_, "level_1": COMPANY_NUMBER_, "level_0": MC_},
        inplace=True,
    )
    return df[order_cols(df.columns)]


def rank_df(df):
    # Sort and rank by order
    df.sort_values(
        [COMPANY_NUMBER_, NBC_, QAPP_GW_SW_, MC_],
        ascending=[True, False, True, True],
        inplace=True,
    )

    df[RANK_] = df.groupby(COMPANY_NUMBER_)[COMPANY_NUMBER_].rank(method="first")

    return df


def get_rank_one(df):
    df_nr_one = df.loc[df[RANK_] == 1.0]
    return df_nr_one


# REZOWA-49 TODO maybe do this per columm
def sum_2_columns(df, selection, column_1, column_2):
    area_range_val = df[selection].max() + 1

    benefits_per_year = []
    costs_per_year = []
    used_variations = []

    for i in range(int(area_range_val)):
        #         df.loc[df[selection][col] == i].sum()
        df_selection = df.loc[df[selection] == i]
        if len(df_selection) > 0:
            sum_column1 = df_selection[column_1].sum()
            sum_column2 = df_selection[column_2].sum()
            #         if sum_column1 > 0 or sum_column2 > 0:
            used_variations.append(i)
            cost = round(sum_column1)
            benfits = round(sum_column2)
            benefits_per_year.append(benfits)
            costs_per_year.append(cost)

    return used_variations, costs_per_year, benefits_per_year


def sum_column(df, selection, col, rounded=False):
    unique = sorted(df[selection].unique())

    used_variations = []
    summed = []
    for i in unique:
        df_selection = df.loc[df[selection] == i]
        if len(df_selection) > 0:
            used_variations.append(i)
            sumcol = df_selection[col].sum()
            if rounded:
                summed.append(round(sumcol))
            else:
                summed.append(sumcol)
    return used_variations, summed


# REZOWA-49
# REZOWA-53
# REZOWA-104
def sum_over_companytype(df):
    if df.empty:
        return df

    area_range_val = df[SUBAREA_].max() + 1

    dict_ctype = {
        SUBAREA_: [],
        COMPANY_TYPE_: [],
        "Costs_EuroPerYear": [],
        "BenefitsEuroPerYear": [],
        "Watergebruik_gw_m3": [],
        "Toename_gw_prc": [],
        "Watergebruik_sw_m3": [],
        "Toename_sw_prc": [],
    }
    dict_ctype.update({se: [] for se in SIDEEFFECTS_})

    for i in range(int(area_range_val)):
        df_selection = df.loc[df[SUBAREA_] == i]
        if len(df_selection) > 0:

            used_variations, costs_per_year = sum_column(
                df_selection, COMPANY_TYPE_, COST_, rounded=True
            )
            used_variations2, benefits_per_year = sum_column(
                df_selection, COMPANY_TYPE_, BENEFIT_, rounded=True
            )
            assert used_variations == used_variations2
            used_variations2, gw_per_year = sum_column(
                df_selection, COMPANY_TYPE_, GROUND_WATER_, rounded=True
            )
            assert used_variations == used_variations2
            used_variations2, sw_per_year = sum_column(
                df_selection, COMPANY_TYPE_, SURFACE_WATER_, rounded=True
            )
            assert used_variations == used_variations2
            used_variations2, change_gw_per_year = sum_column(
                df_selection, COMPANY_TYPE_, TOENAME_GW_, rounded=True
            )
            assert used_variations == used_variations2
            used_variations2, change_sw_per_year = sum_column(
                df_selection, COMPANY_TYPE_, TOENAME_SW_, rounded=True
            )
            assert used_variations == used_variations2
            secols = {}
            for se in SIDEEFFECTS_:
                used_variations2, secols[se] = sum_column(
                    df_selection, COMPANY_TYPE_, se
                )
                assert used_variations == used_variations2

            for j in range(
                len(used_variations)
            ):  # variations, benefits, costs in used_variations, benefits_per_year, costs_per_year:
                dict_ctype[SUBAREA_].append(i)
                dict_ctype[COMPANY_TYPE_].append(used_variations[j])
                dict_ctype["Costs_EuroPerYear"].append(costs_per_year[j])
                dict_ctype["BenefitsEuroPerYear"].append(benefits_per_year[j])
                dict_ctype["Watergebruik_gw_m3"].append(gw_per_year[j])
                dict_ctype["Toename_gw_prc"].append(change_gw_per_year[j])
                dict_ctype["Watergebruik_sw_m3"].append(sw_per_year[j])
                dict_ctype["Toename_sw_prc"].append(change_sw_per_year[j])
                # REZOWA-104
                for se in SIDEEFFECTS_:
                    dict_ctype[se].append(secols[se][j])

    df_type = DataFrame(dict_ctype)
    return df_type


def sum_over_subarea(df):
    if df.empty:
        return df

    dict_area = {
        SUBAREA_: [],
        "Costs_EuroPerYear": [],
        "BenefitsEuroPerYear": [],
        "Watergebruik_gw_m3": [],
        "Toename_gw_prc": [],
        "Watergebruik_sw_m3": [],
        "Toename_sw_prc": [],
    }
    dict_area.update({se: [] for se in SIDEEFFECTS_})

    used_variations, costs_per_year = sum_column(
        df, SUBAREA_, "Costs_EuroPerYear", rounded=True
    )
    used_variations2, benefits_per_year = sum_column(
        df, SUBAREA_, "BenefitsEuroPerYear", rounded=True
    )
    assert used_variations == used_variations2
    used_variations2, gw_per_year = sum_column(
        df, SUBAREA_, "Watergebruik_gw_m3", rounded=True
    )
    assert used_variations == used_variations2
    used_variations2, sw_per_year = sum_column(
        df, SUBAREA_, "Watergebruik_sw_m3", rounded=True
    )
    assert used_variations == used_variations2
    used_variations2, change_gw_per_year = sum_column(
        df, SUBAREA_, "Toename_gw_prc", rounded=True
    )
    assert used_variations == used_variations2
    used_variations2, change_sw_per_year = sum_column(
        df, SUBAREA_, "Toename_sw_prc", rounded=True
    )
    assert used_variations == used_variations2
    secols = {}
    for se in SIDEEFFECTS_:
        used_variations2, secols[se] = sum_column(df, SUBAREA_, se)
        assert used_variations == used_variations2

    for j in range(
        len(used_variations)
    ):  # variations, benefits, costs in used_variations, benefits_per_year, costs_per_year:
        dict_area[SUBAREA_].append(used_variations[j])
        dict_area["Costs_EuroPerYear"].append(costs_per_year[j])
        dict_area["BenefitsEuroPerYear"].append(benefits_per_year[j])
        dict_area["Watergebruik_gw_m3"].append(gw_per_year[j])
        dict_area["Toename_gw_prc"].append(change_gw_per_year[j])
        dict_area["Watergebruik_sw_m3"].append(sw_per_year[j])
        dict_area["Toename_sw_prc"].append(change_sw_per_year[j])
        # REZOWA-104
        for se in SIDEEFFECTS_:
            dict_area[se].append(secols[se][j])
    df_subarea = DataFrame(dict_area)

    return df_subarea


# REZOWA-48
#             df_sa_selection.head(max_cols, inplace=True)
#             end_row = df[df.subarea > sa].index.tolist() #get index where next subarea starts
#             df.drop(df.index[[df_sa_selection,end_row]], inplace=True)
def get_implementationdegree(subarea_col, implementation_degree):
    unique, counts = np.unique(subarea_col, return_counts=True)
    sa_companies_col = dict(zip(unique, counts))
    sa_implementation_number_col = {}
    for subarea in sa_companies_col:
        sa_implementation_number_col[subarea] = (
            sa_companies_col[subarea] * implementation_degree / 100
        )
    all_columns = {
        "Companies": sa_companies_col,
        "CompanySuccess": {},
        "ImplementationDegree_percent": implementation_degree,
        "Implementation_number": sa_implementation_number_col,
        "Selected": {},
    }
    return sa_implementation_number_col, all_columns


def rank_by_implementationdegree(df, sa_implementation_number_col, all_columns):
    data_frames = []
    for sa in sa_implementation_number_col:
        implementation_number = int(round(sa_implementation_number_col[sa]))
        df_sa_selection = df.loc[df[SUBAREA_] == int(sa)]
        company_success = len(df_sa_selection)
        max_select = min(company_success, implementation_number)
        if max_select < company_success:
            df_sa_selection = df_sa_selection.head(max_select)

        all_columns["CompanySuccess"].update({sa: company_success})
        all_columns["Selected"].update({sa: max_select})

        data_frames.append(df_sa_selection)

    df_by_impl_dg = concat(data_frames)

    return df_by_impl_dg, all_columns


def get_implementationdegree_table(df, companies_df, impl_deg_table):
    # REZOWA-50
    # loop through implementation degree dict
    # make subselection based on best measures companytype per subarea
    grouped = companies_df.groupby(["subarea", "companytype"])
    company_col = {}
    company_col_fwoo = {}
    company_col_sel = {}
    company_col_fwoo_sel = {}
    for (sub, ctype), group in grouped:
        if sub not in company_col:
            company_col[sub] = {}
            company_col_fwoo[sub] = {}
            company_col_sel[sub] = {}
            company_col_fwoo_sel[sub] = {}
        company_col[sub][ctype] = len(group)
        company_col_fwoo[sub][ctype] = len(
            df.loc[(df["subarea"] == sub) & (df["companytype"] == ctype)]
        )
        company_col_sel[sub][ctype] = 0
        company_col_fwoo_sel[sub][ctype] = 0

    implemented = Series(index=df.index, data=False, name="Implemented")
    for sub in impl_deg_table["subarea"].unique():
        dfselsub = df.ix[df["subarea"] == sub]
        for ctype in impl_deg_table.loc[impl_deg_table["subarea"] == sub][
            "companytype"
        ].unique():
            idegree = impl_deg_table.loc[
                (impl_deg_table["subarea"] == sub)
                & (impl_deg_table["companytype"] == ctype)
            ]
            dfselctype = dfselsub.ix[dfselsub["companytype"] == ctype]
            dfselctype = dfselctype.ix[
                dfselctype["mc"].isin(list(idegree["measurecombination"]))
            ]

            companies_impl = []
            # no of companies with certain comptype
            ncomp = company_col[sub][ctype]
            idegree = idegree.set_index("measurecombination")
            idegree = (idegree["impl_degree"] / 100.0 * ncomp).astype(int)
            total = idegree.sum()
            company_col_sel[sub][ctype] = total

            # already sorted on NBC
            # from the top downwards:
            # is mc still in idegree3 and is company not yet used: -> implement, copy to dataframes
            for i, row in dfselctype.iterrows():
                if (
                    idegree.ix[row["mc"]] > 0
                    and row["CompanyNumber"] not in companies_impl
                ):
                    idegree.ix[row["mc"]] -= 1
                    total -= 1
                    companies_impl.append(row["CompanyNumber"])
                    implemented[i] = True

                    if total == 0:  # no more measures left: break from loop
                        break
            company_col_fwoo_sel[sub][ctype] = company_col_sel[sub][ctype] - total

    # df['implemented'] = implemented
    # df.to_csv('test.csv')

    all_columns = {
        "Companies": company_col,
        "Companies_fwoo": company_col_fwoo,
        "Selected": company_col_sel,
        "Selected_fwoo": company_col_fwoo_sel,
    }

    return df.ix[implemented], all_columns


def get_adaptationdegree(subarea_col, df, adaptation_boundary):
    data_frames = []
    unique, counts = np.unique(subarea_col, return_counts=True)
    sa_companies_col = dict(zip(unique, counts))
    sa_col = {}
    sa_success_col = {}
    sa_selection_col = {}
    for subarea in sa_companies_col:
        sa_col[subarea] = int(subarea)

        df_sa_success = df.loc[(df[SUBAREA_] == int(subarea))]
        sa_success_col[subarea] = len(df_sa_success)

        df_sa_selection = df.loc[
            (df[SUBAREA_] == int(subarea)) & (df[NBC_] > adaptation_boundary)
        ]
        sa_selection_col[subarea] = len(df_sa_selection)
        data_frames.append(df_sa_selection)

    df_by_adaptation = concat(data_frames)
    all_columns = {"CompanySuccess": sa_success_col, "Selected": sa_selection_col}

    return df_by_adaptation, all_columns


# REZOWA-79
def process_results(
    company_mc_results, company_mc_results_success, companies_df, hi, ini, pths
):
    selection_by_impl_deg = ini["select_by_impl"]
    selection_by_impl_deg_table = ini["select_by_impl_table"]
    implementation_degree_single = ini["impl_deg_single"]
    implementation_degree_table = ini["impl_deg_table"]
    adaptation = ini["adaptation"]

    # REZOWA-47
    # Per company rank all measure combinations on NBC and Waterusage (groundwater and surfacewater)
    df = write_results_df(company_mc_results)
    df = rank_df(df)
    df["Area_ha"] = df[COMPANY_NUMBER_].map(hi.company_cells) * hi.dx * hi.dy / 10000.0
    df.sort_values([SUBAREA_, COMPANY_TYPE_], ascending=[True, True], inplace=True)
    fn = join(pths["scenario_output"], "AllResultsRankedPerCompany.csv")
    logging.info("Writing {}".format(fn))
    df.to_csv(fn, index=False, sep=";")

    df = write_results_df(company_mc_results_success)
    if len(df):
        df = rank_df(df)
        df["Area_ha"] = (
            df[COMPANY_NUMBER_].map(hi.company_cells) * hi.dx * hi.dy / 10000.0
        )
        df.sort_values([SUBAREA_, COMPANY_TYPE_], ascending=[True, True], inplace=True)
        fn = join(pths["scenario_output"], "SuccessResultsRankedPerCompany.csv")
        logging.info("Writing {}".format(fn))
        df.to_csv(fn, index=False, sep=";")

        # REZOWA-48
        # simple implementation degree in % that maximizes the part of the companies (per subarea) that will take a measure

        # Select only the results with rank=1
        df1 = get_rank_one(df)
        fn = join(pths["scenario_output"], "ResultsRank1.csv")
        # TODO: onderstaande geeft warning (maar werkt wel)
        df1.sort_values(
            [SUBAREA_, NBC_, COST_, QAPP_GW_SW_],
            ascending=[True, False, True, True],
            inplace=True,
        )
        df1[RANK_] = df1.groupby(SUBAREA_)[SUBAREA_].rank(method="first")
        logging.info("Writing {}".format(fn))
        df1.to_csv(fn, index=False, sep=";")

        if selection_by_impl_deg:
            if not selection_by_impl_deg_table:
                logging.info("Selection by single implementation degree")
                # Apply implementation degree:
                # Select top x results where x = companies_per_subarea * implementation_degree / 100.
                sa_implementation_number_col, all_columns = get_implementationdegree(
                    hi.subareas, implementation_degree_single
                )
                df_by_impl_dg, all_columns = rank_by_implementationdegree(
                    df1, sa_implementation_number_col, all_columns
                )
                csv_fn = join(
                    pths["scenario_output"], "ControlSingleImplementationDegree.csv"
                )

                logging.info("Writing {}".format(csv_fn))
                csv_from_dict(csv_fn, all_columns, index_label="SubArea")
            else:
                # REZOWA-50
                logging.info("Selection by implementation degree table")
                df_by_impl_dg, all_columns = get_implementationdegree_table(
                    df, companies_df, implementation_degree_table
                )
                csv_fn = join(
                    pths["scenario_output"], "ControlMultipleImplementationDegree.csv"
                )

                logging.info("Writing {}".format(csv_fn))
                csv_from_dict(
                    csv_fn,
                    all_columns,
                    index_label="SubArea",
                    nested=True,
                    nested_label="CompanyType",
                )
        else:
            logging.info("Selection by autonomous adaptation")
            # Apply adaptation: only companies with a minimum NBC
            df_by_impl_dg, all_columns = get_adaptationdegree(
                hi.subareas, df1, adaptation
            )
            csv_fn = join(pths["scenario_output"], "ControlAdaptation.csv")
            logging.info("Writing {}".format(csv_fn))
            csv_from_dict(csv_fn, all_columns, index_label="SubArea")

        fncomp = join(pths["scenario_output"], "ResultsPerCompany.csv")
        logging.info("Writing {}".format(fncomp))
        df_by_impl_dg.to_csv(fncomp, index=False, sep=";")

        #   REZOWA-49
        df_type = sum_over_companytype(df_by_impl_dg)

        fncomptype = join(pths["scenario_output"], "ResultsPerCompanyType.csv")
        logging.info("Writing {}".format(fncomptype))
        df_type.to_csv(fncomptype, index=False, sep=";")

        df_subarea = sum_over_subarea(df_type)
        fnsub = join(pths["scenario_output"], "ResultsPerSubArea.csv")
        logging.info("Writing {}".format(fnsub))
        df_subarea.to_csv(fnsub, index=False, sep=";")

        #   REZOWA-97
        if ini["write_maps"]:
            makedirs(pths["scenario_output_maps"], exist_ok=True)
            makedirs(join(pths["scenario_output_maps"], "company"), exist_ok=True)
            # makedirs(join(scenario_output_maps,'companytype'), exist_ok=True)
            makedirs(join(pths["scenario_output_maps"], "subarea"), exist_ok=True)
            csv2idf(
                fncomp,
                index_col="CompanyNumber",
                index_idf=hi.companies_fn,
                dir_out=join(pths["scenario_output_maps"], "company"),
            )
            # csv2idf(fncomptype,index_col='CompanyType',index_idf=hi.companytypes_fn,dir_out=join(scenario_output_maps,'companytype'))
            csv2idf(
                fnsub,
                index_col="subarea",
                index_idf=hi.subarea_fn,
                dir_out=join(pths["scenario_output_maps"], "subarea"),
            )
    else:
        logging.info("No fwoo measures in study area")
