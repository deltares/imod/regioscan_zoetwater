#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
from os.path import join, exists, dirname
from os import scandir, getenv
from collections import OrderedDict

import numpy as np
import pandas as pd
from regioscan_zoetwater.exceptions import InputDataError
from regioscan_zoetwater.utils import unique, spatial_reference, idfopen


class HydroInput:
    def __init__(
        self,
        scenariofolder,
        subareas,
        companies_fn="modelbedrijven.idf",
        companytypes_fn="bedrijftypen.idf",
    ):
        self.companies_fn = join(scenariofolder, companies_fn)
        self.companytypes_fn = join(scenariofolder, companytypes_fn)
        self.subarea_fn = subareas

        self.companies, self.company_cells = self.read_companies()
        self.company_cells = self.companies_series()
        self.companytypes_idf = self._read_companies_sub(self.companytypes_fn)
        self.subareas_idf = self._read_companies_sub(self.subarea_fn)

        # get subareas and comptypes
        self.subareas = self.sub_series(self.subareas_idf, "Subarea")
        self.companytypes = self.sub_series(self.companytypes_idf, "Companytypes")
        self.compare_headers(scenariofolder)

    # REZOWA-7
    def compare_headers(self, folder):
        """Compare all metadata for IDF files in one folder."""
        refheader = None

        # Read header data for each idf file in folder
        for entry in scandir(folder):
            if entry.name.lower().endswith(".idf") and entry.is_file():
                header = spatial_reference(idfopen(entry.path, pattern="{name}"))

                # Store header in case of first file
                if refheader is None:
                    refheader = header
                    continue
                # Compare header information by taking XOR of previous header
                else:
                    diff = set(refheader) ^ set(header)

                # If diff contains elements, the dimensions is different
                if len(diff) != 0:
                    logging.error(
                        "{} has different dimensions: {}".format(entry.name, header)
                    )
                    return False

        return True

    #    def plot_modelcompanies(self):
    #        # Plot model companies
    #        plt.imshow(self.companies_idf.arr)
    #        plt.show()

    def read_companies(self):
        """
        REZOWA-7
        # Read grid ModelBedrijven.idf with Integers that represent modelcompanies.
        # Read cell size from grid. Expected is 250x250 m. Keep cell size in memory. Write cell size to debug logfile.
        # Read number of grid cells (horizontally and vertically) and write it to debug logfile.
        # Read nodatavalue (probably -9999) and write it to debug logfile.
        # Read the modelcompanies, write number of cells per company to debug logfile
        # Compare ModelBedrijven.idf with Zone1 file in preprocessor input location (or will we use the same file for both?). The same cells should have nodatavalue. Otherwise, raise InputDataError (write to log) and stop. Also cells size and number of cells should be the same, see next point.
        # The cell size should be the same in all grids and all grids should have the same number of cells. When reading all other grids: Check cell size and numbers, if not the same raise InputDataError and stop.
        """
        self.companies_idf = idfopen(self.companies_fn, pattern="{name}")
        dx, xmin, xmax, dy, ymin, ymax = spatial_reference(self.companies_idf)
        self.dx, self.dy = dx, -dy
        self.nrow, self.ncol = self.companies_idf.shape
        logging.debug("Cellsize x {} y {}".format(self.dx, self.dy))
        logging.debug("Rows {} columns {}".format(*self.companies_idf.shape))
        # logging.debug("Nodata {}".format(self.companies_idf.nodata()))

        # Get number of cells per company
        return unique(self.companies_idf, return_counts=True)

        """
        # slice for each model company
        self.companies_slices = {
            company: self.companies_idf == company for company in self.companies}
        """

    # REZOWA-8
    # REZOWA-9
    def _read_companies_sub(self, fn):
        """Read grid ScenarioX\\x.idf with x
        Integers that represent a subtype like modelcompany type or subarea.
        Check if all cells that belong to a modelcompany have the same subtype.
        """
        arr = idfopen(fn, pattern="{name}")
        match = ~np.any(
            np.isnan(arr.load().values)[~np.isnan(self.companies_idf.load().values)]
        )
        if not match:
            error = f"Not all model companies match {fn}."
            logging.error(error)
            raise InputDataError(error)
        return arr

    # SERIES OUTPUT FUNCTIONS
    def _series(self, data, name):
        return pd.Series(index=self.companies, data=data, name=name)

    def companies_series(self):
        return self._series(data=self.company_cells, name="Cells")

    def sub_series(self, subarray, name="aantal_sub"):

        grp = subarray.load().groupby(self.companies_idf)
        var = grp.var()
        if np.any(var > 0):
            for comp, subs in grp:
                if np.var(subs.values) > 0:
                    error = "Model company {} has {} subs.".format(
                        comp, len(np.unique(subs.values))
                    )
                    # logging.error(error)
                    raise InputDataError(error)

        return self._series(grp.first(), name=name)

    def companies_df(self):
        df = pd.DataFrame(
            index=self.companies, columns=["subarea", "companytype", "company", "cells"]
        )
        df["cells"] = self.companies_series()
        df["company"] = df.index
        df.loc[
            :, "companytype"
        ] = self.companytypes  # _idf.load().groupby(self.companies_idf).first()
        df.loc[
            :, "subarea"
        ] = self.subareas  # _idf.load().groupby(self.companies_idf).first()
        # for (comp1, ct), (comp2,sa) in zip(self.companytypes_idf.groupby(self.companies_idf),
        #                  self.subareas_idf.groupby(self.companies_idf)):
        #    df.loc[comp1,'companytype'] = ct.values[0]
        #    df.loc[comp2,'subarea'] = sa.values[0]

        return df


#
#     def slice_companies(self):
#         slices = {}
#
#         # Check if each modelbedrijf is only part of one deelgebied
#         for modelbedrijf in np.unique(self.companies_idf.arr.compressed()):
#         # slice for each modelbedrijf
#             slices[modelbedrijf] = self.companies_idf.arr == modelbedrijf
#         return slices
