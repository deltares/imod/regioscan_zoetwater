from os.path import join, exists
import logging
from regioscan_zoetwater.exceptions import InputDataError


def set_paths(ini):
    pths = {}

    scenario_name = ini["scenario"]

    pths["hydroinput"] = ini["pth_hydroinput"]
    pths["calcdir"] = ini["pth_calculation"]
    # REZOWA-164
    pths["database"] = ini["pth_database"]

    pths["scenario_input"] = join(pths["hydroinput"], scenario_name)
    pths["scenario_output"] = join(pths["calcdir"], scenario_name)
    pths["scenario_output_maps"] = join(pths["scenario_output"], "maps")
    pths["referencefolder"] = join(pths["scenario_output"], r"reference\")
    pths["irrigation_folder"] = join(pths["scenario_input"], r"MetBeregening\")
    pths["agricom_outA"] = r"Agricom_Out\Module_A\"
    pths["agricom_outC"] = r"Agricom_Out\Module_C\"
    pths["ref_outA_folder"] = join(pths["referencefolder"], pths["agricom_outA"])

    pths["ref_outC_folder"] = join(pths["referencefolder"], pths["agricom_outC"])

    pths["subarea_fn"] = join(pths["hydroinput"], "deelgebieden.idf")
    pths["tred_zb_folder"] = join(pths["scenario_input"], "ZonderBeregening")
    pths["fwoofolder"] = join(pths["hydroinput"], "Kansrijkheidsgrids")
    pths["tred_buffer_folder"] = join(pths["scenario_input"], "Buffer")

    pths["soil_fn"] = join(pths["scenario_input"], "soilreclass.idf")
    pths["glg_fn"] = join(pths["scenario_input"], "glgreclass.idf")
    pths["gvg_fn"] = join(pths["scenario_input"], "gvg.idf")  # not reclassed, m-mv
    pths["crop_fn"] = join(pths["scenario_input"], "cropreclass.idf")
    pths["drain_fn"] = join(pths["scenario_input"], "buisdrainage.idf")
    pths["agrcrops_fn"] = join(pths["scenario_input"], "landgebruik.idf")
    pths["comp_fn"] = join(pths["scenario_input"], "modelbedrijven.idf")

    # REZOWA-133
    for fn in [
        "subarea_fn",
        "comp_fn",
        "soil_fn",
        "glg_fn",
        "gvg_fn",
        "crop_fn",
        "drain_fn",
        "agrcrops_fn",
    ]:
        if not exists(pths[fn]):
            error = "Input map {} does not exist at location {}".format(
                fn[:-3], pths[fn]
            )
            logging.error(error)
            exit()

    return pths
