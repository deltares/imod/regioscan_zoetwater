#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# File for measures
# taken from Access DB

import logging
from os.path import join, exists
from os import scandir, makedirs
from shutil import copyfile
from collections import OrderedDict
import re
from pandas import DataFrame, Series
import numpy as np
import xarray as xr
import pickle
import warnings
from regioscan_zoetwater.utils import (
    idfopen,
    idfwrite,
    spatial_reference,
    csv_from_dict,
    sum_dicts,
    MeasuresDB,
    read_act,
    read_data_per_modelcompany_mm2m3_over_years,
    reduce_timeseries_per_company_to_orddict,
    reduce_timeseries_per_company_to_df,
    unique,
    idf_from_company_value,
    apply_company,
    df_to_orddict,
    idf_from_company_value,
    get_timeseries,
    interpolate_on_valuearr,
)
from regioscan_zoetwater.agricom import run_measure_agricom, read_module_a
from regioscan_zoetwater.constants import *


def mm_to_m3(arr):
    dx, xmin, xmax, dy, ymin, ymax = spatial_reference(arr)
    return (arr * dx * -dy) / 1000.0


"""
OPPERVLAKTEWATER = 'Oppervlaktewater'
GRONDWATER = 'Grondwater'
OPSLAG = 'Opslag' 

def check_rank_water_usage(data, combinations):
    for i in range(1,len(combinations)+1):
        keep_usage_ranking = False
        for measure in combinations[i]:
            if measure['Description'] == OPSLAG and (  measure['SourceType.Description'] == GRONDWATER or measure['SourceType.Description'] == OPPERVLAKTEWATER):
                keep_usage_ranking = True
        if not keep_usage_ranking:
            for j in range(1,len(data[i])+1):
                data[i][j]['WaterUsage'] = 0
                
    return data
"""


def create_highvalue_mask(pths, hi, highvalue_crops=[7, 8, 9, 10]):
    """Create mask that is 1 for highvalue crops and 0 for lowvalue crops on the same company.
    If a company has no highvalue crops, than entire company is 1
    Crop codes:
    1 - gras
    2 - mais
    3 - aardappelen
    4 - (suiker)bieten
    5 - granen
    6 - overig
    7 - boomteelt
    8 - glastuinbouw
    9 - fruit
    10 - bollen"""
    agrlgn = idfopen(pths["agrcrops_fn"], pattern="{name}").load()
    highvalue_mask = xr.ones_like(agrlgn).load()
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        b = False
        for c in highvalue_crops:
            b = b | (agrlgn.values == float(c))
        highvalue_mask.values[~b] = 0
        # REZOWA-148: do this only for companies that have high-value crops
        has_hv = (
            highvalue_mask.groupby(hi.companies_idf).max().to_series()
        )  # company has no highvalue crops: 0
        has_hv = idf_from_company_value(has_hv == 0, hi.companies_idf).astype(bool)
        highvalue_mask.values[
            has_hv.values
        ] = 1.0  # set high_value mask to 1 if company has no high-value crops
        highvalue_mask.values[np.isnan(agrlgn.values)] = np.nan
    return highvalue_mask


def calculate_dimensions(tred_arr, perc_arr, hi, mask_highvalue=None):
    """REZOWA-24"""
    m3 = (mm_to_m3(tred_arr) * 100.0 / perc_arr).load()

    # REZOWA-126:  ability to only dimension for high-value crops
    if mask_highvalue is not None:
        m3 *= mask_highvalue

    m3.name = "dimensions"
    return reduce_timeseries_per_company_to_orddict(m3, hi, func=np.sum)


def get_effect_arr(measure, pths, measuresdb, hi):
    """Get effectvalue arr from different routes: direct value, lookup, map
    Return either value, or arr with value"""
    if measure[EFFECT_VALUETYPE_] == VALUE_DIRECT:
        # make map of direct value
        # return xr.ones_like(hi.companies_idf)*measure[EFFECT_VALUE_]
        return measure[EFFECT_VALUE_]

    elif measure[EFFECT_VALUETYPE_] == VALUE_LOOKUP:
        # Lookup
        return calc_drought_by_table(measure[MEID_], pths, hi, measuresdb)

    elif measure[EFFECT_VALUETYPE_] == VALUE_MAP:
        # EffectValueMap
        return idfopen(
            join(pths["scenario_input"], measure[EFFECT_VALUEMAP_]), pattern="{name}"
        )


def get_company_source(combination, efficiency, pths, hi):
    """Get source per company from different routes: direct value, or map
    Return dict of company : sourcetype, efficiency"""

    # if MeasureCombination of type BUFFSPRI or BUFFAPPL, then Source is Source of buffer-measure,
    # otherwise: source is source of application measure, or other measure
    for measure in combination:
        if (
            measure["MeasureCombinationType"] in [MCT_BUFFSPRI, MCT_BUFFAPPL]
            and measure["Mechanism"] == MECH_BUFF
        ):
            break
        elif measure["Mechanism"] == MECH_APPL:
            break

    # REZOWA-26
    """# Calculation water usage Qapp
    # only 1 source type per measure
    sources = {measure[MECHANISM_]: measure['SourceType.ID'] for measure in combination}
    # lowest id is either 1, Opslag (and then use its source), or 2: application
    sources = {sources[min(sources.keys())]: efficiency}
    """
    if measure[SOURCE_VALUETYPE_] == VALUE_DIRECT:
        # make map of direct value
        # return xr.ones_like(hi.companies_idf)*measure[EFFECT_VALUE_]
        return {k: {measure[SOURCE_VALUE_]: efficiency} for k in hi.companies}

    elif measure[SOURCE_VALUETYPE_] == VALUE_LOOKUP:
        # Lookup: not implemented
        raise NotImplementedError("Lookup table is not implemented for SourceTypes")

    elif measure[SOURCE_VALUETYPE_] == VALUE_MAP:
        # EffectValueMap
        da = idfopen(
            join(pths["scenario_input"], measure[SOURCE_VALUEMAP_]), pattern="{name}"
        )
        # return reduce_timeseries_per_company_to_orddict(da,hi,first='companies',func=Series.mode)  # return the majority value for a company
        df = reduce_timeseries_per_company_to_df(da, hi, func=np.mean)
        df = df.apply(
            np.rint
        )  # round to nearest integer --> this assumes only grids with 1 and 2... Series.mode better, but requires latest pandas
        return {
            k: {v: efficiency}
            for k, v in df_to_orddict(df, hi, first="companies").items()
        }


def sum_water_usage(q_app_kolom):
    # REZOWA-47
    waterusage = {}
    for mc, numbers in q_app_kolom.items():
        measure = next(iter(numbers))
        sum_q = 0
        for source_type in numbers:
            if source_type == 1 or source_type == 2:
                sum_q += sum(c for c in numbers[source_type].values())
        waterusage[int(mc)] = {"WaterUsage": sum_q}

    return waterusage


def copy_drought_damage(
    inputfolder,
    outputfolder,
    set_to_zero,
    ini,
    str_start_with="droogteschade",
    str_end_with="_090.idf",
    mask=None,
    origfolder=None,
):
    """REZOWA-28"""
    yeardict = {}
    agricom_folder = join(outputfolder, "Agricom_In")
    makedirs(agricom_folder, exist_ok=True)
    for entry in scandir(inputfolder):
        if (
            entry.name.lower().endswith(str_end_with)
            and entry.name.lower().startswith(str_start_with)
            and entry.is_file()
        ):

            filename = join(
                agricom_folder, entry.name.replace(str_end_with, "1231.idf")
            )
            # REZOWA-134
            year = int(filename[-12:-8])
            if year >= ini["startyear"] and year <= ini["endyear"]:
                if set_to_zero:
                    set_zero_drought_damage(0, filename, entry.path)
                else:
                    # REZOWA-126: only high-value crops
                    if mask is None:
                        copyfile(entry.path, filename)
                    else:
                        origfile = join(
                            origfolder, entry.name.replace(str_end_with, "1231.idf")
                        )
                        set_mask_drought_damage(mask, filename, entry.path, origfile)
                yeardict[filename.lower().split("_")[-1]] = filename

    return yeardict


def calc_drought_damage_from_value_arr(
    inputfolder,
    outputfolder,
    value_arr,
    years,
    str_start_with="droogteschade",
    str_end_with="??mm.idf",
):
    """REZOWA-125
    Use an index arr to calculate drought damage, interpolate when necessary"""
    yeardict = {}
    agricom_folder = join(outputfolder, "Agricom_In")
    makedirs(agricom_folder, exist_ok=True)

    value_arr = value_arr.load()
    # REZOWA-170
    ini = {"startyear": years[0], "endyear": years[-1]}  # fake ini from years
    da = get_timeseries(
        inputfolder,
        pattern=f"{str_start_with}_*{str_end_with}",
        ini=ini,
        idfopen_pattern="{name}_{time}_{valuearr}",
    )  # soil buffer specific Tred
    # make coord valuearr float
    p = re.compile("([.0-9]*)")  # strip mm's from coords
    da.coords["valuearr"] = [float(p.match(v)[1]) for v in da.coords["valuearr"].values]

    # get values from da based on value_arr, interpolate when necessary
    da = interpolate_on_valuearr(da, value_arr, dim="valuearr", extrapolate=True)
    da = da.where((da >= 0) | da.isnull(), other=0)  # droughtdamage = [0 - 1]
    da = da.where((da <= 1) | da.isnull(), other=1.0)  # droughtdamage = [0 - 1]
    for year in years:
        # save
        filename = join(agricom_folder, "{}_{}1231.idf".format(str_start_with, year))
        idfwrite(filename, da.sel(time=str(year)))
        yeardict[filename.lower().split("_")[-1]] = filename

    return yeardict


def set_zero_drought_damage(zero_drought, filename, original_path):
    out = idfopen(original_path, pattern="{name}")
    if isinstance(zero_drought, (int, float)):
        out *= zero_drought

    idfwrite(filename, out)
    return out


def set_mask_drought_damage(mask, filename, filenotmasked, filemasked):
    out = idfopen(filenotmasked, pattern="{name}").load()
    masked = idfopen(filemasked, pattern="{name}").load()
    try:
        out.values[mask == 0] = masked.values[mask == 0]
    except IndexError:
        print(mask)
        print(type(mask))
        raise
    idfwrite(filename, out)
    return out


def calc_saltdamage_algorithm(arr, saltperc):
    return arr * (1 - saltperc / 100.0)


def calc_salt_damage(inputfolder, saltperc, outputfolder, ini):
    """REZOWA-30"""
    yeardict = {}
    for entry in scandir(join(inputfolder, "ZonderBeregening")):
        if (
            entry.name.lower().endswith(".idf")
            and entry.name.lower().startswith("zoutschade")
            and entry.is_file()
        ):
            filename = join(outputfolder, "Agricom_In", entry.name)

            # REZOWA-134
            year = int(filename[-12:-8])
            if year >= ini["startyear"] and year <= ini["endyear"]:
                arr = idfopen(entry.path, pattern="{name}")
                arr = calc_saltdamage_algorithm(arr, saltperc)
                idfwrite(filename, arr)

                yeardict[entry.name.lower().split("_")[-1]] = filename

    return yeardict


def calc_total_damage(idf_zout, idf_droogte, outputfolder):
    """REZOWA-33"""
    for date, zout in idf_zout.items():
        zout = idfopen(zout, pattern="{name}")
        droogte = idfopen(idf_droogte[date], pattern="{name}")

        total = droogte + zout

        filename = join(outputfolder, "Agricom_In", "totschadefractie_{}".format(date))
        idfwrite(filename, total)


def calc_cost_benefit(
    act_meas,
    act_ref,
    benefit_mdb,
    irr_maint,
    investmentcosts,
    maintenancecost,
    lss,
    discountrate,
):
    """REZOWA-38
    Function is executed per model company"""
    years = sorted(act_meas.keys())
    T = len(years)
    benefits = 0
    crop_benefits = 0
    sprinkl_benefits = 0
    costs = 0
    inv_cost = 0
    for i, year in enumerate(years):
        factor = (1 + discountrate) ** (T - (i + 1))
        benefits += (
            act_meas[year] - act_ref[year] + benefit_mdb + irr_maint[year]
        ) * factor
        crop_benefits += (act_meas[year] - act_ref[year]) * factor
        sprinkl_benefits += irr_maint[year]

        # Different measures have different lifespans
        for mid, investmentcost in investmentcosts.items():
            ls = lss[mid]
            inv_cost += (
                investmentcost * min(ls, T - i) / ls if (ls + i) % ls == 0 else 0
            )
        costs += (inv_cost + maintenancecost[year]) * factor
        inv_cost = 0
    if costs:
        nbc = (benefits - costs) / costs
    else:
        nbc = np.nan
    return costs, benefits, nbc, crop_benefits, sprinkl_benefits


def calc_fwoo_per_company(fwoo_fn, hi, mask=None):
    # REZOWA-19 calc_fwoo_per_company
    fwoo_idf = read_fwoo_grid(fwoo_fn)
    # REZOWA-126:
    if mask is not None:
        fwoo_idf *= mask
    return reduce_timeseries_per_company_to_orddict(fwoo_idf, hi, first="companies")


def read_fwoo_grid(fwoo_fn):
    # FWOO grids are of the size of the Netherlands
    # NOT: Clip the part that equals the case area
    # Assume exact dimensions of case area
    return idfopen(fwoo_fn, pattern="{name}")


def calc_fwoo_per_combination(combination, hi, pths, highvalue_mask):
    """Assess fwoo for measures in combination, return ordered dict"""
    # Determine if measure combination is successful
    fwoo_measure = {}
    for measure in combination:
        # Read FWOO grid name from database per measure in mc
        fwoo_db = measure["FWOOgrid"]
        # REZOWA-157: for buffer and regular sprinkling: skip fwoo for sprinkling
        if fwoo_db > "" and not (
            measure["MeasureCombinationType"] == MCT_BUFFSPRI
            and measure["Mechanism"] == MECH_APPL
        ):
            # Read FWOO grid and determine FWOO per company
            fwoo_fn = join(pths["fwoofolder"], fwoo_db)
            fwoo_measure[measure[MEID_]] = calc_fwoo_per_company(
                fwoo_fn, hi, mask=highvalue_mask
            )
        else:
            # When no grid is supplied mc has success for all companies
            fwoo_col = {}
            for company in hi.companies:
                fwoo_col[int(company)] = 1
            fwoo_measure[measure[MEID_]] = OrderedDict(
                sorted(fwoo_col.items(), key=lambda t: int(t[0]))
            )
    return fwoo_measure


def calc_overall_fwoo(fwoo_measure, hi):
    # When all measures from combination are successful, than the mc is also successful
    fwoo_col = {}
    for company in hi.companies:
        fwoo_col[company] = 1
        for measure in fwoo_measure:
            if (
                fwoo_measure[measure][company] == 0
            ):  # all values larger than 0 are set to one
                fwoo_col[company] = 0
    return fwoo_col


def calculate_q_app(tred, dim, sources):
    """REZOWA-26
    Calculate how much water is drawn for each sourcetype per year per model company.
    tred: summed m3 necessary water per company
    dim: amount of stored / applied water
    sources: water source and efficiency"""
    q_app = {}
    for modelbedrijf, years in tred.items():
        q_app[int(modelbedrijf)] = {}
        source = sources[modelbedrijf]
        for sourcetype, efficiency in source.items():
            q_app[modelbedrijf][sourcetype] = {}
            for (
                year,
                summed,
            ) in (
                years.items()
            ):  # JOOST: separate treatment mc 5 no longer necessary: set dimension earlier on max use
                eff = efficiency / 100.0
                q = min(summed, dim[modelbedrijf] * eff) / eff
                q_app[modelbedrijf][sourcetype][year] = q
    return q_app


def calc_costs(mtype, mvalue, hi, dimensions, mask=None):
    """REZOWA-35, REZOWA-37"""
    csv = {}

    if mtype == DIMENSION_VOLUME:  # Watervolume
        csv = {k: v * mvalue for k, v in dimensions.items()}
    elif mtype == DIMENSION_AREA:  # Areaal
        # REZOWA-142
        if mask is not None:
            # Get number of cells per company, can't use the version in hi, b/o mask
            compmask = hi.companies_idf.copy(deep=True).load()
            compmask.values[mask.values == 0] = np.nan
            companies, company_cells = unique(compmask, return_counts=True)
            company_cells = Series(index=companies, data=company_cells, name="Cells")
            company_cells = company_cells.reindex(hi.companies).fillna(0)
            csv = company_cells * hi.dx * hi.dy / 10000.0 * mvalue
        else:
            csv = hi.companies_series() * hi.dx * hi.dy / 10000.0 * mvalue
        csv = csv.to_dict()
    else:
        logging.error("Encountered unknown dimensiontype")

    return csv


# REZOWA-82


def isnumber(var):
    try:
        float(var)
        return True
    except ValueError:
        return False


def interpolate(idfmin, idfmax, pctmin, pctinter, pctmax):
    return (pctinter - pctmin) / float(pctmax - pctmin) * (idfmax - idfmin) + idfmin


##Load for 0 - 100% tables
def load_drought_damage_files(folder, year):

    drought_damage_perc = {}
    for perc in range(101):

        drought_min_fn = join(
            folder, "droogteschade_" + year + "_" + "{:03d}".format(perc) + ".idf"
        )
        if exists(drought_min_fn):
            drought_damage = idfopen(
                drought_min_fn, pattern="{name}"
            )  # imod is getting pickier with decompose
            drought_damage_perc[perc] = drought_damage

    return drought_damage_perc


# REZOWA-82 #REZOWA-83
def calculate_write_droughtdamages(
    percentage_arr, scenario_input, agricom_in_path, ini
):
    """Adjust annual droughtdamage by fixed percentage"""
    drought_folder = join(scenario_input, "Evenredig")
    idfwrite(join(agricom_in_path, "droogteschade_pct.idf"), percentage_arr)
    idf_droogte = {}

    da = get_timeseries(
        drought_folder,
        pattern=f"droogteschade_????_???.idf",
        ini=ini,
        idfopen_pattern="{name}_{time}_{perc}",
    )

    da = interpolate_on_valuearr(
        da, percentage_arr, "perc", extrapolate=True
    )  # extrapolate True: REZOWA-171
    da = da.where((da >= 0) | da.isnull(), other=0)  # droughtdamage = [0 - 1]
    da = da.where((da <= 1) | da.isnull(), other=1.0)  # droughtdamage = [0 - 1]

    for year in range(ini["startyear"], ini["endyear"] + 1):
        # save
        tag = str(year) + "1231.idf"
        drought_idf_fn = join(agricom_in_path, "droogteschade_" + tag)
        idfwrite(drought_idf_fn, da.sel(time=str(year)))
        idf_droogte[tag] = drought_idf_fn
    return idf_droogte


# REZOWA-82
def calc_drought_by_table(measure, pths, hi, measuresdb, gw_type=None):
    """create a map of drought effect from table lookup"""

    soil_type = idfopen(pths["soil_fn"], pattern="{name}").load()
    if gw_type is None:
        gw_type = idfopen(pths["glg_fn"], pattern="{name}").load()
    crop_type = idfopen(pths["crop_fn"], pattern="{name}").load()
    drain_type = idfopen(pths["drain_fn"], pattern="{name}").load()
    comp_nr = hi.companies_idf
    # accesdb = MeasuresDB()
    # get records from database for current measure
    df = measuresdb.get_table_entry(
        "DroughtLookupQuery", "WHERE Measure={}".format(measure)
    )

    # loop over df, and create a percentage map by selecting cells where gwk,soil and crop are true
    percentage_arr = xr.zeros_like(comp_nr).load()
    percentage_arr.values[:] = np.nan
    for i, row in df.iterrows():
        soil = row["SoilType"]
        gw = row["GroundwaterType"]
        crop = row["CropType"]
        drain = row["DrainType"]
        percentage_arr.values[
            (soil_type.values == soil)
            & (gw_type.values == gw)
            & (crop_type.values == crop)
            & (drain_type.values == drain)
        ] = row["EffectValue"]

    return percentage_arr
    # calculate_write_droughtdamages(percentage_arr, scenario_input, agricom_in_path, ini)


# REZOWA-127
# zoek op GVG kaart GVG op
# zoek op kaart GVG verhoging GVG verhoging op voor maatregel --> route maatregeleffect per kaart
# zoek GVG en verhoogde GVG op in meta-tabellen Ruud, per gewas, per bodem --> route maatregeleffect uit tabel
# bepaal ‘evenredige’ verlaging transpiratiereductie als (Tred_GVG – Tred_GVGmaatregel) / Tred_GVG ==> check wat percentage precies is: verlaging, of overblijvend (1- of niet...)
def calculate_pctarr_ditch(measure, pths, measuresdb, hi):
    """ calculate percentage map for ditch measures"""

    # get records from database for current measure
    df = measuresdb.get_table_entry(
        "GroundwaterType", ""
    )  # waarden in database moeten aaneensluitend zijn!

    gvg = idfopen(pths["gvg_fn"], pattern="{name}").load()
    gvgincr = (
        gvg - get_effect_arr(measure, pths, measuresdb, hi) / 100.0
    )  # map of gvg increase in cm, gvg in m-surfacelevel
    gvgincr = gvgincr.fillna(0)
    idfwrite(join(pths["mccalc"], "gvgincr.idf"), gvgincr)

    # Reclass GVG and GVG_increase
    gvgincrrc = xr.zeros_like(gvgincr).load()
    gvgincrrc.values[:] = np.nan
    gvgrc = xr.zeros_like(gvg).load()
    gvgrc.values[:] = np.nan
    with np.testing.suppress_warnings() as sup:
        sup.filter(RuntimeWarning)
        for i, row in df.iterrows():
            if "GVG" in row[1]:
                v1, v2 = float(row[2]), float(row[3])
                gvgincrrc.values[(gvgincr > v2) & (gvgincr <= v1)] = row[0]
                gvgrc.values[(gvg > v2) & (gvg <= v1)] = row[0]

    # TODO: evt: interpolate between Tred items (but they are close enough)
    tred_gvg = calc_drought_by_table(
        measure[MEID_], pths, hi, measuresdb, gw_type=gvgrc
    )
    tred_gvgincr = calc_drought_by_table(
        measure[MEID_], pths, hi, measuresdb, gw_type=gvgincrrc
    )
    idfwrite(join(pths["mccalc"], "gvgrc.idf"), gvgrc)
    idfwrite(join(pths["mccalc"], "gvgincrrc.idf"), gvgincrrc)
    idfwrite(join(pths["mccalc"], "tred_gvg.idf"), tred_gvg)
    idfwrite(join(pths["mccalc"], "tred_gvgincr.idf"), tred_gvgincr)

    percentage_arr = (1.0 - tred_gvgincr / tred_gvg) * 100.0
    percentage_arr.values[tred_gvgincr == 0.0] = 0.0
    percentage_arr.values[np.isinf(percentage_arr)] = 0.0
    return percentage_arr


def calc_costs_by_table(
    measure,
    measuresdb,
    mtype,
    dimension_col,
    pths,
    hi,
    ctype="Investment",
    highvalue_mask=None,
):
    if ctype == "Investment":
        table = "InvestmentCostLookup"
    elif ctype == "Maintenance":
        table = "MaintenanceCostLookup"

    df = measuresdb.get_table_entry(table, "WHERE Measure={}".format(measure[MEID_]))

    cost_arr = xr.zeros_like(hi.companies_idf).load()
    cost_arr.values[:] = np.nan
    drain_type = idfopen(pths["drain_fn"], pattern="{name}")
    for i, row in df.iterrows():
        drain = row["DrainType"]
        cost_arr.values[(drain_type.values == drain)] = row["Cost"]
    if highvalue_mask is not None:
        cost_arr *= highvalue_mask
    idfwrite(
        join(pths["mccalc"], f"{ctype.lower()}_{measure[MEID_]}_costarr.idf"), cost_arr
    )

    # depending on type:
    if mtype == DIMENSION_VOLUME:  # Watervolume
        cost = apply_company(cost_arr, hi, name=ctype, func=np.mean)  # average cost
        return {comp: dim * cost[comp] for comp, dim in dimension_col.items()}
    elif mtype == DIMENSION_AREA:  # Areaal
        # sum over companies, multiply by cell area (ha)
        cost = apply_company(cost_arr, hi, name=ctype) * hi.dx * hi.dy / 10000.0
        return cost.to_dict()


def calculate_measure_costs(
    mcid, cost_combination, dimension_col, highvalue_mask, pths, measuresdb, hi, ini
):
    idf_cost_folder = join(pths["mccalc"], pths["agricom_outA"])
    maintenancecost = {}
    investmentcost = {}
    lifespans = {}
    mcost = {}
    icost = {}
    for measure in cost_combination:
        if measure["CalculationType"] != CALCT_SPRINKL:  # niet reguliere beregening
            mtype = measure["DimensionType"]
            if measure["MValueType"] == VALUE_LOOKUP:
                mcost[measure[MEID_]] = calc_costs_by_table(
                    measure,
                    measuresdb,
                    mtype,
                    dimension_col,
                    pths,
                    hi,
                    "Maintenance",
                    highvalue_mask,
                )
            elif measure["MValueType"] == VALUE_DIRECT:
                mvalue = measure["MCost"]
                mcost[measure[MEID_]] = calc_costs(
                    mtype, mvalue, hi, dimension_col, highvalue_mask
                )
            for c in mcost[measure[MEID_]]:
                mcost[measure[MEID_]][c] = {
                    y: mcost[measure[MEID_]][c]
                    for y in range(ini["startyear"], ini["endyear"] + 1)
                }

            if measure["IValueType"] == VALUE_LOOKUP:
                icost[measure[MEID_]] = calc_costs_by_table(
                    measure,
                    measuresdb,
                    mtype,
                    dimension_col,
                    pths,
                    hi,
                    "Investment",
                    highvalue_mask,
                )
            elif measure["IValueType"] == VALUE_DIRECT:
                mvalue = measure["ICost"]
                icost[measure[MEID_]] = calc_costs(
                    mtype, mvalue, hi, dimension_col, highvalue_mask
                )
        else:
            icost[measure[MEID_]], mcost[measure[MEID_]] = read_module_a(
                idf_cost_folder, hi, ini
            )
            icost[measure[MEID_]] = df_to_orddict(
                icost[measure[MEID_]], hi, first="companies"
            )
            mcost[measure[MEID_]] = df_to_orddict(
                mcost[measure[MEID_]], hi, first="companies"
            )
            tfact = measure["LifeSpan"] / float(ini["endyear"] - ini["startyear"] + 1)
            icost[measure[MEID_]] = {
                c: sum(icost[measure[MEID_]][c].values()) * tfact
                for c in icost[measure[MEID_]].keys()
            }

            # avg p year
        lifespans[measure[MEID_]] = measure["LifeSpan"]

        for c in mcost[measure[MEID_]].keys():
            if c not in maintenancecost:
                maintenancecost[c] = {}
                investmentcost[c] = {}
            for y in range(ini["startyear"], ini["endyear"] + 1):
                if y not in maintenancecost[c]:
                    maintenancecost[c][y] = 0.0
                maintenancecost[c][y] += mcost[measure[MEID_]][c][y]
            investmentcost[c][measure[MEID_]] = icost[measure[MEID_]][c]

    # Costs csvs for checking
    csv_fn = join(pths["mccalc"], "companyresult_mc_{}_icost.csv".format(mcid))
    csv_from_dict(csv_fn, icost)
    csv_fn = join(pths["mccalc"], "companyresult_mc_{}_mcost.csv".format(mcid))
    csv_from_dict(csv_fn, maintenancecost)

    return maintenancecost, investmentcost, lifespans


def calculate_water_stores(
    combination, dimension_col, efficiency, recovery_perc, application_eff, hi
):
    """calculate water taken from all different sources, both in and out
    stores: rain, storage_in, storage_out, irrigation_in, irrigation_out, soilmoisture_in, transpiration_out, groundwater_out, surfacewater_out"""
    """MECH_BUFF = 1
        MECH_APPL = 2
        MECH_DRAIN = 3
        MECH_SLBUFF = 4
        MECH_DITCH = 5
        """
    # REZOWA-125 - quick fix for now
    if isinstance(efficiency, (np.ndarray)):
        efficiency = efficiency.arr.mean()
    if isinstance(application_eff, (np.ndarray)):
        application_eff = application_eff.arr.mean()

    df = DataFrame(
        index=hi.companies,
        data=0.0,
        columns=[
            "Rain",
            "Groundwater",
            "Surfacewater",
            "Buff_In",
            "Buff_Out",
            "Appl_In",
            "Appl_Out",
            "Soilmoist_In",
            "Transpiration",
        ],
    )
    dim = Series(dimension_col)
    buffer = False
    for measure in combination:
        if measure["MechanismType"] == [
            MECH_BUFF
        ]:  # buffer always ahead of application
            df.loc[:, "Rain"] = dim
            df.loc[:, "Buffer_In"] = dim
            df.loc[:, "Buffer_Out"] = dim / recovery_perc
            dim /= recovery_perc
            buffer = True
        # elif measure['MechanismType'] == [MECH_DRAIN]:
        # tred kaarten uitlezen?
        # elif measure['MechanismType'] == [MECH_DITCH]:
        # elif measure['MechanismType'] == [MECH_SLBUFF]:

        elif measure["MechanismType"] == [MECH_APPL]:
            if not buffer:
                # gw or sw?  TODO!
                # Get source from map
                df.loc[:, "Surfacewater"] = dim
                df.loc[:, "Groundwater"] = dim

                # if regular sprinkling
                # Write irrigation files...

            df.loc[:, "Appl_In"] = dim
            df.loc[:, "Appl_Out"] = dim / application_eff
            df.loc[:, "SoilMoist_In"] = dim / application_eff
            dim /= application_eff


# REZOWA-94
def write_irr_files(sources, agricom_path, q_app, hi, mask=None):
    """write irrigation files from Qapp col and sources per company (dict of comp:source:eff)"""
    # strip efficiency from sources
    sources = {k: list(v.keys())[0] for k, v in sources.items()}

    # check if all companies are in dict
    sources = Series(sources)
    sources = sources.reindex(hi.companies).fillna(0)
    # if source is not sw or gw, set to sw (buffer measure)
    sources[sources > 2] = 1
    sources = sources.astype(float)
    sources = sources.to_dict()

    comp_nr = hi.companies_idf
    beregeningstype = idf_from_company_value(sources, comp_nr, mask).fillna(0)

    if mask is None:
        m3_to_mm = (
            1000.0 / hi.dx / hi.dy / hi.companies_series()
        )  # conversion per company
    # REZOWA-126
    else:
        # Get number of cells per company, can't use the version in hi, b/o mask
        compmask = comp_nr.copy(deep=True)
        compmask.values[mask.values == 0] = np.nan
        companies, company_cells = unique(compmask, return_counts=True)
        company_cells = Series(index=companies, data=company_cells, name="Cells")
        company_cells = company_cells.reindex(hi.companies).fillna(0)
        m3_to_mm = 1000.0 / hi.dx / hi.dy / company_cells  # conversion per company

    beregeningstype_fn = join(agricom_path, "beregeningstype.idf")
    idfwrite(beregeningstype_fn, beregeningstype)

    # strip source from qapp, and flip
    q_app = {k: v[list(v.keys())[0]] for k, v in q_app.items()}
    q_appflip = {k: {} for k, v in q_app[list(q_app.keys())[0]].items()}
    for c, v in q_app.items():
        for k, v in v.items():
            q_appflip[k][c] = v

    sources = Series(sources)
    for time in q_appflip.keys():
        qappcomp = Series(q_appflip[time], name="Qapp") * m3_to_mm

        for ST, stype in zip([ST_SURFW, ST_GROUNDW], ["sw", "gw"]):
            qapp = qappcomp.copy()
            qapp.loc[sources != ST] = 0
            qapp = qapp.to_dict()

            beregening_fn = join(agricom_path, f"beregening_{stype}_{time}1231.idf")
            beregening = idf_from_company_value(qapp, hi.companies_idf, mask).fillna(0)
            idfwrite(beregening_fn, beregening)
    return None


def check_validity_measures(combinations):
    """
    # MeasureCombinationTypes
    MCT_BUFFAPPL = 1
    MCT_APPL = 2
    MCT_DRAIN = 3
    MCT_BUFFSPRI = 4
    MCT_SPRI = 5
    MCT_D2B = 6
    MCT_DRAINAPPL = 7
    MCT_DRAINSPRI = 8
    MCT_SLBUFF = 9
    MCT_SLBUFFAPPL = 10
    MCT_SLBUFFSPRI = 11
    """

    mechanisms = {
        MCT_BUFFAPPL: [
            MECH_BUFF,
            MECH_BUFF,
            MECH_APPL,
        ],  # What mechanisms should be in (in what order) in combination
        MCT_APPL: [MECH_APPL],
        MCT_DRAIN: [MECH_DRAIN, MECH_DRAIN],
        MCT_BUFFSPRI: [MECH_BUFF, MECH_BUFF, MECH_APPL],
        MCT_SPRI: [MECH_APPL],
        # MCT_D2B:[MECH_DRAIN],
        MCT_DRAINAPPL: [MECH_DRAIN, MECH_DRAIN, MECH_APPL],
        MCT_DRAINSPRI: [MECH_DRAIN, MECH_DRAIN, MECH_APPL],
        MCT_SLBUFF: [MECH_SLBUFF, MECH_SLBUFF],
        MCT_SLBUFFAPPL: [MECH_SLBUFF, MECH_SLBUFF, MECH_APPL],
        MCT_SLBUFFSPRI: [MECH_SLBUFF, MECH_SLBUFF, MECH_APPL],
        MCT_DITCH: [MECH_DITCH, MECH_DITCH],
        MCT_DITCHAPPL: [MECH_DITCH, MECH_DITCH, MECH_APPL],
        MCT_DITCHSPRI: [MECH_DITCH, MECH_DITCH, MECH_APPL],
    }

    valid_combinations = {}
    for mcid, combination in combinations.items():
        # Test validity of measure combination

        invalid = False
        for measure in combination:

            mc_type = measure["MeasureCombinationType"]

            if mc_type not in mechanisms.keys():
                invalid = True
                logging.error(
                    "Measure combination {}, type {} is not implemented yet".format(
                        mcid, mc_type
                    )
                )
            elif measure["CalculationType.ID"] != 1 and not mc_type in [
                MCT_BUFFSPRI,
                MCT_SPRI,
                MCT_DRAINSPRI,
                MCT_SLBUFFSPRI,
                MCT_DITCHSPRI,
            ]:
                logging.error(
                    "Measure combination {} is not implemented yet".format(mcid)
                )
                invalid = True
            elif measure["QValueType"] == 2 and mc_type == MCT_BUFFAPPL:  ### 2
                logging.error(
                    "ValueType 2 is not implemented (yet) for measure {}".format(
                        measure[TYPE_]
                    )
                )
                invalid = True
            #            elif measure['MValueType'] != 1:
            #                logging.error(
            #                    "Maintenance ValueType 2 is not implemented (yet) for measure {}".format(measure[TYPE_]))
            #                invalid = True
            #            elif measure['IValueType'] != 1:
            #                logging.error(
            #                    "Investment ValueType 2 is not implemented (yet) for measure {}".format(measure[TYPE_]))
            #                invalid = True
            elif measure["EffectValueType"] == 1 and not mc_type in [
                MCT_DRAIN,
                MCT_DRAINAPPL,
                MCT_DRAINSPRI,
            ]:
                logging.error(
                    "Unexpected input in measure: EffectValueType has to be 1 for measure mechanism {}".format(
                        measure["Mechanism"]
                    )
                )
                invalid = True
            elif len(combination) != 1 and mc_type == MCT_SPRI:
                logging.error(
                    "Unexpected input in measure: Only 1 measure of type {}".format(
                        measure[TYPE_]
                    )
                )
                invalid = True
            elif mc_type == MCT_SPRI and (
                measure["SourceType"] != ST_SURFW
                and measure["SourceType"] != ST_GROUNDW
            ):
                logging.error(
                    "Unexpected sourcetype in measure {}, should be 1 or 2, got {}".format(
                        measure["LongDescription"], measure["SourceType"]
                    )
                )
                invalid = True
        #             if len(combinations) != 1:# and measure['MeasureCombinationType'] == 3:
        #                 logging.error("Single measures only allowed with storage and administering".format(
        #                     measure[TYPE_]))

        if sorted([measure[MECHANISM_] for measure in combination]) != sorted(
            mechanisms[mc_type]
        ):
            logging.error(
                "Measure combination {} is not valid, should contain mechanisms {}, contains {}".format(
                    mcid,
                    sorted(mechanisms[mc_type]),
                    sorted([measure[MECHANISM_] for measure in combination]),
                )
            )
            invalid = True

        #        if sum([measure[MECHANISM_] for measure in combination]) != 3 and not mc_type==3 and not mc_type==5:
        #            logging.error("Measure combination {} is not valid".format(mcid))
        #            invalid = True

        if not invalid:
            # REZOWA-118: enforce sorting according to mechanisms dict
            valid_combinations[mcid] = sorted(
                combination, key=lambda x: mechanisms[mc_type].index(x[MECHANISM_])
            )

    return valid_combinations


# TODO: refactor: create separate function for each drought measure
# preferably: pass once!
# output: dimension, idf-droogte, ...?
def calculate_drought_drainage_ditch(combination, pths, measuresdb, hi, ini):
    """Function to calculate drought effect of drainage or ditch measures"""
    for measure in combination:  # assuming only 'drought' type left in mc
        assert measure[TYPE_] == "Drought"

        if measure["Mechanism"] == MECH_DITCH:  # ditch only
            # REZOWA-127
            # with warnings.catch_warnings():
            #    warnings.simplefilter('error')
            percentage_arr = calculate_pctarr_ditch(measure, pths, measuresdb, hi)
            idfwrite(join(pths["mccalc"], "effect_pct.idf"), percentage_arr)
        elif measure["Mechanism"] == MECH_DRAIN:  # drainage only, direct from database
            # REZOWA-125
            percentage_arr = get_effect_arr(measure, pths, measuresdb, hi)
            idfwrite(join(pths["mccalc"], "effect_pct.idf"), percentage_arr)

        if measure["Mechanism"] in [MECH_DRAIN, MECH_DITCH]:  # both drainage and ditch
            dimension_col = {}
            for company in hi.companies:
                dimension_col[company] = 0

            if measure["MeasureCombinationType"] in [
                MCT_DRAIN,
                MCT_DITCH,
            ]:  # if only drainage, or only ditch
                idf_droogte = calculate_write_droughtdamages(
                    percentage_arr, scenario_input, Agricom_In_path, ini
                )

        if measure["Mechanism"] == MECH_APPL:  # toediening van drainage met toediening
            # REZOWA-106
            efficiency = application_eff = get_effect_arr(measure, pths, measuresdb, hi)

            if percentage_arr is None:
                logging.ERROR(
                    "Error: calculating MeasureCombination {} not supported yet".format(
                        mcid
                    )
                )

            tred_sum_col, tred_max_arr = read_data_per_modelcompany_mm2m3_over_years(
                tred_zb_folder, hi, ini, "tred", percentage_arr, return_max_arr=True
            )

            # dimensioned on tred_max_arr: all remaining drought damage prevented by sprinkling
            dimension_col = calculate_dimensions(tred_max_arr, efficiency, hi)
            idf_droogte = copy_drought_damage(
                tred_zb_folder,
                pths["mccalc"],
                str_start_with="droogteschade",
                str_end_with="1231.idf",
                set_to_zero=True,
                ini=ini,
            )

    return dimension_col, idf_droogte, efficiency, application_eff


def calculate_measures(
    combinations,
    hi,
    ini,
    act_ref,
    maintenance_cost_reference,
    csv_waterusage_reference,
    pths,
    measuresdb,
):
    scenario_input = pths["scenario_input"]
    tred_zb_folder = pths["tred_zb_folder"]
    buffer_folder = pths["tred_buffer_folder"]

    companies = hi.companies_idf

    company_mc_results = {}
    company_mc_results_success = {}

    # keep only valid combinations
    combinations = check_validity_measures(combinations)

    # REZOWA-27  #JOOST: outside loop
    # Calculate sum of Tred per model company per year
    tred_sum_col_org, tred_max_arr_org = read_data_per_modelcompany_mm2m3_over_years(
        tred_zb_folder, hi, ini, "tred", return_max_arr=True
    )

    for mcid, combination in combinations.items():
        pths["mccalc"] = join(pths["scenario_output"], "mc_{}".format(mcid))
        makedirs(pths["mccalc"], exist_ok=True)
        Agricom_In_path = join(pths["mccalc"], "Agricom_In")
        makedirs(Agricom_In_path, exist_ok=True)

        # Read from existing data?
        if ini["use_existing"] and exists(
            join(pths["mccalc"], "mc_{}_results.pickle".format(mcid))
        ):
            logging.info(
                "Reading {} ({}) from existing data".format(
                    combination[0]["LongDescription"], mcid
                )
            )

            with open(
                join(pths["mccalc"], "mc_{}_results.pickle".format(mcid)), "rb"
            ) as fp:
                company_results = pickle.load(fp)
                company_results_success = pickle.load(fp)

        # Calculate
        else:
            logging.info(
                "Calculating {} ({})".format(combination[0]["LongDescription"], mcid)
            )

            company_results = {}
            company_results_success = {}

            tred_sum_col, tred_max_arr = (
                tred_sum_col_org.copy(),
                tred_max_arr_org.copy(),
            )
            csv_fn = join(pths["mccalc"], "companyresult_mc_{}_tred.csv".format(mcid))

            csv_from_dict(csv_fn, tred_sum_col, orient="index")

            saltperc = None
            recoveryperc = 100.0  # zero for when no recovery pct has been entered for opslag measure
            efficiency = 100.0
            application_eff = 100.0
            buffermm = None
            runAgricomA = False
            dimension_col = None
            idf_droogte = None
            idf_zout = None
            percentage_arr = None
            highvalue_mask = None
            years = ini["years"]
            q_app_col = {
                c: {
                    1: dict(zip(years, [0] * len(years))),
                    2: dict(zip(years, [0] * len(years))),
                }
                for c in hi.companies
            }

            add_benefit = {}
            sideeffects = {se: 0.0 for se in SIDEEFFECTS_}

            benefit_mdb = sum(measure[ADDITIONAL_BENEFIT_] for measure in combination)

            for measure in combination:
                # REZOWA-36
                add_benefit[measure[MEID_]] = measure[ADDITIONAL_BENEFIT_]

                # REZOWA-103 # TODO: maatregelen niet dubbel tellen
                for se in SIDEEFFECTS_:
                    sideeffects[se] += measure[se]

                if measure["CalculationType"] == 2:
                    runAgricomA = True

                # TODO(Maarten) Refactor
                # not on type but on mechanismtype (!)
                # it's already in here (Mechanism)
                if measure[TYPE_] == "Salt":
                    saltperc = measure[EFFECT_VALUE_]

                    # REZOWA-30
                    # Salt damage
                    idf_zout = calc_salt_damage(
                        scenario_input, saltperc, pths["mccalc"], ini
                    )

                elif measure[TYPE_] == "Drought":
                    logging.info(
                        "- Measure {}, mechanism {}".format(
                            measure["meID"], measure["Mechanism"]
                        )
                    )

                    ######################################
                    ####  DRAINAGE AND DITCH MEASURES  ###
                    ######################################
                    if measure["MeasureCombinationType"] in [
                        MCT_DRAIN,
                        MCT_DRAINAPPL,
                        MCT_DRAINSPRI,
                        MCT_DITCH,
                        MCT_DITCHAPPL,
                        MCT_DITCHSPRI,
                    ]:  # Drainage, drainage plus T, Sloot, Sloot+T
                        if measure["Mechanism"] == MECH_DITCH:  # ditch only
                            # REZOWA-127
                            # with warnings.catch_warnings():
                            #    warnings.simplefilter('error')
                            percentage_arr = calculate_pctarr_ditch(
                                measure, pths, measuresdb, hi
                            )
                            idfwrite(
                                join(pths["mccalc"], "effect_pct.idf"), percentage_arr
                            )
                        elif (
                            measure["Mechanism"] == MECH_DRAIN
                        ):  # drainage only, direct from database
                            # REZOWA-125
                            percentage_arr = get_effect_arr(
                                measure, pths, measuresdb, hi
                            )
                            idfwrite(
                                join(pths["mccalc"], "effect_pct.idf"), percentage_arr
                            )

                        if measure["Mechanism"] in [
                            MECH_DRAIN,
                            MECH_DITCH,
                        ]:  # both drainage and ditch
                            dimension_col = {}
                            for company in hi.companies:
                                dimension_col[company] = 0

                            if measure["MeasureCombinationType"] in [
                                MCT_DRAIN,
                                MCT_DITCH,
                            ]:  # if only drainage, or only ditch
                                idf_droogte = calculate_write_droughtdamages(
                                    percentage_arr, scenario_input, Agricom_In_path, ini
                                )

                        if (
                            measure["Mechanism"] == MECH_APPL
                        ):  # toediening van drainage met toediening
                            # REZOWA-106
                            efficiency = application_eff = get_effect_arr(
                                measure, pths, measuresdb, hi
                            )

                            if percentage_arr is None:
                                logging.ERROR(
                                    "Error: calculating MeasureCombination {} not supported yet".format(
                                        mcid
                                    )
                                )

                            (
                                tred_sum_col,
                                tred_max_arr,
                            ) = read_data_per_modelcompany_mm2m3_over_years(
                                tred_zb_folder,
                                hi,
                                ini,
                                "tred",
                                percentage_arr,
                                return_max_arr=True,
                            )

                            # dimensioned on tred_max_arr: all remaining drought damage prevented by sprinkling
                            dimension_col = calculate_dimensions(
                                tred_max_arr, efficiency, hi
                            )

                            idf_droogte = copy_drought_damage(
                                tred_zb_folder,
                                pths["mccalc"],
                                str_start_with="droogteschade",
                                str_end_with="1231.idf",
                                set_to_zero=True,
                                ini=ini,
                            )

                    ##########################
                    ####  BUFFER MEASURES  ###
                    ##########################
                    elif measure["MeasureCombinationType"] in [
                        MCT_BUFFAPPL,
                        MCT_BUFFSPRI,
                    ]:  # Opslag combi met toediening
                        # REZOWA-112: differentiate storage and application
                        # if storage: perc is recoverypercentage, if application: application efficiency.
                        if measure["Mechanism"] == MECH_BUFF:
                            recovery_perc = get_effect_arr(
                                measure, pths, measuresdb, hi
                            )

                        elif (
                            measure["Mechanism"] == MECH_APPL
                        ):  # assume 1 always before 2, so recovery already there
                            application_eff = get_effect_arr(
                                measure, pths, measuresdb, hi
                            )

                            efficiency = recovery_perc * application_eff / 100.0

                            # REZOWA-24, only when Droughtperc is available
                            tred_arr = idfopen(
                                join(buffer_folder, "Tred_90perc.idf"), pattern="{name}"
                            )

                            # REZOWA-126: mask high-value crops
                            if ini["use_highvalue"]:
                                highvalue_mask = create_highvalue_mask(pths, hi)
                                idfwrite(
                                    join(pths["mccalc"], "highvaluemask.idf"),
                                    highvalue_mask,
                                )

                            dimension_col = calculate_dimensions(
                                tred_arr, efficiency, hi, highvalue_mask
                            )  # dimensioned on 90pct drought

                            # REZOWA-28
                            # Copy general input
                            idf_droogte = copy_drought_damage(
                                buffer_folder,
                                pths["mccalc"],
                                set_to_zero=False,
                                ini=ini,
                                mask=highvalue_mask,
                                origfolder=pths["tred_zb_folder"],
                            )

                    ###############################
                    ####  APPLICATION MEASURES  ###
                    ###############################
                    elif measure["MeasureCombinationType"] in [
                        MCT_APPL,
                        MCT_SPRI,
                    ]:  # alleen toediening: 100% opheffen (effect is effectiviteit toediening)
                        efficiency = application_eff = get_effect_arr(
                            measure, pths, measuresdb, hi
                        )
                        # REZOWA-24, only when Droughtperc is available
                        # dimensioned on 100pct drought
                        dimension_col = calculate_dimensions(
                            tred_max_arr, efficiency, hi, None
                        )

                        idf_droogte = copy_drought_damage(
                            tred_zb_folder,
                            pths["mccalc"],
                            str_start_with="droogteschade",
                            str_end_with="1231.idf",
                            set_to_zero=True,
                            ini=ini,
                        )

                    ###############################
                    ####  SOIL BUFFER MEASURES  ###
                    ###############################
                    elif measure["MeasureCombinationType"] in [
                        MCT_SLBUFF,
                        MCT_SLBUFFAPPL,
                        MCT_SLBUFFSPRI,
                    ]:

                        if measure["Mechanism"] == MECH_SLBUFF:
                            # REZOWA-113
                            # REZOWA-125
                            buffermm = get_effect_arr(measure, pths, measuresdb, hi)
                            idfwrite(join(pths["mccalc"], "buffermm.idf"), buffermm)

                            if (
                                measure["MeasureCombinationType"] == MCT_SLBUFF
                            ):  # no additional application
                                # Assumes existence of pre-created buffer grids 5.0 or 2.0 mm
                                dimension_col = {}
                                for company in hi.companies:
                                    dimension_col[company] = 0

                                idf_droogte = calc_drought_damage_from_value_arr(
                                    buffer_folder,
                                    pths["mccalc"],
                                    buffermm,
                                    years,
                                    str_start_with="droogteschade",
                                    str_end_with="??mm.idf",
                                )

                        elif (
                            measure["Mechanism"] == MECH_APPL
                        ):  # toediening van bodembuffer met toediening
                            # REZOWA-117
                            efficiency = application_eff = get_effect_arr(
                                measure, pths, measuresdb, hi
                            )

                            # determine reduction of transpiration reduction
                            (
                                tred_sum_col,
                                tred_max_arr,
                            ) = read_data_per_modelcompany_mm2m3_over_years(
                                buffer_folder,
                                hi,
                                ini,
                                "tred",
                                absolute_mm_arr=buffermm,
                                return_max_arr=True,
                            )

                            # dimensioned on tred_max_arr: all remaining drought damage prevented by sprinkling
                            dimension_col = calculate_dimensions(
                                tred_max_arr, efficiency, hi, None
                            )

                            idf_droogte = copy_drought_damage(
                                tred_zb_folder,
                                pths["mccalc"],
                                str_start_with="droogteschade",
                                str_end_with="1231.idf",
                                set_to_zero=True,
                                ini=ini,
                            )

                    else:
                        logging.ERROR(
                            "MeasureCombinationType {} not supported yet".format(
                                measure["MeasureCombinationType"]
                            )
                        )
                else:
                    pass

            # TEMPORARY SOLUTION!: if idf_drought or idf_salt not present (e.g. fo mc 2, 5), set to no effect and report error
            if idf_droogte is None:
                logging.warning(
                    "No drought effect implemented for measure combination {}, assuming no effect".format(
                        mcid
                    )
                )
                path = join(scenario_input, "MetBeregening")
                idf_droogte = copy_drought_damage(
                    path, pths["mccalc"], False, ini, "droogteschade", "1231.idf"
                )
            if idf_zout is None:
                logging.warning(
                    "No salt effect implemented for measure combination {}, assuming no effect".format(
                        mcid
                    )
                )
                path = join(scenario_input, "MetBeregening")
                idf_zout = copy_drought_damage(
                    path, pths["mccalc"], False, ini, "zoutschade", "1231.idf"
                )

            # REZOWA-33
            # Total damage
            calc_total_damage(idf_zout, idf_droogte, pths["mccalc"])

            # REZOWA-125 - quick fix for now  # TODO: value per company
            if isinstance(efficiency, (np.ndarray)):
                efficiency = efficiency.arr.mean()
            if isinstance(application_eff, (np.ndarray)):
                application_eff = application_eff.arr.mean()

            # REZOWA-26
            # Calculation water usage Qapp
            sources = get_company_source(combination, efficiency, pths, hi)
            sources_appl = get_company_source(combination, application_eff, pths, hi)

            if measure["MeasureCombinationType"] in [
                MCT_BUFFAPPL,
                MCT_APPL,
                MCT_BUFFSPRI,
                MCT_SPRI,
                MCT_DRAINAPPL,
                MCT_DRAINSPRI,
                MCT_SLBUFFAPPL,
                MCT_SLBUFFSPRI,
                MCT_DITCHAPPL,
                MCT_DITCHSPRI,
            ]:
                q_app_col = calculate_q_app(
                    tred_sum_col, dimension_col, sources
                )  # q_app_col contains the stored volume or (w/o storage) the sprinkled volume
                csv_fn = join(
                    pths["mccalc"], "companyresult_mc_{}_qapp.csv".format(mcid)
                )

                csv_from_dict(
                    csv_fn,
                    q_app_col,
                    nested=True,
                    nested_label="SourceType",
                    orient="index",
                )

                # if runAgricomA: REZOWA-173
                # REZOWA-119  q_app_col_appl contains the sprinkle volume, not the stored volume
                q_app_col_appl = calculate_q_app(
                    tred_sum_col, dimension_col, sources_appl
                )
                csv_fn = join(
                    pths["mccalc"], "companyresult_mc_{}_qapp2.csv".format(mcid)
                )

                csv_from_dict(
                    csv_fn,
                    q_app_col_appl,
                    nested=True,
                    nested_label="SourceType",
                    orient="index",
                )
                write_irr_files(
                    sources_appl,
                    Agricom_In_path,
                    q_app_col_appl,
                    hi,
                    mask=highvalue_mask,
                )

                waterusage = sum_water_usage(q_app_col)
                company_results = waterusage
            else:
                waterusage = {}
                for company in hi.companies:
                    waterusage[company] = {"WaterUsage": 0}
                company_results = waterusage

            logging.info("Running Agricom")
            if runAgricomA:
                run_measure_agricom(
                    pths["hydroinput"],
                    scenario_input,
                    pths["mccalc"],
                    "control_AGRICOM_MeasureRB.inp",
                    ini,
                )
            else:
                run_measure_agricom(
                    pths["hydroinput"],
                    scenario_input,
                    pths["mccalc"],
                    "control_AGRICOM_Measure.inp",
                    ini,
                )

            # REZOWA-84
            logging.info("Calculating costs and benefits")
            cost_combination = []
            # throw out double measure-investments:
            for comb in combination:
                if comb["InvestmentCost.ID"] not in [
                    c["InvestmentCost.ID"] for c in cost_combination
                ]:
                    cost_combination.append(comb)

            # REZOWA-35
            # Maintenancecosts
            # REZOWA-131: make cost calculation more flexible, possibility of lookup
            # function calculate_measure_costs
            maintenancecost, investmentcost, lifespans = calculate_measure_costs(
                mcid,
                cost_combination,
                dimension_col,
                highvalue_mask,
                pths,
                measuresdb,
                hi,
                ini,
            )

            # REZOWA-32
            # Write Agricom input and run
            #         -93 if:

            # Companyresult csv
            # TODO: gebruiken we deze? anders effect per bedrijf niet beter?
            csv_fn = join(pths["mccalc"], "companyresult_mc_{}.csv".format(mcid))

            salt_col = {k: saltperc for k, v in dimension_col.items()}
            drought_col = {k: efficiency for k, v in dimension_col.items()}

            all_columns = {
                "Dimension_m3": dimension_col,
                "MaintenanceCost_EuroPerYear": {
                    c: sum(maintenancecost[c].values()) / len(maintenancecost[c])
                    for c in maintenancecost.keys()
                },
                "SaltPercDamage": salt_col,
                "DroughtPercDamage": drought_col,
            }
            for mid in investmentcost[list(investmentcost.keys())[0]].keys():
                all_columns.update(
                    {
                        "InvestmentCost_{}_Euro".format(mid): {
                            k: v[mid] for k, v in investmentcost.items()
                        }
                    }
                )
            # all_columns.update({'SideEffect_{}'.format(x): v for x, v in sideeffects.items()})
            all_columns.update(sideeffects)
            for mid, benefit in add_benefit.items():
                all_columns.update({"Benefits_{}_EuroPerYear".format(mid): benefit})

            csv_from_dict(csv_fn, all_columns)

            # REZOWA-38
            # /modelcompany/year
            meas_outC_folder = join(pths["mccalc"], pths["agricom_outC"])
            act_meas = read_act(meas_outC_folder, hi, ini)

            T = len(ini["years"])
            csv = {}
            for company in hi.companies:
                (
                    costs,
                    benefits,
                    nbc,
                    crop_benefits,
                    sprinkl_benefits,
                ) = calc_cost_benefit(
                    act_meas[company],
                    act_ref[company],
                    benefit_mdb,
                    maintenance_cost_reference[company],
                    investmentcost[company],
                    maintenancecost[company],
                    lifespans,
                    ini["discountrate"],
                )
                csv[company] = {
                    "Costs": costs,
                    "Benefits": benefits,
                    NBC_: nbc,
                    "Benefits_cropdamage": crop_benefits,
                    "Benefits_sprinkling": sprinkl_benefits,
                }

                Q_sw_ref = csv_waterusage_reference[company][WATERUSAGE_SW_M3_]
                Q_gw_ref = csv_waterusage_reference[company][WATERUSAGE_GW_M3_]
                Q_gw = 0
                Q_sw = 0
                if 1 in q_app_col[company]:  # surface water
                    Q_sw = sum(q_app_col[company][1].values()) / T

                if 2 in q_app_col[company]:  # groundwater
                    Q_gw = sum(q_app_col[company][2].values()) / T

                new_data = {
                    GROUND_WATER_: Q_gw,
                    SURFACE_WATER_: Q_sw,
                    TOENAME_GW_: Q_gw - Q_gw_ref,
                    TOENAME_SW_: Q_sw - Q_sw_ref,
                    NBC_: nbc,
                    COST_: costs / T,
                    BENEFIT_: benefits / T,
                    BENEFIT_CROP_: crop_benefits / T,
                    BENEFIT_SPRINKL_: sprinkl_benefits / T,
                    SUBAREA_: hi.subareas[company],
                    COMPANY_TYPE_: hi.companytypes[company],
                }
                # REZOWA 103
                new_data.update(sideeffects)

                company_results[company].update(new_data)

            ##subarea
            ##company type
            csv_fn = join(pths["mccalc"], "companyresult_mc_{}_nbc.csv".format(mcid))

            for c, typ in hi.companytypes.items():
                csv[c]["CompanyType"] = typ
            for c, sub in hi.subareas.items():
                csv[c]["SubArea"] = sub
            csv_from_dict(csv_fn, csv, orient="index")

            # REZOWA-19 calc_fwoo_per_company
            # Determine if measure combination is successful
            fwoo_measure = calc_fwoo_per_combination(
                combination, hi, pths, highvalue_mask
            )

            # When all measures from combination are successful, than the mc is also successful
            fwoo_col = calc_overall_fwoo(fwoo_measure, hi)

            # Write FWOO per company to csv
            csv_fn = join(pths["mccalc"], "companyresult_mc_{}_fwoo.csv".format(mcid))

            all_columns = {"FWOO": fwoo_col}
            for mid, fwoo in fwoo_measure.items():
                all_columns.update({"M{}_FWOO".format(mid): fwoo})
            csv_from_dict(csv_fn, all_columns)

            # REZOWA-56
            # select only measure combinations that are successful (FWOO grid)
            for company in hi.companies:
                if fwoo_col[company] == 1:
                    company_results_success[int(company)] = company_results[company]

            # Write results to pickle
            with open(
                join(pths["mccalc"], "mc_{}_results.pickle".format(mcid)), "wb"
            ) as fp:
                pickle.dump(company_results, fp, pickle.HIGHEST_PROTOCOL)
                pickle.dump(company_results_success, fp, pickle.HIGHEST_PROTOCOL)

        # REZOWA-146
        # Store results per measure combination in overall results
        company_mc_results[mcid] = company_results
        company_mc_results_success[mcid] = company_results_success

    return company_mc_results, company_mc_results_success
