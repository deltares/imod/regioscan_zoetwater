#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import makedirs
from os.path import join, abspath, dirname
from subprocess import run, PIPE, STDOUT
from multiprocessing import Pool
import logging

from jinja2 import Environment, PackageLoader
import numpy as np

from regioscan_zoetwater.constants import *
from regioscan_zoetwater.utils import (
    get_years,
    reduce_timeseries_per_company_to_df,
    idfopen,
)

currentdir = dirname(abspath(__file__))
calcdir = abspath(join(currentdir, "../calc"))
env = Environment(loader=PackageLoader("regioscan_zoetwater", "templates"))

# 93
def run_measure_agricom(hydrofolder, scenario, modelcombo, template_fn, ini):
    template = env.get_template(template_fn)
    # create folders if they don't exist yet
    makedirs(modelcombo, exist_ok=True)
    inputfile = join(modelcombo, "agricom_measure.inp")
    bindir = dirname(join(currentdir, ini["AGRICOM_exe"]))
    with open(inputfile, "w") as f:
        f.write(
            template.render(
                hydrofolder=hydrofolder,
                scenario=scenario,
                modelcombo=modelcombo,
                bindir=bindir,
                startdate=ini["startdate"],
                enddate=ini["enddate"],
            )
        )
    if not ini["test"]:
        call_agricom(inputfile, ini)
    return


def call_agricom(inputfile, ini, timeout=None):
    """Call AGRICOM exe."""
    exe = join(currentdir, ini["AGRICOM_exe"])
    logging.debug(f"Called Agricom: {exe} {inputfile}")
    result = run(
        [exe, inputfile], stderr=STDOUT, stdout=PIPE, check=True, timeout=timeout
    )
    if result.returncode != 0:
        for line in result.stdout.decode("utf-8").split("\n"):
            logging.error(line.strip()) if line.strip() != "" else 1
    else:
        for line in result.stdout.decode("utf-8").split("\n"):
            logging.debug(line.strip()) if line.strip() != "" else 1


# hergebruiken voor issue 95
def read_module_a(folder, hi, ini):
    """Read all years for Module A output.

    Module A contains idf files for each year for:
    - arbeidskosten
    - energiekosten
    - heffingen
    - vastekosten

    Read these into a DataArray and sum these columns for
    each modelbedrijf.
    """
    years = list(range(ini["startyear"], ini["endyear"] + 1))

    investment = idfopen(join(folder, "vastekosten_*.idf"), pattern="{name}_{time}")
    energie = idfopen(join(folder, "energiekosten_*.idf"), pattern="{name}_{time}")
    heff = idfopen(join(folder, "heffingen_*.idf"), pattern="{name}_{time}")
    arbeid = idfopen(join(folder, "arbeidskosten_*.idf"), pattern="{name}_{time}")
    maintenance = energie + heff + arbeid
    maintenance.name = "maintenance"
    # if not ~np.any(np.isnan(investment[0].values)[~np.isnan(hi.companies_idf)]):
    #    logging.error(f"Mask of Agricom output in {folder} is different than that of modelbedrijven")

    # Check if all years are added
    miss_years = [
        str(y)
        for y in years
        if y not in get_years(maintenance) or y not in get_years(investment)
    ]
    if len(miss_years):
        logging.error(f"Not all years found in output: missing {','.join(miss_years)}")
        exit()

    """  ==> why here and not in HydroInput?
    ######
    # Check if each modelbedrijf is only part of one deelgebied
    for modelbedrijf in np.unique(modelbedrijven.arr.compressed()):
        # slice for each modelbedrijf
        slices[modelbedrijf] = modelbedrijven.arr == modelbedrijf
    """
    # TODO: order should be reversed: [time][company][value]
    investment_csv = reduce_timeseries_per_company_to_df(
        investment, hi, func=np.mean
    )  # ,first='companies')
    maintenance_csv = reduce_timeseries_per_company_to_df(
        maintenance, hi, func=np.sum
    )  # ,first='companies')
    return investment_csv, maintenance_csv
