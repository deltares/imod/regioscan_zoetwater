import logging
from regioscan_zoetwater.utils import (
    csv_from_dict,
    read_act,
    read_data_per_modelcompany_mm2m3_over_years,
    calc_average_over_years,
    df_to_orddict,
)
from regioscan_zoetwater.agricom import read_module_a, call_agricom, env
from os.path import join, dirname
from os import makedirs
from pandas import DataFrame
from regioscan_zoetwater.constants import (
    WATERUSAGE_GW_M3_,
    WATERUSAGE_SW_M3_,
)  # , STARTDATE, ENDDATE, STARTYEAR, ENDYEAR


def calculate_reference(companies, hi, pths, ini):
    # Read the costs from agricom reference calculation and write to file
    investment, maintenance = read_module_a(pths["ref_outA_folder"], hi, ini)
    csv_i_fn = join(pths["referencefolder"], "IrrigationInvestmentCost.csv")
    csv_m_fn = join(pths["referencefolder"], "IrrigationMaintenanceCost.csv")
    investment.to_csv(csv_i_fn, sep=";", index_label="CompanyNumber")
    maintenance.to_csv(csv_m_fn, sep=";", index_label="CompanyNumber")

    # Determine water usage in reference situation
    gw_sum_col = read_data_per_modelcompany_mm2m3_over_years(
        pths["irrigation_folder"], hi, ini, "beregening_gw"
    )
    sw_sum_col = read_data_per_modelcompany_mm2m3_over_years(
        pths["irrigation_folder"], hi, ini, "beregening_sw"
    )
    csv_from_dict(
        join(pths["referencefolder"], "GroundwaterSprinkling.csv"),
        gw_sum_col,
        orient="index",
    )
    csv_from_dict(
        join(pths["referencefolder"], "SurfwaterSprinkling.csv"),
        sw_sum_col,
        orient="index",
    )
    csv_waterusage_reference = {}
    for company in gw_sum_col.keys():
        gw_average = calc_average_over_years(gw_sum_col[company])
        sw_average = calc_average_over_years(sw_sum_col[company])
        csv_waterusage_reference[company] = {
            WATERUSAGE_GW_M3_: gw_average,
            WATERUSAGE_SW_M3_: sw_average,
        }
    csv_fn = join(pths["referencefolder"], "WaterUsage.csv")
    csv_from_dict(csv_fn, csv_waterusage_reference, orient="index")
    act_ref = read_act(pths["ref_outC_folder"], hi, ini)

    return (
        df_to_orddict(maintenance, hi, first="companies"),
        csv_waterusage_reference,
        act_ref,
    )


def run_reference_agricom(hydrofolder, scenario, referencefolder, ini):

    template = env.get_template("control_AGRICOM_Ref.inp")
    inputfile = join(referencefolder, "agricom_ref.inp")

    # create folders if they don't exist yet
    makedirs(referencefolder, exist_ok=True)

    bindir = dirname(join(dirname(__file__), ini["AGRICOM_exe"]))
    with open(inputfile, "w") as f:
        f.write(
            template.render(
                output=referencefolder,
                hydrofolder=hydrofolder,
                scenario=scenario,
                bindir=bindir,
                startdate=ini["startdate"],
                enddate=ini["enddate"],
            )
        )

    if not ini["test"]:
        call_agricom(inputfile, ini)
