# STARTDATE = '19800101'
# ENDDATE = '20101231'
# STARTYEAR = int(STARTDATE[:4])
# ENDYEAR = int(ENDDATE[:4])
#
# DISCOUNTRATE = 0.01

TYPE_ = "Type"
SUBAREA_ = "subarea"
ID_ = "ID"
MEID_ = "meID"
ADDITIONAL_BENEFIT_ = "AdditionalBenefit"
COMPANY_TYPE_ = "companytype"
NBC_ = "NBC"
COST_ = "cost (E/y)"
MECHANISM_ = "Mechanism"
WATERUSAGE_SW_M3_ = "Waterusage_sw (m3/y)"
WATERUSAGE_GW_M3_ = "Waterusage_gw (m3/y)"
RANK_ = "rank"
BENEFIT_ = "benefit (E/y)"
BENEFIT_CROP_ = "benefit cropdamage (E/y)"
BENEFIT_SPRINKL_ = "benefit sprinkling (E/y)"
COMPANY_NUMBER_ = "CompanyNumber"
QAPP_GW_SW_ = "Qapp_gw_sw (m3)"
MC_ = "mc"

GROUND_WATER_ = "groundwater"
SURFACE_WATER_ = "surfacewater"
TOENAME_GW_ = "Toename_gw (m3/y)"
TOENAME_SW_ = "Toename_sw (m3/y)"

EFFECT_VALUE_ = "EffectValue"
EFFECT_VALUETYPE_ = "QValueType"
EFFECT_VALUEMAP_ = "EffectValueMap"
SOURCE_VALUE_ = "SourceType"
SOURCE_VALUETYPE_ = "SourceValueType"
SOURCE_VALUEMAP_ = "SourceMap"
VALUE_DIRECT = 1
VALUE_LOOKUP = 2
VALUE_MAP = 3

DIMENSION_VOLUME = 1
DIMENSION_AREA = 2

SIDEEFFECTS_ = [
    "SEwaterkwaliteit",
    "SEpiekafvoer",
    "SEbodemdaling",
    "SEverdroging",
    "SEuitspoelN",
    "SEuitspoelP",
]

# MeasureCombinationTypes
MCT_BUFFAPPL = 1
MCT_APPL = 2
MCT_DRAIN = 3
MCT_BUFFSPRI = 4
MCT_SPRI = 5
MCT_D2B = 6
MCT_DRAINAPPL = 7
MCT_DRAINSPRI = 8
MCT_SLBUFF = 9
MCT_SLBUFFAPPL = 10
MCT_SLBUFFSPRI = 11
MCT_DITCH = 12
MCT_DITCHAPPL = 13
MCT_DITCHSPRI = 14

# MechanismTypes
MECH_BUFF = 1
MECH_APPL = 2
MECH_DRAIN = 3
MECH_SLBUFF = 4
MECH_DITCH = 5

# CalculationTypes
CALCT_STANDARD = 1
CALCT_SPRINKL = 2
CALCT_D2B = 3

# SourceTypes
ST_SURFW = 1
ST_GROUNDW = 2
