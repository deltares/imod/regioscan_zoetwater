#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from os import scandir
from os.path import dirname, abspath, join, exists
import logging
from collections import Counter
import configparser
import numpy as np
import pandas as pd
import xarray as xr
import scipy.interpolate
from pandas import DataFrame, Series, concat, read_csv, to_datetime
from pandas.io.json import json_normalize
from pyodbc import connect
from collections import OrderedDict
from imod.idf import open as open_, write as write_
from imod.util import spatial_reference
from imod.select import points_set_values
from datetime import datetime
import warnings


def unique(da, return_counts=False):
    """Get unique values from dataArray"""
    # TODO: handle multiple dims
    if not isinstance(da, np.ndarray):
        da = da.values
    u, counts = np.unique(da, return_counts=True)
    u2 = u[~np.isnan(u)]
    counts = counts[~np.isnan(u)]

    if return_counts:
        return u2, counts
    else:
        return u2


def idfopen(fn, pattern="{name}", **kwargs):
    da = open_(fn, pattern=pattern, **kwargs)
    if "time" in da.coords and len(da.coords["time"]) == 1:
        try:
            da = da.squeeze(dim="time", drop=True)  # drop time coord if only 1
        except ValueError:
            print(da)
            raise
    return da


def idfwrite(fn, da, nodata=-9999, **kwargs):
    return write_(
        fn, da.squeeze(drop=True), nodata=nodata, **kwargs
    )  # drop coords if only 1


def get_years(da):
    """get unique years in dataarray"""
    return (
        da.groupby("time.year").count(dim=xr.ALL_DIMS).year.values.tolist()
    )  # bit of a hack around xr, but works


# l = ['mc','CompanyNumber','SEuitspoelN','surfacewater','SEwaterkwaliteit','SEverdroging','SEuitspoelP','ziternietin']
def order_cols(cols):
    # reference order
    col_order = [
        "subarea",
        "mc",
        "CompanyNumber",
        "companytype",
        "NBC",
        "benefit",
        "BenefitsEuroPerYear",
        "cost",
        "Costs_EuroPerYear",
        "Toename_gw",
        "Toename_gw_prc",
        "groundwater",
        "Watergebruik_gw_m3",
        "Toename_sw",
        "Toename_sw_prc",
        "surfacewater",
        "Watergebruik_sw_m3",
        "Qapp_gw_sw",
        "SEpiekafvoer",
        "SEwaterkwaliteit",
        "SEuitspoelN",
        "SEuitspoelP",
        "SEverdroging",
        "SEbodemdaling",
        "rank",
    ]

    # extract elements that are and are not in list
    in_l = [c for c in cols if c in col_order]
    out_l = [c for c in cols if c not in col_order]
    return sorted(in_l, key=col_order.index) + out_l


def sum_dicts(dicts):
    c = Counter()
    for d in dicts:
        c.update(d)

    return dict(c)


def csv_from_dict(
    fn,
    data,
    index_label="CompanyNumber",
    orient="columns",
    nested=False,
    nested_label="nested",
):
    """Create csv from data dicts."""
    if nested:
        df = DataFrame.from_dict(
            DataFrame.from_dict(
                {(i, j): data[i][j] for i in data.keys() for j in data[i].keys()},
                orient="index",
            )
        )
        df.reset_index(inplace=True, level=1)
        new_columns = df.columns.values.tolist()
        new_columns[0] = nested_label
        df.columns = new_columns
    else:
        df = DataFrame.from_dict(data, orient=orient)

    # Sort column headers
    years = []
    other = []
    for col in df:
        if (isinstance(col, str) and col.startswith("19")) or isinstance(
            col, (int, np.integer, np.float)
        ):
            years.append(col)
        else:
            other.append(col)
    df = df[order_cols(other) + sorted(years)]
    df.to_csv(fn, index_label=index_label, sep=";")


def csv_from_series(fn, data, index_label="CompanyNumber"):
    """Create csv from multiple series."""
    df = concat(data, axis=1)

    # Sort column headers
    years = []
    other = []
    for col in df:
        if (isinstance(col, str) and col.startswith("19")) or isinstance(
            col, (int, np.integer, np.float)
        ):
            years.append(col)
        else:
            other.append(col)
    df = df[order_cols(other) + sorted(years)]
    df.to_csv(fn, index_label=index_label, sep=";")


def idf_from_company_value(compvals, indexarr, mask=None):
    """map a series or dict of company->value on the companies idf
    outside of xarray for performance reasons"""
    retarr = indexarr.copy(deep=True).load()

    # REZOWA-136
    # compvals = dict(compvals)
    # compvals[np.nan] = np.nan

    retarr.values[~np.isnan(retarr.values)] = np.vectorize(compvals.get)(
        retarr.values[~np.isnan(retarr.values)]
    )
    retarr.values[retarr.values == None] = np.nan
    # REZOWA-126
    # for c in hi.companies:
    #    retarr.values[(compidf==c)] = compvals[c]

    if mask is not None:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")

            retarr.values *= mask.values

    try:
        retarr.name = compvals.name
    except:
        retarr.name = "companyvalue"
    # retarr = xr.DataArray(retarr,coords=hi.companies_idf.coords,attrs=hi.companies_idf.attrs,dims=hi.companies_idf.dims,name=name)
    return retarr


# REZOWA-97
# TODO: support multiple indexes (e.g. subarea - companytype)
def csv2idf(fn_csv="", index_col=0, index_idf="", dir_out="idf", cols="all"):
    indexarr = idfopen(index_idf, pattern="{name}").load()
    df = read_csv(fn_csv, sep=";")
    if isinstance(index_col, int):
        index_col = df.columns[index_col]
    df = df.set_index(index_col)

    # set values not in index to nodata
    idxarrvals = unique(indexarr)
    idxsetna = [i for i in idxarrvals if i not in df.index]
    for i in idxsetna:
        indexarr.values[indexarr == i] = np.nan

    # assign df values to idf and write
    for col in df.columns:
        fn = col.replace(" ", "_")
        fn = fn.replace("(", "").replace(")", "").replace(r"/", "")
        arr = idf_from_company_value(df[col], indexarr)
        logging.info("Writing map " + "%s.idf" % col)
        idfwrite(join(dir_out, "%s.idf" % fn), arr)


def get_timeseries(
    folder, pattern="droogteschade*1231.idf", ini=None, idfopen_pattern="{name}_{time}"
):
    try:
        da = idfopen(join(folder, pattern), pattern=idfopen_pattern)
    except KeyError:
        # hack to circumvent imod.util.decompose throwing KeyError. Solved from commit 06ec7fe1
        da = idfopen(
            join(folder, pattern),
            pattern=idfopen_pattern.replace("{name}_{time}", "{name}_{year}"),
        )
        da = da.rename({"year": "time"})
        da.coords["time"] = [pd.Timestamp(f"{t.values}1231") for t in da.time]

    # if only years in pattern, and not 1231, time is not set to 12/31, correct
    if "time" in da.coords:
        da.coords["time"] = [
            datetime(pd.Timestamp(t.values).year, 12, 31) for t in da.time
        ]

    if ini is not None:
        # check for years
        years = range(ini["startyear"], ini["endyear"] + 1)
        miss_years = [str(y) for y in years if y not in get_years(da)]
        if len(miss_years):
            logging.error(
                f"Missing years in timeseries {pattern}: {','.join(miss_years)}"
            )
            exit()

        # REZOWA-134 drop years from timeseries
        add_years = [y for y in get_years(da) if y not in years]
        if len(add_years):
            da = da.drop(labels=[datetime(y, 12, 31) for y in add_years], dim="time")

        pass

    return da


def apply_company(idf, hi, name=None, func=np.sum):
    """Utility function to sum a timeseries per company, outside xarray for performance"""
    return Series(
        {c: func(idf.values[hi.companies_idf == c]) for c in hi.companies}, name=name
    )


def reduce_timeseries_per_company_to_df(da, hi, func=np.sum, savemem=True):
    """Utility function to sum a timeseries per company,
    returns a DataFrame with companies on rows and (if time) years on cols"""
    da = da.drop(["dx", "dy"])  # fix to remove coordinates from output DF
    """
    if savemem:
        df = da.groupby(hi.companies_idf).reduce(func=func,dim='stacked_y_x')
    else:
        da = da.load()
        df = da.groupby(hi.companies_idf).reduce(func=func,dim=['x','y'])#stacked_y_x')
    df = df.to_dataframe().unstack().T
    df.index = df.index.droplevel(0)
    if 'time' in da.dims:
        df.columns = [t.year for t in df.columns]
    """
    # speedup: instead of using xarray's groupby, use pandas'
    if "dx" in hi.companies_idf.coords:
        comps = hi.companies_idf.drop(["dx", "dy"]).to_dataframe()
    else:
        comps = hi.companies_idf.to_dataframe()

    if "time" in da.coords:
        df = {}
        for t in da.coords["time"]:
            dat = da.sel(time=t).to_dataframe()
            dat["comp"] = comps
            df[to_datetime(t.values).year] = dat.groupby("comp").agg(func).squeeze()
        df = concat(df, axis=1)
    else:
        dat = da.to_dataframe()
        dat["comp"] = comps
        df = dat.groupby("comp").agg(func)
    return df.sort_index().squeeze()


def df_to_orddict(df, hi, first="time"):
    """Utility function to convert a Dataframe into an ordered dict"""
    if isinstance(df, DataFrame) and len(df.columns) > 1:
        if first == "time":
            df = df.T
            return OrderedDict({c: df.loc[c].to_dict() for c in df.index})
        else:
            return OrderedDict({c: df.loc[c].to_dict() for c in hi.companies})
    else:
        return OrderedDict(df.to_dict())


def reduce_timeseries_per_company_to_orddict(da, hi, first="time", func=np.sum):
    """Utility function to sum a timeseries per company,
    returns an ordered dict for now"""  # TODO: use dataframes instead of OrdDicts
    # df = da.apply_ufunc()
    df = reduce_timeseries_per_company_to_df(da, hi, func=func)
    # df = df.unstack().T

    return df_to_orddict(df, hi, first)


def reduce_timeseries_per_company_to_csv(da, hi, func=np.sum):
    """Utility function to sum a timeseries per company,
    and return a company-indexed dataframe with years as columns"""
    return reduce_timeseries_per_company_to_df(da, hi, func=func)


def avg_timeseries_per_company_to_csv(da, hi):
    """Utility function to average a timeseries per company,
    and return a company-indexed dataframe with years as columns"""
    df = da.groupby(hi.companies_idf).mean(dim="stacked_y_x").to_dataframe().unstack().T

    df.columns = [t.year for t in df.columns]
    return df


def read_act(folder, hi, ini):
    """REZOWA-38"""

    act = get_timeseries(folder, "ActYldEur*.idf", ini)
    # REZOWA-166
    act = act * hi.dx * hi.dy / 10000.0  # from E/ha to E
    return reduce_timeseries_per_company_to_orddict(act, hi, first="companies")


def calc_average_over_years(data_per_year):
    years = sorted(data_per_year.keys())
    T = len(years)
    benefits = 0
    try:
        average = sum(i for i in data_per_year.values()) / T
    except TypeError:
        print(data_per_year)
        print(T)
        print(type(data_per_year), type(T))
        raise
    return average


# REZOWA-170
# REZOWA-171
def interpolate_on_valuearr(da, valuearr, dim, extrapolate=False):
    """interpolate per time in da on dimension dim using values from valuearr"""

    # make coord dim float and ensure sorting
    # da = da.load()
    da.coords[dim] = da.coords[dim].astype(float)
    if not da.indexes[dim].is_monotonic_increasing:
        da = da.sortby(dim, ascending=True)

    # min and max necessary value for interpolation
    # valuearr = valuearr.load()
    minval = min(
        min(da.coords[dim]) - 1.0, valuearr.min().compute()
    )  # + / - 1 to prevent duplicates along dim
    maxval = max(max(da.coords[dim]) + 1.0, valuearr.max().compute())

    if not extrapolate:
        # if extrapolate=False: 'extrapolate' to lowest or highest available value
        damin = da.isel({dim: 0})
        damax = da.isel({dim: -1})
    else:
        # extrapolate da linearly outside bounds # REZOWA-171
        daminslope = (da.isel({dim: 1}) - da.isel({dim: 0})) / (
            da.coords[dim][1] - da.coords[dim][0]
        )
        damin = da.isel({dim: 0}) + (minval - da.coords[dim][0]) * daminslope
        damaxslope = (da.isel({dim: -1}) - da.isel({dim: -2})) / (
            da.coords[dim][-1] - da.coords[dim][-2]
        )
        damax = da.isel({dim: -1}) + (maxval - da.coords[dim][-1]) * damaxslope

    # put back together
    damin[dim] = minval
    damax[dim] = maxval
    da = xr.concat((damin, da, damax), dim=dim)

    # select from interpolated absolute_mm da using absolute_mm_arr
    points_df = valuearr.to_dataframe()
    points_df = (
        points_df.reset_index()
        .dropna(axis=0)
        .reindex(["y", "x", valuearr.name], axis=1)
    )
    result_da = []
    for t in da.time:
        dat = da.sel(time=t).fillna(0).compute()
        # interpolate between soil buffer values
        interp = scipy.interpolate.interpn(
            (dat.y[::-1], dat.x, dat.coords[dim]),
            np.flipud(dat.values),
            points_df.values,
        )
        out = dat.isel({dim: 0}).copy() * 0
        out = out.drop(dim).load()
        out = points_set_values(
            da=out,
            values=np.nan_to_num(interp),
            y=points_df.y.values,
            x=points_df.x.values,
        )
        result_da.append(out)

    return xr.concat(result_da, dim="time")


def read_data_per_modelcompany_mm2m3_over_years(
    folder,
    hi,
    ini,
    itemstart,
    percentage_arr=None,
    absolute_mm_arr=None,
    return_max_arr=False,
):
    # REZOWA-117
    # REZOWA-125
    if absolute_mm_arr is not None:
        da = get_timeseries(
            folder,
            pattern=f"{itemstart}_*1231_??mm.idf",
            ini=ini,
            idfopen_pattern="{name}_{time}_{absolute_mm}mm",
        )  # soil buffer specific Tred
    else:
        da = get_timeseries(folder, pattern=f"{itemstart}*.idf", ini=ini)

    # REZOWA-106
    if percentage_arr is not None:
        da = da * (1.0 - percentage_arr / 100.0)
    # REZOWA-125
    # REZOWA-170
    if absolute_mm_arr is not None:
        # select from interpolated absolute_mm da using absolute_mm_arr
        da = interpolate_on_valuearr(da, absolute_mm_arr, "absolute_mm")

    max_arr = da.max(dim="time")
    max_arr.attrs = hi.companies_idf.attrs

    mm_to_m3 = hi.dx * hi.dy / 1000.0  # Constant.
    da_m3 = da * mm_to_m3
    da_m3.name = itemstart

    retval = reduce_timeseries_per_company_to_orddict(
        da_m3, hi, first="companies", func=np.sum
    )

    if return_max_arr:
        return retval, max_arr
    else:
        return retval


class MeasuresDB:
    def __init__(self, fn="../data/maatregelen.accdb"):
        # TODO Make class to properly close db connection
        currentdir = dirname(abspath(__file__))
        db_fn = abspath(join(currentdir, fn))

        conn_str = (
            "DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};"
            + "DBQ={};".format(db_fn)
        )
        self.cnxn = connect(conn_str)
        self.cnxn.autocommit = True
        logging.info("Connected to MS Access DB")
        self.crsr = self.cnxn.cursor()

    def list_tables(self):
        for table_info in self.crsr.tables():
            logging.info(table_info.table_name)

    def get_measurecombinations(self):
        sql = str("SELECT * FROM MeasureCombinationQuery")
        self.crsr.execute(sql)
        # self.cnxn.commit()
        rows = self.crsr.fetchall()

        out = {}

        for row in rows:
            drow = dict(zip([r[0] for r in row.cursor_description], row))
            # print(drow)
            i = drow["mcid"]
            if i not in out:
                out[i] = [drow]
            else:
                out[i].append(drow)

        # REZOWA-106
        out2 = {}
        # for measurecombination 7 or 8, ensure 'O&T before T'
        for mc, comb in out.items():
            if comb[0]["MeasureCombinationType"] in [7, 8]:
                comb = sorted(comb, key=lambda x: x["Mechanism"], reverse=True)
            out2[mc] = comb

        return out2

    def get_table_entry(self, name, query):
        """Query database and return rows as a pandas Dataframe"""
        sql = str("SELECT * FROM " + name + " " + query)
        self.crsr.execute(sql)
        # self.cnxn.commit()
        rows = self.crsr.fetchall()
        columns = [d[0] for d in self.crsr.description]

        return DataFrame.from_records(rows, columns=columns)


def read_ini(fn_ini):
    defaults = {
        "Scenario": "Huidig",
        "Implementation degree single": "25",
        "Adaptation threshold": "-0.2",
        "Selection by implementation degree": "True",
        "Selection by implementation degree table": "False",
        "Filename implementation degree table": "impl_deg_table.csv",
        "Allowed measure combinations": "all",
        "HydroInput path": "./data/HydroInput",
        "Calculation path": "./calc",
        "Database path": "./data/maatregelen.accdb",  # REZOWA-164
        "Test": "False",
        "Discount rate": "0.01",
        "Startyear": "1980",
        "Endyear": "2010",
        "Use existing calculation": "False",
        "Limit buffer measures to high-value crops": "True",
        "AGRICOM exe": r"../bin/AGRICOM.exe",
        "Minimize memory usage": "False",
        "Write output maps": "True",
    }
    cfg = configparser.SafeConfigParser(
        defaults, strict=False, default_section="REGIOSCAN"
    )

    if not exists(fn_ini):
        logging.error("Initialisation file {} does not exists, exiting".format(fn_ini))
        exit(1)
    else:
        logging.info("Reading initialisation file {}".format(fn_ini))

    try:
        cfg.read(fn_ini)
    except configparser.NoSectionError:
        logging.error("Initialisation file does not contain [REGIOSCAN] header")
        exit(1)

    ini = {}
    ini["scenario"] = cfg["REGIOSCAN"].get("scenario")
    ini["impl_deg_single"] = cfg["REGIOSCAN"].getfloat("implementation degree single")
    ini["adaptation"] = cfg["REGIOSCAN"].getfloat("adaptation threshold")
    ini["select_by_impl"] = cfg["REGIOSCAN"].getboolean(
        "selection by implementation degree"
    )
    ini["select_by_impl_table"] = cfg["REGIOSCAN"].getboolean(
        "selection by implementation degree table"
    )
    ini["fn_impl_table"] = cfg["REGIOSCAN"].get("filename implementation degree table")
    allowed = cfg["REGIOSCAN"].get("allowed measure combinations")
    if allowed.lower() == "all":
        ini["allowed_mcs"] = range(100)
    else:
        ini["allowed_mcs"] = [float(a) for a in allowed.split(",")]
    ini["pth_hydroinput"] = cfg["REGIOSCAN"].get("hydroInput path")
    ini["pth_calculation"] = cfg["REGIOSCAN"].get("calculation path")
    ini["test"] = cfg["REGIOSCAN"].getboolean("test")
    ini["discountrate"] = cfg["REGIOSCAN"].getfloat("discount rate")
    ini["startyear"] = cfg["REGIOSCAN"].getint("startyear")
    ini["endyear"] = cfg["REGIOSCAN"].getint("endyear")
    ini["startdate"] = ini["startyear"] * 10000 + 101
    ini["enddate"] = ini["endyear"] * 10000 + 1231
    ini["use_existing"] = cfg["REGIOSCAN"].getboolean("use existing calculation")
    ini["use_highvalue"] = cfg["REGIOSCAN"].getboolean(
        "limit buffer measures to high-value crops"
    )
    ini["AGRICOM_exe"] = cfg["REGIOSCAN"].get("AGRICOM exe")
    ini["pth_database"] = cfg["REGIOSCAN"].get("Database path")
    ini["savemem"] = cfg["REGIOSCAN"].getboolean("minimize memory usage")
    ini["write_maps"] = cfg["REGIOSCAN"].getboolean("write output maps")

    # REZOWA-52
    if ini["select_by_impl"] and ini["select_by_impl_table"]:
        valid = True
        df = read_csv(ini["fn_impl_table"], index_col=False)
        if (
            "subarea" not in df
            or "companytype" not in df
            or "measurecombination" not in df
            or "impl_degree" not in df
        ):
            logging.error("Invalid implementation degree table: incorrect column names")
            valid = False

        # Validate: impl degrees per subarea / companytype may not exceed 100%
        summed = df.groupby(["subarea", "companytype"]).sum()
        if summed["impl_degree"].max() > 100.0:
            logging.error(
                "Invalid implementation degree table: implementation degrees per subarea/companytype exceeding 100%"
            )
            valid = False

        # TODO: all subareas / companytype combinations present in table
        # --> now: missing combinations are discarded in selection process
        # only possible after reading hydroinput...

        if valid:
            ini["impl_deg_table"] = df
        else:
            ini["impl_deg_table"] = None
    else:
        ini["impl_deg_table"] = None

    ini["years"] = list(range(ini["startyear"], ini["endyear"] + 1))

    return ini


def create_default_impl_deg_table(fn_impl_table, combinations, hi):
    df = hi.companies_df()
    subs = df["subarea"].unique()
    ctypes = df["companytype"].unique()
    mcs = [mcid for mcid in combinations.keys()]

    result = []
    for sub in subs:
        for ctype in ctypes:
            for mc in mcs:
                result.append([sub, ctype, mc, 0.0])

    result = DataFrame(
        data=result,
        columns=["subarea", "companytype", "measurecombination", "impl_degree"],
    )
    result.to_csv(fn_impl_table, index=False)


if __name__ == "__main__":
    mdb = MeasuresDB()
    for row in mdb.get_measurecombinations():
        print(row)
