#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# All custom exceptions go here


class InputDataError(Exception):
    """Raise when Input Data does not match."""

    pass
