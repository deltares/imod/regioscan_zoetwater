#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Bokeh dashboard to explore Regioscan Zoetwater results.
"""
import sys
from os.path import exists
import pandas as pd
import numpy as np
import imod
from pathlib import Path
from bokeh.io import curdoc
from bokeh.models.widgets import Tabs, Panel
from draw_map import map_tab
from draw_effects import effect_tab
import configparser
import argparse


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(description="Regioscan Zoetwatermaatregelen")
    parser.add_argument(
        "-i",
        "-ini",
        dest="fn_ini",
        help="filename initialisation file",
        action="store",
        default="bokeh.ini",
    )
    parser.add_argument(
        "-p",
        "--port",
        dest="port",
        help="port of bokeh app",
        action="store",
        default=5002,
    )
    return parser.parse_args(args)


def read_ini(fn_ini):
    defaults = {
        "Case": "CaseName",
        "Scenario": "Huidig",
        "Allowed measure combinations": "all",
        "HydroInput path": "./data/HydroInput",
        "Calculation path": "./calc",
        "Result csv": "./calc/Huidig/SuccessResultsRankedPerCompany.csv",
        "Company map": "./data/HydroInput/modelbedrijven.idf",
        "Database path": "./data/maatregelen.accdb",
        "Startyear": "1974",
        "Endyear": "2003",
        "Effect panel": "True",
        "Regioscan panel": "True",
        "Cumulative graphs": "False",
        "Percentile upper": 100,
        "Percentile lower": 0,
        "Temporary files": "False",
    }
    cfg = configparser.SafeConfigParser(
        defaults, strict=False, default_section="REGIOSCAN"
    )

    if not exists(fn_ini):
        raise IOError("Initialisation file {} does not exists, exiting".format(fn_ini))
    else:
        print("Reading initialisation file {}".format(fn_ini))

    try:
        cfg.read(fn_ini)
    except configparser.NoSectionError:
        raise ValueError("Initialisation file does not contain [REGIOSCAN] header")

    ini = {}
    ini["case"] = cfg["REGIOSCAN"].get("case")
    ini["scenario"] = cfg["REGIOSCAN"].get("scenario")
    allowed = cfg["REGIOSCAN"].get("allowed measure combinations")
    if allowed.lower() == "all":
        ini["allowed_mcs"] = range(100)
    else:
        ini["allowed_mcs"] = [float(a) for a in allowed.split(",")]
    ini["pth_hydroinput"] = cfg["REGIOSCAN"].get("hydroInput path")
    ini["pth_calculation"] = cfg["REGIOSCAN"].get("calculation path")
    ini["pth_database"] = cfg["REGIOSCAN"].get("database path")
    ini["result_csv"] = cfg["REGIOSCAN"].get("result csv")
    ini["company_idf"] = cfg["REGIOSCAN"].get("company map")
    ini["effect_panel"] = cfg["REGIOSCAN"].getboolean("effect panel")
    ini["regioscan_panel"] = cfg["REGIOSCAN"].getboolean("regioscan panel")
    ini["cum_graphs"] = cfg["REGIOSCAN"].getboolean("cumulative graphs")
    ini["startyear"] = cfg["REGIOSCAN"].getint("startyear")
    ini["endyear"] = cfg["REGIOSCAN"].getint("endyear")
    ini["pct_up"] = cfg["REGIOSCAN"].getint("percentile upper")
    ini["pct_low"] = cfg["REGIOSCAN"].getint("percentile lower")
    ini["temp_files"] = cfg["REGIOSCAN"].getboolean("temporary files")

    return ini


def get_results(ini):  # case='fase2', scenario="REF2017BP18"):
    bedrijfstypen = {
        "RAAM": {
            1: "Bedrijven met voer",
            2: "Akkerbouw",
            3: "Bedrijven met overige teelten",
            4: "Boomteelt-bedrijven",
            5: "Fruit-bedrijven",
            6: "Bollenbedrijven",
            7: "Voer akkerbouwbedrijven",
            8: "Overige bedrijven",
        },
        "HHNK": {
            1: "Bedrijven met voer",
            2: "Akkerbouw",
            3: "Bedrijven met overige teelten",
            4: "Bollenbedrijven",
            5: "Overig",
        },
        "fase2": {
            1: "Melkveehouderij",
            2: "Veehouderij",
            3: "Akkerbouw",
            4: "Gemengd (akkerbouw-ruwvoer)",
            5: "Boomkweek",
            6: "Fruit",
            7: "Bollen",
            8: "Overig",
            9: "Niet-grondgebonden",
        },
    }
    if ini["case"] not in bedrijfstypen:
        bedrijfstypen[ini["case"]] = bedrijfstypen["fase2"]

    # subarea;mc;CompanyNumber;companytype;NBC;groundwater;surfacewater;Qapp_gw_sw (m3);Toename_gw (m3/y);Toename_sw (m3/y);cost (E/y);benefit (E/y);benefit cropdamage (E/y);benefit sprinkling (E/y);rank;Area_ha

    # Read data into dataframes
    results = pd.read_csv(ini["result_csv"], sep=";")

    results["Afname (m3/y)"] = (
        -results["Toename_gw (m3/y)"] - results["Toename_sw (m3/y)"]
    )
    results["Afname gw (m3/y)"] = -results["Toename_gw (m3/y)"]
    results["Afname ow (m3/y)"] = -results["Toename_sw (m3/y)"]
    results["cost (E/ha/y)"] = results["cost (E/y)"] / results["Area_ha"]
    results["benefit (E/ha/y)"] = results["benefit (E/y)"] / results["Area_ha"]
    results["benefit cropdamage (E/ha/y)"] = (
        results["benefit cropdamage (E/y)"] / results["Area_ha"]
    )
    results["benefit sprinkling (E/ha/y)"] = (
        results["benefit sprinkling (E/y)"] / results["Area_ha"]
    )
    results["Invest (E/y)"] = results["cost (E/y)"] - results["benefit (E/y)"]
    results["Invest (E/m3)"] = (
        results["Invest (E/y)"] / np.fmax(0.0, results["Afname (m3/y)"])
    ).fillna(10.0)
    results["Invest (E/m3)"].loc[results["Invest (E/m3)"] < 0.0] = 0.0
    results["Invest (E/m3)"].loc[results["Invest (E/m3)"] > 5.0] = 5.0
    results["Bedrijfstype"] = results["companytype"].apply(
        lambda x: bedrijfstypen[ini["case"]][x]
    )
    results = results.rename(columns={"rank": "origrank"})

    # datacells = int((~compmap.isnull()).astype(int).sum())
    results["Afname (mm/y)"] = results["Afname (m3/y)"] / results["Area_ha"] / 10.0
    results["Afname gw (mm/y)"] = (
        results["Afname gw (m3/y)"] / results["Area_ha"] / 10.0
    )
    results["Afname ow (mm/y)"] = (
        results["Afname ow (m3/y)"] / results["Area_ha"] / 10.0
    )
    results["surfacewater (mm/y)"] = results["surfacewater"] / results["Area_ha"] / 10.0
    results["groundwater (mm/y)"] = results["groundwater"] / results["Area_ha"] / 10.0

    # which measures?
    if ini["case"] in ["HHNK", "RAAM"]:
        mctitles = [
            "ASR zoet met dripirrigatie (1)",
            "Freshmaker met dripirrigatie (2)",
            "ASR zoet met reguliere beregening (3)",
            "Freshmaker met reguliere beregening (4)",
            "ASR zout met dripirrigatie (5)",
            "ASR zout met reguliere beregening (6)",
            "Kreekruginfiltratie met reguliere beregening (7)",
            "Kreekruginfiltratie met dripirrigatie (8)",
            "Traditioneel drainagesysteem (10)",
            "Regelbare drainage (11,12)",
            "Regelbare drainage (11,12)",
            "Regelbare drainage met subinfiltratie (13,14)",
            "Regelbare drainage met subinfiltratie (13,14)",
            "Reguliere beregening grondwater (15)",
            "Reguliere beregening oppervlaktewater (16)",
            "Drains2buffer (17,18)",
            "Drains2buffer (17,18)",
            "Systeemgerichte drainage (19,20)",
            "Systeemgerichte drainage (19,20)",
            "Spaarwater lokale opslag van perceelseigen water gecombineerd met subinfiltratie (21,22)",
            "Spaarwater lokale opslag van perceelseigen water gecombineerd met subinfiltratie (21,22)",
            "Druppelirrigatie aan de oppervlakte (23)",
            "Druppelirrigatie onder de ploegzool aangebracht (24)",
            "Regelbare drainage, met reguliere beregening voor resterend tekort (25,26)",
            "Regelbare drainage, met reguliere beregening voor resterend tekort (25,26)",
            "Regelbare drainage, met dripirrigatie oppervlakte voor resterend tekort (27,28)",
            "Regelbare drainage, met dripirrigatie oppervlakte voor resterend tekort (27,28)",
        ]
        mcids = [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
        ]
        mctitles = pd.Series(index=mcids, data=mctitles).sort_values()
        mcstrip = mctitles.apply(lambda x: x[: x.find("(") - 1])
    else:
        # strip salt measures
        # allowed = [11,13,15,16,17,19,21,22,23,24,25,27,29,30,31,32,33,34,35,36,37] #1,3,10
        allowed = ini["allowed_mcs"]
        results = results.loc[results["mc"].isin(allowed)]
        results = results.drop(columns=[c for c in results.columns if "Unnamed" in c])

        # filter out mc's that are only present at < 5% of companies
        nmcs = results.groupby("mc")["CompanyNumber"].count() / len(
            results["CompanyNumber"].unique()
        )
        nmcs = nmcs.loc[nmcs > 0.05].index.values
        results = results.loc[results["mc"].isin(nmcs)]

        mctitles = {
            1: "ASR zoet en druppelirrigatie",
            2: "Freshmaker en druppelirrigatie",
            3: "ASR zoet en regulier",
            4: "Freshmaker en regulier",
            5: "ASR zout en druppelirrigatie",
            6: "ASR zout en regulier",
            7: "Kreekruginfiltratie regulier",
            8: "Kreekruginfiltratie druppelirrigatie",
            10: "Conventionele drainage",
            11: "Regelbare drainage",
            13: "Regelbare drainage subinfiltratie",
            15: "Reguliere beregening grondwater",
            16: "Reguliere beregening oppervlaktewater",
            17: "Drains2buffer",
            19: "Systeemgerichte drainage",
            21: "Spaarwater lokale opslag zandperceel",
            22: "Spaarwater lokale opslag kleiperceel",
            23: "Druppelirrigatie oppervlakte",
            24: "Druppelirrigatie verdiept",
            25: "Regelbare drainage met reguliere beregening",
            27: "Regelbare drainage met druppelirrigatie",
            29: "Bodemverbeteringsmaatregelen",
            30: "Bodemverbetering en beregening",
            31: "Bodemverbetering en druppelirrigatie",
            32: "Perceelstuw",
            33: "Perceelstuw en beregening",
            34: "Perceelstuw en druppelirrigatie",
            35: "Slootbodemverhoging",
            36: "Slootbodemverhoging en beregening",
            37: "Slootbodemverhoging en druppelirrigatie",
            38: "Bodemverbetering door toediening organische stof",
            39: "Organische stof met reguliere beregening",
            40: "Organische stof met druppelirrigatie",
            41: "Bodemverbetering door opheffen verdichting",
            42: "Verdichting opheffen met reguliere beregening",
            43: "Verdichting opheffen met druppelirrigatie",
        }
        mctitles = {k: v for k, v in mctitles.items() if k in results["mc"].unique()}
        mcids = sorted(mctitles.keys())
        mctitles = pd.Series(mctitles)

    return results, mctitles


########################################
args = parse_args(sys.argv[1:])

# read ini file
ini = read_ini(args.fn_ini)

# get results for case
results, mctitles = get_results(ini)

# Create each of the tabs
if ini["regioscan_panel"]:
    tab1 = map_tab(results, mctitles, ini)
    root = tab1
if ini["effect_panel"]:
    tab2 = effect_tab(results, mctitles, ini)
    root = tab2
if ini["regioscan_panel"] and ini["effect_panel"]:
    root = Tabs(
        tabs=[
            Panel(child=tab1, title="Regioscan interactieve GUI"),
            Panel(child=tab2, title="Maatregel effecten"),
        ]
    )

# Put the tabs in the current document for display
curdoc().title = f"{ini['case']}, {ini['scenario']}"
curdoc().add_root(root)
