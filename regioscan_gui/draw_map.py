# pandas and numpy for data manipulation
import pandas as pd
import numpy as np
import colorcet as cc
import imod
from math import log10, floor
from bokeh.plotting import figure
from bokeh.models import (
    HoverTool,
    BoxZoomTool,
    Div,
    Span,
    Panel,
    ColorBar,
    ColumnDataSource,
    LinearAxis,
    DataRange1d,
)
from bokeh.models.widgets import (
    CheckboxGroup,
    Slider,
    RangeSlider,
    Tabs,
    CheckboxButtonGroup,
    Button,
    TableColumn,
    DataTable,
    Select,
    Toggle,
    RadioGroup,
)
from bokeh.layouts import column, row, gridplot, widgetbox
from bokeh.palettes import Category20_16, Category20_20, RdBu
from bokeh.models.mappers import LinearColorMapper
from bokeh.tile_providers import get_provider, Vendors
from rasterio._err import CPLE_AppDefinedError

# from main import get_results
pd.options.mode.chained_assignment = None  # default='warn'

orgprm = "NBC"
sortby = "Netto Baten/Kosten"  #'Netto Kosten per m3'#
impldeg = 100.0
m3mm = "mm"

prm_translate = {
    "companytype": {
        1: "Melkveehouderij",
        2: "Veehouderij",
        3: "Akkerbouw",
        4: "Gemengd (akkerbouw-ruwvoer)",
        5: "Boomkweek",
        6: "Fruit",
        7: "Bollen",
        8: "Overig",
        9: "Niet-grondgebonden",
    },
    "mc": {
        1: "ASR zoet en druppelirrigatie",
        2: "Freshmaker en druppelirrigatie",
        3: "ASR zoet en regulier",
        4: "Freshmaker en regulier",
        5: "ASR zout en druppelirrigatie",
        6: "ASR zout en regulier",
        7: "Kreekruginfiltratie regulier",
        8: "Kreekruginfiltratie druppelirrigatie",
        10: "Conventionele drainage",
        11: "Regelbare drainage",
        13: "Regelbare drainage subinfiltratie",
        15: "Reguliere beregening grondwater",
        16: "Reguliere beregening oppervlaktewater",
        17: "Drains2buffer",
        19: "Systeemgerichte drainage",
        21: "Spaarwater lokale opslag zandperceel",
        22: "Spaarwater lokale opslag kleiperceel",
        23: "Druppelirrigatie oppervlakte",
        24: "Druppelirrigatie verdiept",
        25: "Regelbare drainage met reguliere beregening",
        27: "Regelbare drainage met druppelirrigatie",
        29: "Bodemverbeteringsmaatregelen",
        30: "Bodemverbetering en beregening",
        31: "Bodemverbetering en druppelirrigatie",
        32: "Perceelstuw",
        33: "Perceelstuw en beregening",
        34: "Perceelstuw en druppelirrigatie",
        35: "Slootbodemverhoging",
        36: "Slootbodemverhoging en beregening",
        37: "Slootbodemverhoging en druppelirrigatie",
    },
}

resultprm_translate = {
    "Maatregelcombinatie": "mc",
    "Modelbedrijven": "CompanyNumber",
    "Bedrijfstypen": "companytype",
    "Deelgebieden": "subarea",
    "Netto baten-kosten ratio": "NBC",
    "Kosten (E/ha/j)": "cost (E/ha/y)",
    "Baten totaal (E/ha/j)": "benefit (E/ha/y)",
    "Baten gewasopbrengst (E/ha/j)": "benefit cropdamage (E/ha/y)",
    "Baten vermeden beregening (E/ha/j)": "benefit sprinkling (E/ha/y)",
    "Watervraag oppervlaktewater (mm/j)": "surfacewater (mm/y)",
    "Watervraag grondwater (mm/j)": "groundwater (mm/y)",
    "Afname watervraag totaal (mm/j)": "Afname (m3/y)",
    "Afname watervraag oppervlaktewater (mm/j)": "Afname ow (mm/y)",
    "Afname watervraag grondwater (mm/j)": "Afname gw (mm/y)",
    "Kosten (E/j)": "cost (E/y)",
    "Baten totaal (E/j)": "benefit (E/y)",
    "Baten gewasopbrengst (E/j)": "benefit cropdamage (E/y)",
    "Baten vermeden beregening (E/j)": "benefit sprinkling (E/y)",
    "Watervraag oppervlaktewater (m3/j)": "surfacewater",
    "Watervraag grondwater (m3/j)": "groundwater",
    "Afname watervraag totaal (m3/j)": "Afname (m3/y)",
    "Afname watervraag oppervlaktewater (m3/j)": "Afname ow (m3/y)",
    "Afname watervraag grondwater (m3/j)": "Afname gw (m3/y)",
    "Neveneffecten waterkwaliteit": "SEwaterkwaliteit",
    "Neveneffecten uitspoeling N": "SEuitspoelingN",
    "Neveneffecten uitspoeling P": "SEuitspoelingP",
    "Neveneffecten piekafvoeren": "SEpiekafvoer",
    "Neveneffecten verdroging": "SEverdroging",
    "Neveneffecten bodemdaling": "SEbodemdaling",
}
resultprm_reverse = {v: k for k, v in resultprm_translate.items()}


def to_mercator(da):
    """Function to project a dataarray to WebMercator projection"""
    da.load()
    da.values = np.flipud(da.values)
    return imod.prepare.reproject(
        da, src_crs={"init": "epsg:28992"}, dst_crs={"init": "epsg:3857"}
    )


def create_palette(low, high):
    rgb = np.random.random((int(high - low), 3)) * 255.0
    return [f"#{r:02x}{g:02x}{b:02x}" for r, g, b in rgb.astype(int)]


palettes = {
    "Bedrijfstype": [
        1,
        9,
        tuple(Category20_20[i] for i in (17, 11, 3, 10, 4, 12, 6, 0, 14)),
    ],
}
palettes["companytype"] = palettes["Bedrijfstype"]


def map_tab(results, mctitles, ini):
    mcstrip = mctitles.apply(lambda x: x[: x.find("(") - 1])

    tot_ha = results.groupby(["CompanyNumber"]).nth(0)["Area_ha"].sum()
    # print(tot_ha, len(results.groupby(['CompanyNumber']).nth(0)))

    # load and project compmap
    compmaporg = imod.idf.open(ini["company_idf"]).load()
    compmaporg = to_mercator(compmaporg)

    # make palettes:
    for prm in ["mc", "subarea", "CompanyNumber"]:
        low = results[prm].min()
        high = results[prm].max()
        palettes[prm] = [low, high, create_palette(low, high)]

    def make_dataset(
        results,
        mc_list=None,
        impldegree=impldeg / 100.0,
        prm=orgprm,
        sortby=sortby,
        allow_increase=True,
        temp_files=False
    ):
        """Function to make a dataset for the map based on Regioscan result"""

        if mc_list is None:
            mc_list = results["mc"].unique()
        elif isinstance(mc_list[0], str):
            if "(" not in mctitles.iloc[0]:
                mc_list = list(map(lambda x: x[: x.find("(") - 1], mc_list))
            mc_list = mctitles2.loc[mctitles.isin(mc_list)].index.tolist()

        # Sorteervolgorde   ['Netto Baten/Kosten','Afname watervraag', 'Netto Kosten per m3']
        if sortby == "Afname watervraag":
            sortby = ["Afname (m3/y)", "NBC"]
            sortasc = [False, False]
        elif sortby == "Baten gewasverdamping":
            sortby = ["benefit cropdamage (E/ha/y)", "Afname (m3/y)"]
            sortasc = [False, False]
        elif sortby == "Netto Kosten per m3":
            sortby = ["Invest (E/m3)", "Afname (m3/y)"]
            sortasc = [True, False]
        else:  # sortby == 'Netto Baten/Kosten':
            sortby = ["NBC", "Afname (m3/y)"]
            sortasc = [False, False]

        # Subset to the mc's in the specified list
        subset = results.loc[results["mc"].isin(mc_list)]

        # If increase of water demand is not allowed: exclude measures that increase water demand
        # *excluding the application measures 15,16,23,24
        if not allow_increase:
            # subset = subset.loc[(subset["mc"].isin([15,16,23,24])|(subset["Afname (m3/y)"]>=0))]
            subset = subset.loc[subset["Afname (m3/y)"] >= 0]

        # Neem beste maatregel per company
        subset = subset.sort_values(
            by=["CompanyNumber"] + sortby, ascending=[True] + sortasc
        )  # 10ms

        # grp = subset.groupby(['CompanyNumber']).first()  #1s??
        grp = subset.groupby(["CompanyNumber"]).nth(
            0
        )  # Appears to be a bug in Pandas, caused by the text columns (see https://github.com/pandas-dev/pandas/issues/19283)
        grp = grp.sort_values(by=sortby, ascending=sortasc)
        grp["CompanyNumber"] = grp.index
        grp.index.name = None
        grp["MeasureName"] = grp["mc"].map(prm_translate["mc"])
        # Rank binnen subarea, bepaal implementatiegraad
        # NB: voor figuren gaat dit niet zo goed: verwisselingen in de sortering omdat impldeg wordt beinvloed door aantal bedrijven per deelgebied...
        # OF: loslaten ranking per deelgebied, en over hele gebied ranken  (en daarnaast optie om op deelgebied-niveau te werken)
        # n_subarea = grp.groupby(['subarea']).count()['mc']
        # n_subarea = grp['subarea'].map(n_subarea)
        # grp['rank_'] = grp.groupby(['subarea']).rank(method='first',ascending=False)['CompanyNumber']
        # grp['impldeg'] = grp['rank_'] / n_subarea * 100.
        grp["rank_"] = grp[sortby[0]].rank(method="first", ascending=sortasc[0])
        grp["impldeg"] = grp["rank_"] / len(grp) * 100.0

        # sorteer op implementatiegraad
        grp = grp.sort_values(by=["impldeg"], ascending=[True])

        # Create data for curves of cost, benefit, NBC, watervraag, side-effects
        for col in [
            "Afname (m3/y)",
            "Afname gw (m3/y)",
            "Afname ow (m3/y)",
            "Invest (E/y)",
            "SEwaterkwaliteit",
            "SEpiekafvoer",
        ]:  # ,'SEuitspoelingN','SEuitspoelingP']:
            grp[col + " cum"] = grp[col].cumsum()  # gaat zo goed met subareas?
        for col in ["Afname", "Afname gw", "Afname ow"]:
            grp[col + " (mm/y) cum"] = grp[col + " (m3/y) cum"] / tot_ha / 10.0

        # recalc invest cumsum to start from minimum (if negative)
        grp["Invest (E/y) cum"] = (
            min(0.0, grp["Invest (E/y) cum"].min()) + grp["Invest (E/y)"].abs().cumsum()
        )

        # grp['Investm3Cum'] = np.fmax(0,grp['InvestCum'] / grp['AfnameCum'])
        grp["Invest (E/m3) cum"] = grp["Invest (E/y) cum"] / np.fmax(
            0, grp["Afname (m3/y) cum"]
        )
        if sortby[0] == "Invest (m3/y) cum":
            grp["Invest (E/m3) cum"].loc[grp["impldeg"] < 2.0] = grp[
                "Invest (E/m3)"
            ].loc[
                grp["impldeg"] < 1.0
            ]  # to avoid weird values when both AfnameCum and InvestCum are small
        grp["Invest (E/m3) cum"] = (
            grp["Invest (E/m3) cum"].rolling(5, center=True).mean()
        )
        grp["Invest (E/y) cum"] /= 1000.0  # to kE/j
        grp = grp.rename(columns={"Invest (E/y) cum":"Invest (kE/y) cum"})

        # Select top impldegree % of measures
        # cutoff = (impldegree*n_subarea+0.5).astype(int)
        # grpsel = grp.loc[grp['rank_']<=grp['subarea'].map(cutoff)]
        grpsel = grp.loc[grp["impldeg"] <= (impldegree * 100.0)]

        # map values onto compmap
        compmap = compmaporg.copy()
        attrs = {}
        (
            attrs["dx"],
            attrs["xmin"],
            attrs["xmax"],
            attrs["dy"],
            attrs["ymin"],
            attrs["ymax"],
        ) = imod.util.spatial_reference(compmap)
        compmap.values = np.vectorize(grpsel[prm].to_dict().get)(compmap.values)
        attrs["prm"] = prm
        attrs["title"] = resultprm_reverse[prm]
        attrs["nominal"] = False

        # array of nominal description if necessary
        cds_values = compmap.values
        if prm in prm_translate:
            translator = prm_translate[prm]
            translator[np.nan] = ""
            cds_values = cds_values.astype(object)
            cds_values = np.vectorize(translator.get)(cds_values)
            attrs["nominal"] = True

        # bin number of mc's per bedrijfstype

        attrs["mcs"] = grp["MeasureName"].unique().tolist() + ["Geen maatregel"]
        attrs["bt"] = grp["Bedrijfstype"].unique().tolist()
        n_bt = grp.groupby("Bedrijfstype").count()["CompanyNumber"]
        comptypemc_all = pd.DataFrame(
            index=pd.Index(data=attrs["bt"], name="Bedrijfstype"),
            columns=attrs["mcs"],
            data=0,
        )
        comptypemc = (
            grpsel.groupby(["Bedrijfstype", "MeasureName"])
            .count()["CompanyNumber"]
            .unstack()
            .fillna(0)
        )
        comptypemc_all.update(comptypemc)
        comptypemc_all["Geen maatregel"] = n_bt - comptypemc_all[attrs["mcs"]].sum(
            axis=1
        )

        # output temporary files?
        if temp_files:
            try:
                imod.rasterio.write("temp_map.tif", compmap.astype(float))
                grp.to_csv("temp_all.csv")
                grpsel.to_csv("temp_selected.csv")
                comptypemc_all.to_csv("temp_comptypes.csv")
            except (PermissionError, CPLE_AppDefinedError):
                print("Could not write temporary files, probably in use")

        # to ColumnDataSource
        cds1 = ColumnDataSource({"image": [compmap.values], "values": [cds_values]})
        cds2 = ColumnDataSource(grp)
        cds3 = ColumnDataSource(grpsel)
        cds4 = ColumnDataSource(comptypemc_all.reset_index())
        return cds1, cds2, cds3, cds4, attrs

    def make_plot(src, attrs):
        # Create the plot with no axes or grid
        buf = 0.0  # 2000.
        p = figure(
            plot_width=700,
            plot_height=700,
            title="Regioscan Zoetwatermaatregelen - " + attrs["title"],
            x_axis_type=None,
            y_axis_type=None,
            match_aspect=True,
            aspect_ratio=1,
            aspect_scale=1,
            x_range=(attrs["xmin"] - buf, attrs["xmax"] + buf),
            y_range=(attrs["ymin"] - buf, attrs["ymax"] + buf),
            tools=["pan", "wheel_zoom", "save", "reset"],
            toolbar_location="right",
        )
        # p.xaxis.visible = False
        # p.yaxis.visible = False
        p.grid.visible = False
        p.add_tools(
            HoverTool(tooltips=[("value", "@values")]), BoxZoomTool(match_aspect=True)
        )
        # p.add_tools(HoverTool(tooltips=[('value', "@image{0.00}")]))

        if attrs["prm"] == "NBC":
            cmap = LinearColorMapper(
                palette=cc.b_diverging_gwr_55_95_c38[::-1], low=-1, high=1
            )
        else:
            cmap = LinearColorMapper(
                palette=cc.b_diverging_gwr_55_95_c38[::-1]
            )  # ,low=-1, high=1)

        p.add_tile(get_provider(Vendors.CARTODBPOSITRON))
        img = p.image(
            image="image",
            dh=attrs["ymax"] - attrs["ymin"],
            dw=attrs["xmax"] - attrs["xmin"],
            x=attrs["xmin"],
            y=attrs["ymin"],
            global_alpha=0.5,
            color_mapper=cmap,
            dh_units="data",
            dw_units="data",
            source=src,
        )
        img.glyph.color_mapper.nan_color = (0, 0, 0, 0)
        color_bar = ColorBar(
            color_mapper=cmap, label_standoff=8, border_line_color=None, location=(0, 0)
        )

        p.add_layout(color_bar, "left")
        return p

    def make_plot2(src2, src3):

        p2 = figure(
            plot_width=400,
            plot_height=300,
            title=f"Afname watervraag ({m3mm})",
            tools="pan,wheel_zoom,box_zoom,reset",
            x_range=[0, 100],
            toolbar_location="right",
        )
        hover = HoverTool(
            tooltips=[
                ("Impl.graad", "@impldeg{0.0} %"),
                ("Afname totaal", f"@{{Afname ({m3mm}/y) cum}} ({m3mm}/y)"),
                ("Afname opp.w.", f"@{{Afname ow ({m3mm}/y) cum}} ({m3mm}/y)"),
                ("Afname grondw.", f"@{{Afname gw ({m3mm}/y) cum}} ({m3mm}/y)"),
            ]
        )
        p2.xaxis.axis_label = "Implementatiegraad (%)"
        p2.yaxis.axis_label = f"Afname watervraag ({m3mm})"
        p2.add_tools(hover)

        # entire range
        p2.line(
            x="impldeg",
            y=f"Afname ({m3mm}/y) cum",
            line_color="black",
            line_width=2,
            line_dash="dashed",
            legend_label="Afname totaal",
            source=src2,
        )
        p2.line(
            x="impldeg",
            y=f"Afname gw ({m3mm}/y) cum",
            line_color="green",
            line_width=2,
            line_dash="dashed",
            legend_label="Grondwater",
            source=src2,
        )
        p2.line(
            x="impldeg",
            y=f"Afname ow ({m3mm}/y) cum",
            line_color="blue",
            line_width=2,
            line_dash="dashed",
            legend_label="Oppervlaktewater",
            source=src2,
        )
        # selected
        p2.line(
            x="impldeg",
            y=f"Afname ({m3mm}/y) cum",
            line_color="black",
            line_width=3,
            line_dash="solid",
            legend_label="Afname totaal",
            source=src3,
        )
        p2.line(
            x="impldeg",
            y=f"Afname gw ({m3mm}/y) cum",
            line_color="green",
            line_width=3,
            line_dash="solid",
            legend_label="Grondwater",
            source=src3,
        )
        p2.line(
            x="impldeg",
            y=f"Afname ow ({m3mm}/y) cum",
            line_color="blue",
            line_width=3,
            line_dash="solid",
            legend_label="Oppervlaktewater",
            source=src3,
        )

        p2.xaxis.axis_label = "Implementatiegraad"
        p2.legend.location = "bottom_right"
        p2.legend.click_policy = "hide"
        return p2

    def make_plot3(src2, src3):
        p3 = figure(
            plot_width=400,
            plot_height=300,
            title="Cumulatief baten deficiet (kosten - baten)",
            tools="pan,wheel_zoom,box_zoom,reset",
            x_range=p2.x_range,
            toolbar_location="right",
        )

        hover = HoverTool(
            tooltips=[
                ("Impl.graad", "@impldeg{0.0} %"),
                ("Baten deficiet", "@{Invest (kE/y) cum}{0.0} (kE/y)"),
                ("Baten def. per m3", "@{Invest (E/m3) cum}{0.0} (E/m3)"),
            ]
        )
        p3.xaxis.axis_label = "Implementatiegraad (%)"
        p3.yaxis.axis_label = "Cumulatief baten deficiet (kE/y)"
        p3.add_tools(hover)
        # Setting the second y axis range name and range
        dr = DataRange1d()
        p3.extra_y_ranges = {"perm3": dr}

        # Adding the second axis to the plot.
        p3.add_layout(
            LinearAxis(y_range_name="perm3", axis_label="Baten def. per m3 (E/m3)"),
            "right",
        )

        # Entire range
        p3.line(
            x="impldeg",
            y="Invest (kE/y) cum",
            line_color="blue",
            line_width=2,
            line_dash="dashed",
            legend_label="Baten def.",
            source=src2,
        )
        p3s = p3.line(
            x="impldeg",
            y="Invest (E/m3) cum",
            line_color="green",
            line_width=2,
            line_dash="dashed",
            legend_label="Baten def. per m3",
            y_range_name="perm3",
            source=src2,
        )
        # only selected
        p3.line(
            x="impldeg",
            y="Invest (kE/y) cum",
            line_color="blue",
            line_width=3,
            line_dash="solid",
            legend_label="Baten def.",
            source=src3,
        )
        p3s2 = p3.line(
            x="impldeg",
            y="Invest (E/m3) cum",
            line_color="green",
            line_width=3,
            line_dash="solid",
            legend_label="Baten def. per m3",
            y_range_name="perm3",
            source=src3,
        )

        dr.renderers = [p3s, p3s2]  # to let the 2nd y axis only listen to p3s
        p3.xaxis.axis_label = "Implementatiegraad"
        p3.legend.location = "bottom_right"
        p3.legend.click_policy = "hide"
        return p3

    def make_plot4(src4, attrs):
        # df = src4.to_df()
        # Plot of bedrijfstypen that implement a certain measure
        p4 = figure(
            title="Maatregel per bedrijfstype",
            plot_width=700,
            plot_height=200,
            y_range=attrs["bt"],
            tools="pan,wheel_zoom,box_zoom,reset",
        )

        # col = [cc.b_rainbow_bgyrm_35_85_c71[i//256] for i in range(len(attrs['mcs']))]
        col = [
            cc.b_rainbow_bgyrm_35_85_c71[int(255 * i / (len(attrs["mcs"]) - 2))]
            for i in range(len(attrs["mcs"]) - 1)
        ] + ["#aaaaaa"]
        renderers = p4.hbar_stack(
            attrs["mcs"],
            y="Bedrijfstype",
            height=0.9,
            color=col,
            name=attrs["mcs"],
            source=src4,
        )
        # legend=attrs['mcs'],source=src4)#,color=cc.b_rainbow_bgyrm_35_85_c71)
        for r in renderers:
            mc = r.name
            hover = HoverTool(tooltips=[("%s" % mc, "@{%s}" % mc)], renderers=[r])
            p4.add_tools(hover)

        return p4

    def update(attr, old, new):
        global orgprm, impldeg

        # which param?
        prm = resultprm_translate[prmselect.value]
        sortby = sortbyselect.value
        allow = not bool(allowincrease.active)

        # which mc's?
        mclist = [mcselection.labels[i] for i in mcselection.active]

        # redo analysis
        newarr, newdf, newdfsel, newctype, newattrs = make_dataset(
            results,
            mc_list=mclist,
            prm=prm,
            impldegree=impldegselect.value / 100.0,
            sortby=sortby,
            allow_increase=allow,
            temp_files=ini["temp_files"]
        )
        # update column data source
        src.data.update(newarr.data)
        src2.data.update(newdf.data)
        src3.data.update(newdfsel.data)
        src4.data.update(newctype.data)
        p4 = make_plot4(src4, attrs)

        # change colorbar to type of data
        if prm != orgprm:
            p.title.text = "Regioscan Zoetwatermaatregelen - " + resultprm_reverse[prm]

            cm = p.select_one(LinearColorMapper)
            cm.update(palette=cc.b_diverging_gwr_55_95_c38[::-1])
            max_ = max(abs(results[prm].quantile(0.1)), abs(results[prm].quantile(0.9)))
            order = floor(log10(max_))
            maxrnd = floor(max_ / 10 ** order) * 10 ** order
            # print(max_,maxrnd)
            cm.update(low=-maxrnd, high=maxrnd)
            if prm == "NBC":
                cm.update(low=-1.0, high=1.0)
            elif prm in [
                "benefit (E/ha/y)",
                "benefit cropdamage (E/ha/y)",
                "benefit sprinkling (E/ha/y)",
                "Afname (m3/y)",
            ]:
                cm.update(low=-100.0, high=100.0)
            elif prm in [
                "cost (E/ha/y)",
                "cost (E/y)",
                "Invest (E/y)",
                "Invest (E/m3)",
            ]:
                cm.update(palette=cc.b_diverging_gwr_55_95_c38)  # low=-500., high=500.,
            elif prm in [
                "SEwaterkwaliteit",
                "SEuitspoelingN",
                "SEuitspoelingP",
                "SEpiekafvoer",
                "SEverdroging",
                "SEbodemdaling",
            ]:
                cm.update(low=-2, high=2)
            elif prm in [
                "mc",
                "companytype",
                "Bedrijfstype",
                # "rank",
                "subarea",
                "CompanyNumber",
            ]:
                cm.update(
                    low=palettes[prm][0],
                    high=palettes[prm][1],
                    palette=palettes[prm][2],
                )  # cc.b_rainbow_bgyrm_35_85_c71)

            orgprm = prm

    # get available measure combinations
    mctitles2 = mctitles.reindex(index=sorted(results["mc"].unique()))
    if "(" not in mctitles2.iloc[0]:
        # add mc id to mctitles
        mclist = list(map(lambda x, y: x + f" ({y})", mctitles2, mctitles2.index))
    else:
        mclist = np.unique(mctitles2.values).tolist()

    # CheckboxGroup to select mc's for plotting
    mcselection = CheckboxGroup(
        labels=mclist, active=list(range(len(mclist)))
    )  # title='Selecteer maatregelen',
    mcselection.on_change("active", update)

    def updateall():
        mcselection.active = list(range(len(mclist)))
        update(None, None, None)

    def updatenone():
        mcselection.active = [0]
        update(None, None, None)

    selall = Button(label="Selecteer alle", width=100)
    selall.on_click(updateall)
    selnone = Button(label="Geen", width=100)
    selnone.on_click(updatenone)

    # Selection of parameter to plot
    prmselect = Select(
        title="Laat zien:",
        value=resultprm_reverse[orgprm],
        options=list(resultprm_translate.keys()),
    )
    prmselect.on_change("value", update)

    # Selection of sortby
    sortbyselect = Select(
        title="Neem maatregelen op basis van:",
        value=sortby,
        options=[
            "Netto Baten/Kosten",
            "Afname watervraag",
            "Netto Kosten per m3",
            "Baten gewasverdamping",
        ],
    )
    sortbyselect.on_change("value", update)

    # implementation degree
    impldegselect = Slider(
        start=1,
        end=100,
        step=1,
        value=impldeg,  # _throttled
        title="Implementatiegraad (%)",
    )
    impldegselect.on_change("value", update)

    # allow increase of water demand
    # allowincrease = Toggle(label="Allow increase water demand", active=True)
    def updateonclick(attr):
        update(attr, None, None)

    allowincrease = RadioGroup(
        margin=(20, 0, 20, 20),
        labels=["Sta toename watervraag toe", "Sta toename watervraag niet toe"],
        active=0,
    )
    allowincrease.on_click(updateonclick)

    # Initial source and plot
    src, src2, src3, src4, attrs = make_dataset(results, prm=orgprm, sortby=sortby, temp_files=ini["temp_files"])
    p = make_plot(src, attrs)
    if ini["cum_graphs"]:
        p2 = make_plot2(src2, src3)
        p3 = make_plot3(src2, src3)
    p4 = make_plot4(src4, attrs)

    # Layout setup
    if ini["cum_graphs"]:
        layout = column(
            row([impldegselect, prmselect, sortbyselect, allowincrease]),
            row(
                [
                    column(
                        [
                            Div(text="<b>Selecteer maatregelcombinaties</b>"),
                            mcselection,
                            selall,
                            selnone,
                        ]
                    ),
                    p,
                    gridplot([[p2], [p3]]),
                ]
            ),
            row([p4]),
            sizing_mode="scale_both",
        )
    else:
        layout = column(
            row([impldegselect, prmselect, sortbyselect, allowincrease]),
            row(
                [
                    column(
                        [
                            Div(text="<b>Selecteer maatregelcombinaties</b>"),
                            mcselection,
                            selall,
                            selnone,
                        ]
                    ),
                    p,
                ]
            ),
            row([p4]),
            sizing_mode="scale_both",
        )

    return layout
