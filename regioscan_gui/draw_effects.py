"""
Display for calculated effects of Regioscan measures
Interactively show effects of measures

GUI:
On the left:
- Drop-down box with all active measures
- Drop-down box with parameter to show:
  - Tact before measure
  - Tact after measure
  - Tact increase / decrease
  - Tact increase / decrease %
  - Droughtdamage before measure
  - Droughtdamage after measure
  - Droughtdamage increase / decrease
  - Droughtdamage increase / decrease %
  - Saltdamage before measure
  - Saltdamage after measure
  - Saltdamage increase / decrease
  - Saltdamage increase / decrease %
- Drop-down box with years and average

On the right:
- map
"""
# pandas and numpy for data manipulation
import pandas as pd
import numpy as np
import colorcet as cc
import imod
from math import log10, floor
from bokeh.plotting import figure
from bokeh.models import (
    HoverTool,
    BoxZoomTool,
    Div,
    Span,
    Panel,
    ColorBar,
    ColumnDataSource,
    LinearAxis,
    DataRange1d,
)
from bokeh.models.widgets import (
    CheckboxGroup,
    Slider,
    RangeSlider,
    Tabs,
    CheckboxButtonGroup,
    Button,
    TableColumn,
    DataTable,
    Select,
)
from bokeh.layouts import column, row, gridplot, widgetbox
from bokeh.palettes import Category20_20, RdBu
from bokeh.models.mappers import LinearColorMapper, CategoricalColorMapper
from bokeh.tile_providers import get_provider, Vendors
import re
import xarray as xr

# from main import get_results
pd.options.mode.chained_assignment = None  # default='warn'


def to_mercator(da):
    """Function to project a dataarray to WebMercator projection"""
    da.load()
    da.values = np.flipud(da.values)
    return imod.prepare.reproject(
        da, src_crs={"init": "epsg:28992"}, dst_crs={"init": "epsg:3857"}
    )


prmoptions = [
    "Gewasopbrengst voor maatregel (E/ha)",
    "Gewasopbrengst na maatregel (E/ha)",
    "Gewasopbrengst toe / afname (E/ha)",
    "Gewasopbrengst toe / afname (%)",
    "Droogteschade voor maatregel (-)",
    "Droogteschade na maatregel (-)",
    "Droogteschade toe / afname (-)",
    "Droogteschade toe / afname (%)",
    "Zoutschade voor maatregel (-)",
    "Zoutschade na maatregel (-)",
    "Zoutschade toe / afname (-)",
    "Zoutschade toe / afname (%)",
    "Beregening voor maatregel (mm)",
    "Beregening na maatregel (mm)",
    "Beregening toe / afname (mm)",
    "Beregening toe / afname (%)",
    "Kansrijkheid van maatregel (-)",
]
orgprm = prmoptions[0]

measure_combinations = {
    1: [1, 22],
    2: [3, 22],
    3: [1, 14],
    4: [3, 14],
    5: [6, 22],
    6: [6, 14],
    7: [7, 14],
    8: [7, 22],
    10: [10],
    11: [11],
    13: [13],
    15: [15],
    16: [16],
    17: [17],
    19: [19],
    21: [21, 22],
    22: [21, 23],
    23: [22],
    24: [23],
    25: [11, 14],
    27: [11, 22],
    29: [26],
    30: [14, 26],
    31: [22, 26],
    32: [27],
    33: [14, 27],
    34: [22, 27],
    35: [28],
    36: [14, 28],
    37: [22],
}
fwoo_measures = {
    1: "dank_8_mar_hoog.idf",
    2: "kansr_sup.idf",
    3: "fwoo4_freshmaker.idf",
    6: "dank_5_asr_laag.idf",
    7: "fwoo3_kreekruginfiltratie.idf",
    10: "kansrijkheid_draintrad.idf",
    11: "fwoo2_drainage.idf",
    13: "fwoo2_drainage.idf",
    14: "everywhere.idf",
    15: "bereg_gw.idf",
    16: "bereg_oppw.idf",
    17: "fwoo1_drains2buffer.idf",
    19: "fwoo1_drains2buffer.idf",
    21: "dank_5_asr_laag.idf",
    22: "everywhere.idf",
    23: "kansr_sub.idf",
    24: "kansr_sup.idf",
    25: "kansr_sub.idf",
    26: "everywhere.idf",
    27: "fwoo6_stuwen.idf",
    28: "fwoo7_slootbodemv.idf",
}
fwoo_measure_combinations = {
    k: [fwoo_measures[vi] for vi in v] for k, v in measure_combinations.items()
}
palettes = {
    "Type modelbedrijf": tuple(
        Category20_20[i] for i in (17, 11, 3, 10, 4, 12, 6, 0, 14)
    ),
    "Landgebruik": (
        "#72ea14",
        "#f49c0e",
        "#b86300",
        "#f0147f",
        "#ffff00",
        "#ff00cd",
        "#ffb9ad",
        "#40ffd8",
        "#eeff6e",
        "#b181ad",
    ),
    "Bodemtype": (
        "#8121ff",
        "#ac4bfd",
        "#347fff",
        "#5aabfd",
        "#d47fff",
        "#194cff",
        "#ffff49",
        "#ffd4a9",
        "#ffd44a",
        "#fe7f4a",
        "#d24c49",
        "#58021d",
        "#aa7f49",
        "#ff7ea7",
        "#d4ff4b",
        "#82ff4b",
        "#1baa00",
        "#18d4a6",
        "#d4d400",
        "#aad31e",
        "#ff014a",
        "#d5fffe",
        "#abaca7",
    ),
    "GLG": (
        "#bf0000",
        "#d90000",
        "#ed0000",
        "#ff2a00",
        "#ff5500",
        "#fe7300",
        "#fe8c00",
        "#feaa00",
        "#febf0a",
        "#fec414",
        "#fedd33",
        "#feff00",
        "#feff73",
        "#ffffbe",
        "#d1ff73",
        "#a3ff73",
        "#55ff00",
    ),
    "Aanwezigheid buisdrainage": tuple(Category20_20[i] for i in (15, 6)),
    "Aanwezigheid beregening": tuple(Category20_20[i] for i in (15, 0, 4)),
}


inputmaps = {
    "Modelbedrijven": {
        "fn": "modelbedrijven.idf",
        "nominal": True,
        "palette": Category20_20,
    },
    "Type modelbedrijf": {
        "fn": "bedrijftypen.idf",
        "nominal": True,
        "palette": palettes["Type modelbedrijf"],
        "min": 1,
        "max": 9,
    },
    "Landgebruik": {
        "fn": "landgebruik.idf",
        "nominal": True,
        "palette": palettes["Landgebruik"],
        "min": 1,
        "max": 10,
    },
    "Bodemtype": {
        "fn": "soilreclass.idf",
        "nominal": True,
        "palette": palettes["Bodemtype"],
        "min": 1,
        "max": 23,
    },
    "Gemiddeld Laagste Grondwaterstand": {
        "fn": "glgreclass.idf",
        "nominal": True,
        "palette": palettes["GLG"],
        "min": 1,
        "max": 21,
    },
    #    "Gemiddelde Voorjaarsgrondwaterstand":"gvg.idf",
    "Aanwezigheid buisdrainage": {
        "fn": "buisdrainage.idf",
        "nominal": True,
        "palette": palettes["Aanwezigheid buisdrainage"],
        "min": 0,
        "max": 1,
    },
    "Aanwezigheid beregening": {
        "fn": "beregeningstype.idf",
        "nominal": True,
        "palette": palettes["Aanwezigheid beregening"],
        "min": 0,
        "max": 2,
    },
}


def classes_to_dict(classes):
    dclasses = [f"{a:.1f} - {b:.1f} m+mv" for a, b in zip(classes[:-1], classes[1:])]
    dclasses[0] = f"dieper dan {classes[1]:.1f} m+mv"
    dclasses[-1] = f"ondieper dan {classes[-1]:.1f} m+mv"
    return {i + 1: v for i, v in enumerate(dclasses)}


inputmap_translate = {
    "Type modelbedrijf": {
        1: "Melkveehouderij",
        2: "Veehouderij",
        3: "Akkerbouw",
        4: "Gemengd (akkerbouw-ruwvoer)",
        5: "Boomkweek",
        6: "Fruit",
        7: "Bollen",
        8: "Overig",
        9: "Niet-grondgebonden",
    },
    "Landgebruik": {
        1: "Gras",
        2: "Mais",
        3: "Aardappelen",
        4: "(Suiker)bieten",
        5: "Granen",
        6: "Overig",
        7: "Boomteelt",
        8: "Glastuinbouw",
        9: "Fruitteelt",
        10: "Bollen",
    },
    "Bodemtype": {
        1: "Veraarde bovengrond op diep veen",
        2: "Veraarde bovengrond op veen op zand",
        3: "Kleidek op veen",
        4: "Kleidek op veen op zand",
        5: "Zanddek op veen op zand",
        6: "Veen op ongerijpte klei",
        7: "Stuifzand",
        8: "Leemarm zand",
        9: "Zwaklemig fijn zand",
        10: "Zwaklemig fijn zand op grof zand",
        11: "Sterk lemig fijn zand op (kei-)leem",
        12: "Enkeerdgronden (fijn zand)",
        13: "Sterk lemig zand",
        14: "Grof zand",
        15: "Zavel met homogeen profiel",
        16: "Lichte klei met homogeen profiel",
        17: "Klei met zware tussenlaag of ondergrond",
        18: "Klei op veen",
        19: "Klei op zand",
        20: "Klei op grof zand",
        21: "Leem",
        22: "Water",
        23: "Bebouwing",
    },
    "Gemiddeld Laagste Grondwaterstand": classes_to_dict(np.arange(-3.1, -0.9, 0.1)),
    #    "Gemiddelde Voorjaarsgrondwaterstand":classes_to_dict(np.arange(-3.5,0.6,0.1)),
    "Aanwezigheid buisdrainage": {0: "Niet gedraineerd", 1: "Gedraineerd"},
    "Aanwezigheid beregening": {
        0: "Geen beregening",
        1: "Beregening uit oppervlaktewater",
        2: "Beregening uit grondwater",
    },
    "Kansrijkheid": {
        0: "Niet kansrijk",
        1: "Kansrijk",
    },
}

paths = {
    "Gewasopbrengst": "{out_dir}/Agricom_Out/Module_C/ActYldEur_{year}1231.idf",
    "Beregening": "{in_dir}/beregening_??_{year}1231.idf",
    "Droogteschade": "{in_dir}/droogteschade_{year}1231.idf",
    "Zoutschade": "{in_dir}/zoutschade_{year}1231.idf",
    "Kansrijkheid": "{in_dir}/Kansrijkheidsgrids/{fwoo_map}",
}
# Droogteschade voor maatregel (-)"
prmpattern = re.compile(r"(?P<prm>[^ ]+) (?P<voorna>[^ ]+).*\((?P<unit>[^)]+)\)")
mcpattern = re.compile(r"(?P<name>[^(]+) \((?P<mc>[0-9,]+)\)")


def _isnan(x):
    if isinstance(x, type(np.nan)):
        return np.isnan(x)
    else:
        return False


my_isnan = np.vectorize(_isnan)


def effect_tab(results, mctitles, ini):
    mcstrip = mctitles.apply(lambda x: x[: x.find("(") - 1])

    tot_ha = results.groupby(["CompanyNumber"]).nth(0)["Area_ha"].sum()
    # print(tot_ha, len(results.groupby(['CompanyNumber']).nth(0)))

    # load and project compmap
    compmap = imod.idf.open(ini["company_idf"]).load()

    # create unique legend for companies
    mincomp = float(compmap.min())
    maxcomp = float(compmap.max())
    inputmaps["Modelbedrijven"]["min"] = mincomp
    inputmaps["Modelbedrijven"]["max"] = maxcomp
    rgb = np.random.random((int(maxcomp - mincomp), 3)) * 255.0
    inputmaps["Modelbedrijven"]["palette"] = [
        f"#{r:02x}{g:02x}{b:02x}" for r, g, b in rgb.astype(int)
    ]

    def make_dataset(results, mc=None, prm="NBC", year="2003", inputmap=None, ini=ini):
        """Function to make a dataset for the map based on Regioscan result"""

        # strip prm and mc to parts
        prmname = prm
        prm = prmpattern.match(prm).groupdict()
        # print(prm)
        mc = mcpattern.match(mc).groupdict()
        mcname = mc["name"]
        mc = int(mc["mc"])

        attrs = {}
        if inputmap is None:
            attrs["name"] = f"{prmname} - {year}"

            # year
            if year == "gemiddeld":
                year = "*"

            # load required map(s), only where compmap
            path = paths[prm["prm"]]
            if prm["voorna"] in ["voor", "toe"]:
                fn_voor = path.format(
                    out_dir=f"{ini['pth_calculation']}/{ini['scenario']}/reference",
                    in_dir=f"{ini['pth_hydroinput']}/{ini['scenario']}/MetBeregening",
                    year=year,
                )
                # print(fn_voor)
                if prm["prm"] == "Beregening":
                    try:
                        voor = (
                            imod.idf.open(fn_voor, pattern="{name}_{gwsw}_{time}")
                            .fillna(0)
                            .sum(dim="gwsw")
                            .mean(dim="time")
                        )
                    except IOError:
                        voor = compmap.copy() * 0
                else:
                    voor = imod.idf.open(fn_voor).mean(dim="time")
                voor = voor.where(~compmap.isnull())
                show = voor
            if prm["voorna"] in ["na", "toe"]:
                fn_na = path.format(
                    out_dir=f"{ini['pth_calculation']}/{ini['scenario']}/mc_{mc}",
                    in_dir=f"{ini['pth_calculation']}/{ini['scenario']}/mc_{mc}/Agricom_In",
                    year=year,
                )
                # print(fn_na)
                if prm["prm"] == "Beregening":
                    try:
                        na = (
                            imod.idf.open(fn_na, pattern="{name}_{gwsw}_{time}")
                            .fillna(0)
                            .sum(dim="gwsw")
                            .mean(dim="time")
                        )
                    except IOError:
                        na = compmap.copy() * 0
                else:
                    na = imod.idf.open(fn_na).mean(dim="time")
                na = na.where(~compmap.isnull())
                show = na
            if prm["voorna"] == "toe":
                diff = na - voor
                if prm["unit"] == "%":
                    diff = diff / voor * 100.0
                show = diff
            if prm["voorna"] == "van":
                # kansrijkheid
                if prm["prm"] == "Kansrijkheid":
                    fwoo_mc = fwoo_measure_combinations[mc]

                    # combine different measures
                    fwoo = xr.ones_like(compmap).where(~compmap.isnull())
                    for fwoo_fn in fwoo_mc:
                        fwoo_fn = path.format(
                            in_dir=f"{ini['pth_hydroinput']}", fwoo_map=fwoo_fn
                        )
                        fwoo *= imod.idf.open(fwoo_fn, pattern="{name}").load()
                    fwoo = fwoo.where(fwoo < 1 | fwoo.isnull(), 1.0)

                    # fwoo per company
                    # fwoo = fwoo.groupby(compmap).max()
                    # show = compmap.copy().load()
                    # show.values[~show.isnull()] = np.vectorize(fwoo.to_series().get)(show.values[~show.isnull()])#.groupby(compmap).max()
                    show = fwoo.where(~compmap.isnull())

            show = show.load()
            attrs["min"] = float(
                show.quantile(ini["pct_low"] / 100.0, dim=xr.ALL_DIMS).values
            )
            attrs["max"] = float(
                show.quantile(ini["pct_up"] / 100.0, dim=xr.ALL_DIMS).values
            )
            attrs["nominal"] = False
            show = to_mercator(show)
            cds_values = np.round(show.values, 2)
            if attrs["min"] < 0 or prm["voorna"] == "toe":
                attrs["diverging"] = True
                max_ = max(abs(attrs["max"]), abs(attrs["min"]))
                attrs["max"] = max_
                attrs["min"] = -max_
            elif prm["prm"] == "Kansrijkheid":
                attrs["diverging"] = True
                attrs["max"] = 1.0
                attrs["min"] = 0.0
                attrs["nominal"] = True
                translator = inputmap_translate["Kansrijkheid"]
                translator[np.nan] = ""
                cds_values = cds_values.astype(object)
                cds_values = np.vectorize(translator.get)(cds_values)
            else:
                attrs["diverging"] = False
            if attrs["min"] == attrs["max"]:
                attrs["max"] += 0.01
                attrs["min"] -= 0.01
            if "opbrengst" in prmname or "Kansrijkheid" in prmname:
                attrs["flip"] = True
            else:
                attrs["flip"] = False
        else:
            # show Inputmap
            attrs["name"] = f"Invoerkaart {inputmap}"
            attrs["palette"] = inputmaps[inputmap].get("palette", Category20_20)
            fn = (
                f"{ini['pth_hydroinput']}/{ini['scenario']}/{inputmaps[inputmap]['fn']}"
            )
            show = imod.idf.open(fn, pattern="{name}").load()
            show = show.where(~compmap.isnull())
            attrs["diverging"] = False
            attrs["flip"] = False
            attrs["nominal"] = inputmaps[inputmap]["nominal"]
            attrs["min"] = inputmaps[inputmap].get("min", float(show.min()))
            attrs["max"] = inputmaps[inputmap].get("max", float(show.max()))

            show = to_mercator(show)
            cds_values = np.round(show.values, 2)
            if inputmap in inputmap_translate:
                translator = inputmap_translate[inputmap]
                translator[np.nan] = ""
                cds_values = cds_values.astype(object)
                # cds_values[~my_isnan(cds_values)] = np.vectorize(translator.get)(cds_values[~my_isnan(cds_values)])
                cds_values = np.vectorize(translator.get)(cds_values)

        (
            attrs["dx"],
            attrs["xmin"],
            attrs["xmax"],
            attrs["dy"],
            attrs["ymin"],
            attrs["ymax"],
        ) = imod.util.spatial_reference(show)
        cds = ColumnDataSource({"image": [show.values], "values": [cds_values]})
        return cds, attrs

    def make_plot(src, attrs):
        # Create the plot with no axes or grid
        buf = 0.0  # 2000.
        p = figure(
            plot_width=700,
            plot_height=800,
            title=attrs["name"],
            x_axis_type=None,
            y_axis_type=None,
            match_aspect=True,
            aspect_ratio=1,
            aspect_scale=1,
            x_range=(attrs["xmin"] - buf, attrs["xmax"] + buf),
            y_range=(attrs["ymin"] - buf, attrs["ymax"] + buf),
            tools=["pan", "wheel_zoom", "save", "reset"],
            toolbar_location="above",
        )
        # p.xaxis.visible = False
        # p.yaxis.visible = False
        p.grid.visible = False
        p.add_tools(
            HoverTool(tooltips=[("value", "@values")]), BoxZoomTool(match_aspect=True)
        )

        if attrs["diverging"]:
            if attrs["flip"]:
                cmap = LinearColorMapper(
                    palette=cc.b_diverging_gwr_55_95_c38[::-1],
                    low=attrs["min"],
                    high=attrs["max"],
                )
            else:
                cmap = LinearColorMapper(
                    palette=cc.b_diverging_gwr_55_95_c38,
                    low=attrs["min"],
                    high=attrs["max"],
                )
        else:
            cmap = LinearColorMapper(
                palette=cc.b_linear_gow_65_90_c35[::-1],
                low=attrs["min"],
                high=attrs["max"],
            )
        # rainbow_bgyr_35_85_c72

        p.add_tile(get_provider(Vendors.CARTODBPOSITRON))
        img = p.image(
            image="image",
            dh=attrs["ymax"] - attrs["ymin"],
            dw=attrs["xmax"] - attrs["xmin"],
            x=attrs["xmin"],
            y=attrs["ymin"],
            global_alpha=0.5,
            color_mapper=cmap,
            dh_units="data",
            dw_units="data",
            source=src,
        )
        img.glyph.color_mapper.nan_color = (0, 0, 0, 0)
        color_bar = ColorBar(
            color_mapper=cmap, label_standoff=8, border_line_color=None, location=(0, 0)
        )

        p.add_layout(color_bar, "left")
        return p

    def update_map(prm, mc, year, inputmap):
        global orgprm

        # redo analysis
        newarr, newattrs = make_dataset(
            results, mc=mc, prm=prm, year=year, inputmap=inputmap
        )
        # update column data source
        src.data.update(newarr.data)

        # print(newattrs)
        p.title.text = newattrs["name"]

        cm = p.select_one(LinearColorMapper)
        if newattrs["diverging"]:
            if newattrs["flip"]:
                cm.update(
                    palette=cc.b_diverging_gwr_55_95_c38[::-1],
                    low=newattrs["min"],
                    high=newattrs["max"],
                )
            else:
                cm.update(
                    palette=cc.b_diverging_gwr_55_95_c38,
                    low=newattrs["min"],
                    high=newattrs["max"],
                )
        elif newattrs["nominal"]:
            cm.update(
                low=newattrs["min"],
                high=newattrs["max"],
                palette=newattrs.get("palette", Category20_20),
            )
        else:
            cm.update(
                palette=cc.b_linear_gow_65_90_c35[::-1],
                low=newattrs["min"],
                high=newattrs["max"],
            )
        orgprm = prm

    def update_notinput(attr, old, new):
        update_map(prmselect.value, mcselection.value, yearselect.value, None)

    def update_input(attr, old, new):
        # which inputmap
        inputmap = inputselect.value
        if inputmap not in inputmaps:
            inputmap = None
        update_map(prmselect.value, mcselection.value, yearselect.value, inputmap)

    # get available measure combinations
    mctitles2 = mctitles.reindex(index=sorted(results["mc"].unique()))
    if "(" not in mctitles2.iloc[0]:
        # add mc id to mctitles
        mclist = list(map(lambda x, y: x + f" ({y})", mctitles2, mctitles2.index))
    else:
        mclist = np.unique(mctitles2.values).tolist()

    # Selection of mc's for plotting
    mcselection = Select(
        title="Kies maatregel", options=mclist, value=mclist[0]
    )  # title='Selecteer maatregelen',
    mcselection.on_change("value", update_notinput)

    # Selection of parameter to plot
    prmselect = Select(title="Laat zien:", value=prmoptions[0], options=prmoptions)
    prmselect.on_change("value", update_notinput)

    # Selection of years
    yearselect = Select(
        title="Bekijk resultaten voor:",
        value="gemiddeld",
        options=["gemiddeld"]
        + [str(y) for y in range(ini["startyear"], ini["endyear"] + 1)],
    )
    yearselect.on_change("value", update_notinput)

    # Input maps
    inputselect = Select(
        title="Bekijk invoerkaarten:",
        value="-----------------",
        options=["-----------------"] + list(inputmaps.keys()),
    )
    inputselect.on_change("value", update_input)

    # Initial source and plot
    src, attrs = make_dataset(
        results, prm=orgprm, mc=mclist[0], year=str(ini["startyear"])
    )

    p = make_plot(src, attrs)

    # Layout setup
    layout = column(  # row([impldegselect,prmselect,sortbyselect]),
        row(
            [
                column(
                    [
                        Div(text="<b>Regioscan Zoetwatermaatregelen</b>"),
                        mcselection,
                        prmselect,
                        yearselect,
                        inputselect,
                    ]
                ),
                p,
            ]
        ),
        sizing_mode="scale_both",
    )
    # tab = Panel(child=layout, title='Regioscan kaart')

    return layout


# TODO: sorteer op euros per m3!
